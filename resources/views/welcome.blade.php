@include('shop.front.layouts.welcome_headers.welcome_header')
{{--
@error(401)
    {{$message}}
@enderror --}}

        <!-- ========== MAIN CONTENT ========== -->
        <main id="content" role="main">
            <!-- Vertical-Slider-Category-Banner Section -->
            <div class="mb-6 bg-gray-1 pb-4">
                <div class="container">
                    <div class="row no-gutters">
                        @include('shop.front.layouts.welcome_headers.vertical_menu')
                        @include('shop.front.layouts.welcome_headers.slider')
                    </div>
                </div>
            </div>
            <!-- End Vertical-Slider-Category-Banner Section -->
            @include('shop.front.layouts.inc.cats_carousel')


            {{-- Near Me --}}
            @php
            $user_city = App\UserDetails::where('user_id', Illuminate\Support\Facades\Auth::user()->id)->first()->city_id;

            $near_by_sellers = App\UserDetails::where('city_id', $user_city)->get();

            $near_me = array();
            foreach($near_by_sellers as $nbs){
                $ads = App\Ad::where('user_id', $nbs->user_id)->get();
                foreach($ads as $ad){
                    array_push($near_me, $ad);
                }
            }
            @endphp

            @if(count($near_me) > 0)

            <div class="px-5 mx-5">
             <div class="d-flex border-bottom border-color-1 flex-lg-nowrap flex-wrap border-md-down-top-0 border-sm-bottom-0 mb-2 mb-md-0">
                 <h3 class="section-title section-title__full mb-0 pb-2 font-size-22">Near Me</h3>

                 <a class="ml-md-auto d-block text-gray-16 align-self-center" href="../shop/product-categories-7-column-full-width.html">Go to Near Me Section <i class="ec ec-arrow-right-categproes"></i></a>
             </div>
            </div>
            <div class="mb-5">
             <div class="bg-img-hero" style="background-image: url({{asset('/images/slide1.jpg')}});">

             @include('shop.front.layouts.inc.near_me')
             </div>
            </div>
            @endif
            {{-- End Near Me --}}


        {{-- Deals of the Day --}}
            <div class="px-5 mx-5">
                <div class="d-flex border-bottom border-color-1 flex-lg-nowrap flex-wrap border-md-down-top-0 border-sm-bottom-0 mb-2 mb-md-0">
                    <h3 class="section-title section-title__full mb-0 pb-2 font-size-22">Deals of the Day</h3>
                    <div class="js-countdown ml-md-5 mt-md-n1 border-top border-color-1 border-md-top-0 w-100 w-md-auto pt-2 pt-md-0 mb-2 mb-md-0"
                            data-end-date="2020/11/30"
                            data-hours-format="%H"
                            data-minutes-format="%M"
                            data-seconds-format="%S">
                            <div class="flex-horizontal-center d-inline-flex bg-primary py-2 align-self-start height-33 px-5 rounded-pill text-gray-2 font-size-15 font-weight-bold text-lh-1">
                                <h5 class="font-size-15 mb-0 font-weight-bold text-lh-1 mr-1">Ends in:</h5>
                                <div class="px-1">
                                    <span class="js-cd-hours"></span>
                                </div>
                                <div class="">:</div>
                                <div class="px-1">
                                    <span class="js-cd-minutes"></span>
                                </div>
                                <div class="">:</div>
                                <div class="px-1">
                                    <span class="js-cd-seconds"></span>
                                </div>
                            </div>
                        </div>
                    <a class="ml-md-auto d-block text-gray-16 align-self-center" href="../shop/product-categories-7-column-full-width.html">Go to Near Me Section <i class="ec ec-arrow-right-categproes"></i></a>
                </div>
               </div>
               <div class="mb-5">
                <div class="bg-img-hero" style="background-image: url({{asset('/images/slide2.jpg')}});">
                @include('shop.front.layouts.inc.deals_of_the_day')
                </div>
               </div>
                <!-- End Deals of The Day -->

                <!-- Full banner -->
                <div class="mb-6">
                    <a href= class="d-block text-gray-90">
                        <div class=""
                        style="background-image: url({{asset('/images/slide4.jpg')}});"
                        >
                            <div class="space-top-2-md p-4 pt-6 pt-md-8 pt-lg-6 pt-xl-8 pb-lg-4 px-xl-8 px-lg-6">
                                <div class="flex-horizontal-center mt-lg-3 mt-xl-0 overflow-auto overflow-md-visble">
                                    {{-- <h1 class="text-lh-38 font-size-32 font-weight-light mb-0 flex-shrink-0 flex-md-shrink-1">SHOP AND <strong>SAVE BIG</strong> ON HOTTEST TABLETS</h1>
                                    <div class="ml-5 flex-content-center flex-shrink-0">
                                        <div class="bg-primary rounded-lg px-6 py-2">
                                            <em class="font-size-14 font-weight-light">STARTING AT</em>
                                            <div class="font-size-30 font-weight-bold text-lh-1">
                                                <sup class="">$</sup>79<sup class="">99</sup>
                                            </div>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- End Full banner -->

                @include('shop.front.layouts.inc.one_with_special_offer')

                <!-- Popular Categories this Week -->
                <div class="mb-6">
                    <!-- Nav nav-pills -->
                    <div class="position-relative text-center z-index-2">
                        <div class=" d-flex justify-content-between border-bottom border-color-1 flex-lg-nowrap flex-wrap border-md-down-top-0 border-md-down-bottom-0">
                            <h3 class="section-title mb-0 pb-2 font-size-22">Popular Categories this Week</h3>
                        </div>
                    </div>
                    <!-- End Nav Pills -->
                    <div class="row">
                        <div class="col-12 col-xl-auto pr-lg-2">
                            <div class="min-width-200 mt-xl-5">
                                <ul class="list-group list-group-flush flex-nowrap flex-xl-wrap flex-row flex-xl-column overflow-auto overflow-xl-visble mb-3 mb-xl-0 border-top border-color-1 border-lg-top-0">
                                    <li class="border-color-1 list-group-item border-lg-down-0 flex-shrink-0 flex-xl-shrink-1"><a class="hover-on-bold py-1 px-3 text-gray-90 d-block" href="../shop/product-categories-7-column-full-width.html">Earbuds and In-ear</a></li>
                                    <li class="border-color-1 list-group-item border-lg-down-0 flex-shrink-0 flex-xl-shrink-1"><a class="hover-on-bold py-1 px-3 text-gray-90 d-block" href="../shop/product-categories-7-column-full-width.html">Headphones</a></li>
                                    <li class="border-color-1 list-group-item border-lg-down-0 flex-shrink-0 flex-xl-shrink-1"><a class="hover-on-bold py-1 px-3 text-gray-90 d-block" href="../shop/product-categories-7-column-full-width.html">Kids' Headphones</a></li>
                                    <li class="border-color-1 list-group-item border-lg-down-0 flex-shrink-0 flex-xl-shrink-1"><a class="hover-on-bold py-1 px-3 text-gray-90 d-block" href="../shop/product-categories-7-column-full-width.html">Over-Ear and On-Ear</a></li>
                                    <li class="border-color-1 list-group-item border-lg-down-0 flex-shrink-0 flex-xl-shrink-1"><a class="hover-on-bold py-1 px-3 text-gray-90 d-block" href="../shop/product-categories-7-column-full-width.html">PC Gaming Headsets</a></li>
                                    <li class="border-color-1 list-group-item border-lg-down-0 flex-shrink-0 flex-xl-shrink-1"><a class="hover-on-bold py-1 px-3 text-gray-90 d-block" href="../shop/product-categories-7-column-full-width.html">Pro &amp; DJ Headset</a></li>
                                    <li class="border-color-1 list-group-item border-lg-down-0 flex-shrink-0 flex-xl-shrink-1"><a class="hover-on-bold py-1 px-3 text-gray-90 d-block" href="../shop/product-categories-7-column-full-width.html">Refurbished Headset</a></li>
                                    <li class="border-color-1 list-group-item border-lg-down-0 flex-shrink-0 flex-xl-shrink-1"><a class="hover-on-bold py-1 px-3 text-gray-90 d-block" href="../shop/product-categories-7-column-full-width.html">Waterproof Headset</a></li>
                                    <li class="border-color-1 list-group-item border-lg-down-0 flex-shrink-0 flex-xl-shrink-1"><a class="hover-on-bold py-1 px-3 text-gray-90 d-block" href="../shop/product-categories-7-column-full-width.html">Wireless and Bluetooth</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-5 col-lg-auto">
                            <a href= class="d-block"><img class="img-fluid" src={{asset("/front_new/img/370X608/img1.jpg")}} alt="Image Description"></a>
                        </div>
                        <div class="col-md pl-md-0">
                            <ul class="row list-unstyled products-group no-gutters mb-0">
                                @foreach($loaded_ads as $ad)
                                <li class="col-6 col-md-4 col-wd-3 product-item">
                                    <div class="product-item__outer h-100">
                                        <div class="product-item__inner bg-white p-3">
                                            <div class="product-item__body pb-xl-2">
                                                <div class="mb-2"><a href="/view-ad/{{$ad->id}}" class="font-size-12 text-gray-5">Speakers</a></div>
                                                <h5 class="mb-1 product-item__title"><a href="/view-ad/{{$ad->id}}" class="text-blue font-weight-bold">{{$ad->title}}</a></h5>
                                                <div class="mb-2">
                                                    <a href="/view-ad/{{$ad->id}}" class="d-block text-center"><img class="img-fluid" src={{asset(env('IMAGE_LOCATION').$ad->img_1)}} alt="Image Description"></a>
                                                </div>
                                                <div class="flex-center-between mb-1">
                                                    <div class="prodcut-price">
                                                        <div class="text-gray-100">Rs.{{$ad->price}}</div>
                                                    </div>
                                                    <div class="d-none d-xl-block prodcut-add-cart">
                                                        <a
                                                        onclick="add_to_cart({{$ad->id}}, 1, {{$ad->price}}, {{$ad->quantity}})" class="btn-add-cart btn-primary transition-3d-hover"><i class="ec ec-add-to-cart"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-item__footer">
                                                <div class="border-top pt-2 flex-center-between flex-wrap">
                                                    {{-- <a href="../shop/compare.html" class="text-gray-6 font-size-13"><i class="ec ec-compare mr-1 font-size-15"></i> Compare</a> --}}
                                                    <a
                                                    title="Add to Wishlist"
                                                    onclick="add_to_wishlist({{$ad->id}})"
                                                    class=" btn btn-primary">
                                                    <i
                                                    class="ec ec-favorites mr-1 "></i> Add to Wishlist</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>

                <!-- End Popular Categories this Week -->



                <!-- Banner -->
                <div class="mb-5">
                    <div class="row">
                        <div class="col-lg-6 mb-4 mb-xl-0">
                            <a href="../shop/shop.html" class="d-block">
                                <img class="img-fluid" src={{asset('/images/slide1.jpg')}} alt="Image Description">
                            </a>
                        </div>
                        <div class="col-lg-6 mb-4 mb-xl-0">
                            <a href="../shop/shop.html" class="d-block">
                                <img class="img-fluid" src={{asset('/images/slide2.jpg')}} alt="Image Description">
                            </a>
                        </div>
                    </div>
                </div>
                <!-- End Banner -->

                @include('shop.front.layouts.inc.custom_one')
                <!-- Banners -->
                <div class="mb-6">
                    <div class="row">
                        <div class="col-lg-8 mb-5">
                            <div class="bg-gray-17">
                                <a href="../shop/shop.html" class="row align-items-center">
                                    {{-- <div class="col-md-6">
                                        <div class="ml-md-7 mt-6 mt-md-0 ml-4 text-gray-90">
                                            <h2 class="font-size-28 font-size-20-lg max-width-270 text-lh-1dot2">G9 Laptops with Ultra 4K HD Display</h2>
                                            <p class="font-size-18 font-size-14-lg text-gray-90 font-weight-light">and the fastest Intel Core i7 processor ever</p>
                                            <div class="text-lh-28">
                                                <span class="font-size-18 font-size-14-lg font-weight-light">from</span>
                                                <span class="font-size-46 font-size-30-lg font-weight-semi-bold"><sup class="">$</sup>399</span>
                                            </div>
                                        </div>
                                    </div> --}}
                                    <div class="col-md-6">
                                        <img class="img-fluid" src={{asset('/images/slide2.jpg')}} alt="Image Description">
                                    </div>
                                    <div class="col-md-6">
                                        <img class="img-fluid" src={{asset('/images/slide3.jpg')}} alt="Image Description">
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 mb-5">
                            <div class="h-100">
                                <a href= class="d-block"><img class="img-fluid" src={{asset('/images/slide4.jpg')}} alt="Image Description"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Banners -->
                @include('shop.front.layouts.inc.auto_slider')
                <!-- Brand Carousel -->
                <div class="container mb-8">
                    <div class="py-2 border-top border-bottom">
                        <div class="js-slick-carousel u-slick my-1"
                            data-slides-show="5"
                            data-slides-scroll="1"
                            data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-normal u-slick__arrow-centered--y"
                            data-arrow-left-classes="fa fa-angle-left u-slick__arrow-classic-inner--left z-index-9"
                            data-arrow-right-classes="fa fa-angle-right u-slick__arrow-classic-inner--right"
                            data-responsive='[{
                                "breakpoint": 992,
                                "settings": {
                                    "slidesToShow": 2
                                }
                            }, {
                                "breakpoint": 768,
                                "settings": {
                                    "slidesToShow": 1
                                }
                            }, {
                                "breakpoint": 554,
                                "settings": {
                                    "slidesToShow": 1
                                }
                            }]'>
                            <div class="js-slide">
                                <a href="#" class="link-hover__brand">
                                    <img class="img-fluid m-auto max-height-50" src={{asset("/front_new/img/200X60/img1.png")}} alt="Image Description">
                                </a>
                            </div>
                            <div class="js-slide">
                                <a href="#" class="link-hover__brand">
                                    <img class="img-fluid m-auto max-height-50" src={{asset("/front_new/img/200X60/img2.png")}} alt="Image Description">
                                </a>
                            </div>
                            <div class="js-slide">
                                <a href="#" class="link-hover__brand">
                                    <img class="img-fluid m-auto max-height-50" src={{asset("/front_new/img/200X60/img3.png")}} alt="Image Description">
                                </a>
                            </div>
                            <div class="js-slide">
                                <a href="#" class="link-hover__brand">
                                    <img class="img-fluid m-auto max-height-50" src={{asset("/front_new/img/200X60/img4.png")}} alt="Image Description">
                                </a>
                            </div>
                            <div class="js-slide">
                                <a href="#" class="link-hover__brand">
                                    <img class="img-fluid m-auto max-height-50" src={{asset("/front_new/img/200X60/img5.png")}} alt="Image Description">
                                </a>
                            </div>
                            <div class="js-slide">
                                <a href="#" class="link-hover__brand">
                                    <img class="img-fluid m-auto max-height-50" src={{asset("/front_new/img/200X60/img6.png")}} alt="Image Description">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Brand Carousel -->
            </div>
        </main>
        <!-- ========== END MAIN CONTENT ========== -->

 @include('shop.front.layouts.footer')
