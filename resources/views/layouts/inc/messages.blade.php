@if($errors->any() )
    @foreach ($errors->all() as $error)
        <div class="alert alert-danger">
            {{$error}}
        </div>
    @endforeach
@endif

@if(session('success'))
    {{-- <div class='alert alert-success text-center'>
        {{session('success')}}
    </div> --}}
    <script>
        Swal.fire("{{session('success')}}")
    </script>
@endif

@if (session('error'))
    {{-- <div class='alert alert-danger text-center'>
        {{session('error')}}
    </div> --}}
    <script>
        Swal.fire("{{session('error')}}")
    </script>
@endif
