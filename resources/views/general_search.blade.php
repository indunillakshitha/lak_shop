@extends('shop.front.layouts.main')

@section('content')

<div class="container">

    @if(count($loaded_ads)==0)
    <div class="row py-5"><h1 >No Search Results Available...</h1></div>
    @else
            <div class="col-md-12 pl-md-0">
                <ul class="row list-unstyled products-group no-gutters mb-0">
                    @foreach($loaded_ads as $ad)
                    <li class="col-6 col-md-4 col-wd-2 product-item">
                        <div class="product-item__outer h-100">
                            <div class="product-item__inner bg-white p-3">
                                <div class="product-item__body pb-xl-2">
                                    <div class="mb-2"><a href="/view-ad/{{$ad->id}}" class="font-size-12 text-gray-5">Speakers</a></div>
                                    <h5 class="mb-1 product-item__title"><a href="/view-ad/{{$ad->id}}" class="text-blue font-weight-bold">{{$ad->title}}</a></h5>
                                    <div class="mb-2">
                                        <a href="/view-ad/{{$ad->id}}" class="d-block text-center"><img class="img-fluid" src={{asset(env('IMAGE_LOCATION').$ad->img_1)}} alt="Image Description"></a>
                                    </div>
                                    <div class="flex-center-between mb-1">
                                        <div class="prodcut-price">
                                            <div class="text-gray-100">Rs.{{$ad->price}}</div>
                                        </div>
                                        <div class="d-none d-xl-block prodcut-add-cart">
                                            <a
                                            onclick="add_to_cart({{$ad->id}}, 1, {{$ad->price}}, {{$ad->quantity}})" class="btn-add-cart btn-primary transition-3d-hover"><i class="ec ec-add-to-cart"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-item__footer">
                                    <div class="border-top pt-2 flex-center-between flex-wrap">
                                        {{-- <a href="../shop/compare.html" class="text-gray-6 font-size-13"><i class="ec ec-compare mr-1 font-size-15"></i> Compare</a> --}}
                                        <a
                                        title="Add to Wishlist"
                                        onclick="add_to_wishlist({{$ad->id}})"
                                        class=" btn btn-primary">
                                        <i
                                        class="ec ec-favorites mr-1 "></i> Add to Wishlist</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    @endif
</div>

@endsection
