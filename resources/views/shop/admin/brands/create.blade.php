@extends('shop.admin.layouts.main')

@section('content')
<div class="content-header row">
</div>
<div class="content-overlay"></div>
<div class="content-wrapper">
    <div class="content-body">
        <div class="col-12">
            <div class="card mb-0">
                <div class="card-header">
                    <form action="{{route('brand.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                        <label for="">New Brand for {{$parent[0]->category}}  <a href="{{route('category.index')}}"><<<<-----Go Back To Categories</a></label>
                            <input type="text" class="form-control" name="brand" required>
                            <input type="hidden" class="form-control" name="status" value="1">
                            <input type="hidden" class="form-control" name="category" value="{{$parent[0]->id}}">
                        </div>
                        <div class="form-group">
                        <label for="">Brand Image</label>
                            <input type="file" class="form-control" name="brand_image" required >

                        </div>

                        <button type="submit" class="btn btn-success pull-right">Add </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- END: Content-->


@endsection
