@extends('shop.admin.layouts.main')

@section('content')
<div class="content-header row">
</div>
<div class="content-overlay"></div>
<div class="content-wrapper">
    <div class="content-body">

        <!-- Material Data Tables -->
        <section id="material-datatables">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-0">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-5">
                                    <h4 class="card-title">Sub Categories of  <a href="{{route('category.index')}}"><<<<-----Go Back To Categories</a></h4>
                                </div>
                                <div class="col-2">
                                    <h4 class="card-title"><a
                                            href="{{route('brand.create',['cat_id'=>$cat_id] )}}"
                                            class="btn btn-primary">ADD BRAND</a></h4>
                                </div>
                            </div>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <table class="table material-table">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Category</th>
                                            <th>Brand Image</th>
                                            <th>Status</th>
                                            <th>Actions</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @isset($brands)
                                        @foreach($brands as $brand)
                                        <tr>
                                            <td>{{$brand->id}}</td>
                                            <td>{{$brand->category}}</td>
                                            <td>image</td>
                                            <td>{{$brand->brand}}</td>
                                            @if($brand->status ==1)
                                            <td><a  class="btn btn-sm btn-primary">ACTIVE</a></td>
                                            @else
                                            <td><a  class="btn btn-sm btn-danger">INACTIVE</a></td>
                                            @endif
                                            <td>
                                                {{-- <a href="{{route('sub-category.show',$c->id)}}" class="btn btn-sm
                                                btn-primary">ADD SUBCATEGORY</a> --}}
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endisset
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Material Data Tables -->
    </div>
</div>
</div>
</div>
<!-- END: Content-->
@endsection
