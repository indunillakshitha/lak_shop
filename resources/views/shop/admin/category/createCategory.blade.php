@extends('shop.admin.layouts.main')

@section('content')
<div class="content-header row">
</div>
<div class="content-overlay"></div>
<div class="content-wrapper">
    <div class="content-body">
        <div class="col-12">
            <div class="card mb-0">
                <div class="card-header">
                    <form action="{{route('category.store')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="">Category Name</label>
                            <input type="text" class="form-control" name="category" required>
                            <input type="hidden" class="form-control" name="status" value="1">
                            <input type="hidden" class="form-control" name="parent_id" value="0">
                        </div>

                        <button type="submit" class="btn btn-success pull-right">Add Category</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- END: Content-->


@endsection
