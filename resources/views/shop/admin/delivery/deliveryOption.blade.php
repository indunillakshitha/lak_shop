
@extends('shop.admin.layouts.main')

@section('content')
<div class="content-header row">
</div>
<div class="content-overlay"></div>
<div class="content-wrapper">
    <div class="content-body">

        <!-- Material Data Tables -->
        <section id="material-datatables">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-0">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-3">
                                    <h4 class="card-title">DELIVERY OPTIONS</h4>
                                </div>
                                <div class="col-3">
                                     <div class="pull-right">
                                    <h4 class="card-title"><a href="{{route('delivery-option.create')}}" class="btn btn-primary btn-glow ">ADD DELIVERY OPTION</a></h4>
                                     </div>
                                </div>
                            </div>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <table class="table material-table">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Price Per Kg</th>
                                            <th>Price Per Km</th>
                                            <th>Status</th>
                                            <th>Actions</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @isset($dos)
                                        @foreach($dos as $do)
                                        <tr>
                                            <td>{{$do->id}}</td>
                                            <td>{{$do->pricePerKg}}</td>
                                            <td>{{$do->pricePerKm}}</td>

                                            <td><input data-id="{{$do->id}}"  class="toggle-class" type="checkbox"  data-onstyle="success"
                                                data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $do->status ? 'checked' : '' }}>
                                            </td>



                                            <td>
                                                <a href="{{route('delivery-option.edit',$do->id)}}" class="btn btn-sm btn-primary">EDIT</a>
                                                <a href="#" class="btn btn-sm btn-danger">DELETE</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endisset
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Material Data Tables -->
    </div>
</div>
</div>
</div>
<script>


      $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });




    $('.toggle-class').change(function(){

        var status = $(this).prop('checked') == true ? 1 : 0;

        var user_id = $(this).data('id');

        console.log(status)
        console.log(user_id)


        $.ajax({

            type: 'GET',

            dataType: 'JSON',

            url: '{{('/delivery-option/changeStatus')}}',

            data: {'status': status, 'user_id': user_id},

            success: function(data){

              console.log(data.success)

            }

        });
    })


</script>
<!-- END: Content-->
@endsection
