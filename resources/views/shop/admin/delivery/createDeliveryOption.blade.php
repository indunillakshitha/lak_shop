@extends('shop.admin.layouts.main')

@section('content')
<div class="content-header row">
</div>
<div class="content-overlay"></div>
<div class="content-wrapper">
    <div class="content-body">
        <div class="col-12">
            <div class="card mb-0">
                <div class="card-header">
                    <form action="{{route('delivery-option.store')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="">Delivery Option</label>
                            <input type="hidden" class="form-control" name="status" value="1">
                        </div>
                        <div class="form-group">
                            <label for="">Price Per Kg</label>
                            <input type="text" class="form-control" name="pricePerKg" required>
                        </div>
                        <div class="form-group">
                            <label for="">Price Per Km</label>
                            <input type="text" class="form-control" name="pricePerKm" required>
                        </div>

                        <button type="submit" class="btn btn-success pull-right">Add Delivery Option</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- END: Content-->


@endsection
