@extends('shop.admin.layouts.main')
@section('content')


<div class="row">
    <div class=col-md-12>
        <br />
        <h3>Edit User Details</h3>
        <br />
        <form class="form form-horizontal form-bordered" method="POST" action="/create-user">
            @csrf
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="projectinput1">Name</label>
                <div class="col-md-9">
                    <input type="text" id="projectinput1" class="form-control" placeholder="Name" name="name"
                        value="">
                </div>
            </div>
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="projectinput3">E-mail</label>
                <div class="col-md-9">
                    <input type="text" id="projectinput3" class="form-control" placeholder="E-mail" name="email"
                        value="">
                </div>
            </div>
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="projectinput3">Password</label>
                <div class="col-md-9">
                <input placeholder="Enter Your Password"id="password" type="password" class="" name="password"  >
                </div>
            </div>
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="projectinput3">Phone</label>
                <div class="col-md-9">
                    <input type="text" id="projectinput3" class="form-control" placeholder="Phone" name="phone"
                        value="">
                </div>
            </div>
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="projectinput3">Gender</label>
                <div class="col-md-9">
                    <select class="select2 form-control" id="gender" name="gender">
                        <option value="1">Male</option>
                        <option value="0">Female</option>
                    </select>
                </div>
            </div>
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="projectinput3">Verified Phone</label>
                <div class="col-md-9">
                    <div for="input-16" class="alert alert-normal">
                        <input type="checkbox" id="input-16" value='1' name="verifiPhn" >
                    </div>
                </div>
            </div>
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="projectinput3">Verified E-Mail</label>
                <div class="col-md-9">
                    <div for="input-16" class="alert alert-normal">
                        <input type="checkbox" id="input-16" value='1' name="verifiEml" >
                    </div>
                </div>
            </div>
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="projectinput3">Blocked</label>
                <div class="col-md-9">
                    <div for="input-16" class="alert alert-normal">
                        <input type="checkbox" name="blocked" id="input-16" value='1' >
                    </div>
                </div>
            </div>
            <h4 class="form-section"><i class="ft-clipboard"></i>Roles</h4>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-danger">
                            <input type="checkbox" id="input-16" name="super-admin" value='super-admin'>
                            <label for="input 16">&nbsp; Super-Admin </label>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-danger">
                            <input type="checkbox" id="input-16" name="admin" value='admin' >
                            <label for="input 16">&nbsp; Admin </label>
                        </div>
                    </fieldset>
                </div>
            </div>


            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-danger">
                            <input type="checkbox" id="input-16" name="moderator" value='moderator'>
                            <label for="input 16">&nbsp; Modaretor </label>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-danger">
                            <input type="checkbox" id="input-16" name="seller" value='seller' >
                            <label for="input 16">&nbsp; Seller </label>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-danger">
                            <input type="checkbox" id="input-16" name="user" value='user' >
                            <label for="input 16">&nbsp; User </label>
                        </div>
                    </fieldset>
                </div>
            </div>

            <h4 class="form-section"><i class="ft-clipboard"></i>Permissions</h4>



            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="list-permission" value="list-permission" >
                            <label for="input 16">&nbsp; List Permission </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="create-permission" value="create-permission" >
                            <label for="input 16">&nbsp; Create Permission </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="update-permission" value="update-permission" >
                            <label for="input 16">&nbsp; Update Permission </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="delete-permission" value="delete-permission" >
                            <label for="input 16">&nbsp; Delete Permission </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="list-role" value="list-role" >
                            <label for="input 16">&nbsp; List Role </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="create-role" value="create-role" >
                            <label for="input 16">&nbsp; Create Role </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="update-role" value="update-role" >
                            <label for="input 16">&nbsp; Update Role </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="delete-role" value="delete-role" >
                            <label for="input 16">&nbsp; Delete Role </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="access-admin-panel" value="access-admin-panel">
                            <label for="input 16">&nbsp; Access Admin Panel </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="access-dashboard" value="access-dashboard" >
                            <label for="input 16">&nbsp; Access Dashboard </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="update-settings" value="update-settings">
                            <label for="input 16">&nbsp; Update Settings </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="maintenace" value="maintenace" >
                            <label for="input 16">&nbsp; Maintenace </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="list-post" value="list-post" >
                            <label for="input 16">&nbsp; List Post </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="update-post" value="update-post" >
                            <label for="input 16">&nbsp; Update Posts </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="list-users" value="list-users" >
                            <label for="input 16">&nbsp; List Users </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="verification-handling"
                                value="verification-handling" >
                            <label for="input 16">&nbsp; Verfication Handling </label>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="form-actions">
                <button type="button" class="btn btn-warning mr-1">
                    <i class="ft-x"></i> Cancel
                </button>
                <button type="submit" class="btn btn-primary">
                    <i class="la la-check-square-o"></i> Save
                </button>
            </div>
        </form>
    </div>
</div>
</div>

@endsection
