@extends('shop.admin.layouts.main')
@section('content')
<div class="content-header row">
</div>
<div class="content-overlay"></div>
<div class="content-wrapper">
    <div class="content-body">

        <!-- Material Data Tables -->
        <section id="material-datatables">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-0">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-2">
                                    <h2 class="card-title">User Activity</h2>
                                </div>

                            </div>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <table class="table material-table">
                                    <thead>
                                        <tr>
                                            <th>Date & Time</th>
                                            <th>Subject</th>
                                            <th>URL</th>
                                            <th>Method</th>
                                            <th>IP Address</th>
                                            <th>Browser</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($activityLog as $activity)
                                        <tr>
                                            <td>{{$activity->created_at}}</td>
                                            <td>{{$activity->subject}}</td>
                                            <td>{{$activity->url}}</td>
                                            <td>{{$activity->method}}</td>
                                            <td>{{$activity->ip}}</td>
                                            <td>{{$activity->agent}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Material Data Tables -->
    </div>
</div>
</div>
</div>


@endsection
