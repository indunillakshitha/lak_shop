@extends('shop.admin.layouts.main')
@section('content')


<div class="row">
    <div class="col-md-12 card">
        <br />
        <h3>Edit User Details</h3>
        <br />
        <form class="form form-horizontal form-bordered" method="POST" action="/update-user/{{$user->id}}">
            @csrf
            <!-- <div class="form-body"> -->
                <h4 class="form-section"><i class="ft-user"></i> Personal Info</h4>
                <div class="form-group row mx-auto">
                    <label class="col-md-3 label-control" for="projectinput2" ">ID</label>
                        <div class=" col-md-9">
                        <input type="text" id="projectinput2" class="form-control" placeholder="ID" name="id"
                            value="{{$user->id}}" readonly>
                </div>
            </div>

            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="projectinput1">Name</label>
                <div class="col-md-9">
                    <input type="text" id="projectinput1" class="form-control" placeholder="Name" name="name"
                        value="{{$user->name}}">
                </div>
            </div>
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="projectinput3">E-mail</label>
                <div class="col-md-9">
                    <input type="text" id="projectinput3" class="form-control" placeholder="E-mail" name="email"
                        value="{{$user->email}}">
                </div>
            </div>
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="projectinput3">Phone</label>
                <div class="col-md-9">
                    <input type="text" id="projectinput3" class="form-control" placeholder="Phone" name="phone"
                        value="{{$user->phone}}">
                </div>
            </div>
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="projectinput3">Gender</label>
                <div class="col-md-9">
                    <select class="select2 form-control" id="gender" name="gender">
                        <option value="1" @if($user->gender_id=='1') selected @endif>Male</option>
                        <option value="0" @if($user->gender_id=='0') selected @endif>Female</option>
                    </select>
                </div>
            </div>
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="projectinput3">Verified Phone</label>
                <div class="col-md-9">
                    <div for="input-16" class="alert alert-normal">
                        <input type="checkbox" id="input-16" value='1' name="verifiPhn" @if($user->verified_phone == '1')
                        checked
                        @endif>
                    </div>
                </div>
            </div>
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="projectinput3">Verified E-Mail</label>
                <div class="col-md-9">
                    <div for="input-16" class="alert alert-normal">
                        <input type="checkbox" id="input-16" value='1' name="verifiEml" @if($user->verified_email == '1')
                        checked
                        @endif>
                    </div>
                </div>
            </div>
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="projectinput3">Blocked</label>
                <div class="col-md-9">
                    <div for="input-16" class="alert alert-normal">
                        <input type="checkbox" name="blocked" id="input-16" value='1' @if($user->blocked == '1')
                        checked
                        @endif
                        >
                    </div>
                </div>
            </div>
            <h4 class="form-section"><i class="ft-clipboard"></i>Roles</h4>
            @foreach($userRole as $role)
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-danger">
                            <input type="checkbox" id="input-16" name="super-admin" value='super-admin'
                                @if($role=='super-admin' ) checked @endif>
                            <label for="input 16">&nbsp; Super-Admin </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            @endforeach
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-danger">
                            <input type="checkbox" id="input-16" name="admin" value='admin' @if($role=='admin' )
                                checked @endif>
                            <label for="input 16">&nbsp; Admin </label>
                        </div>
                    </fieldset>
                </div>
            </div>


            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-danger">
                            <input type="checkbox" id="input-16" name="moderator" value='moderator'
                                @if($userRole=='moderator' ) checked @endif>
                            <label for="input 16">&nbsp; Modaretor </label>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-danger">
                            <input type="checkbox" id="input-16" name="seller" value='seller' @if($userRole=='seller' )
                                checked @endif>
                            <label for="input 16">&nbsp; Seller </label>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-danger">
                            <input type="checkbox" id="input-16" name="user" value='user' @if($userRole=='user' )
                                checked @endif>
                            <label for="input 16">&nbsp; User </label>
                        </div>
                    </fieldset>
                </div>
            </div>

            <h4 class="form-section"><i class="ft-clipboard"></i>Permissions</h4>



            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="list-permission" value="list-permission" @php
                                if(in_array('list-permission', $perm_arr)) echo 'checked' @endphp>
                            <label for="input 16">&nbsp; List Permission </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="create-permission" value="create-permission" @php
                                if(in_array('create-permission', $perm_arr)) echo 'checked' @endphp>
                            <label for="input 16">&nbsp; Create Permission </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="update-permission" value="update-permission" @php
                                if(in_array('update-permission', $perm_arr)) echo 'checked' @endphp>
                            <label for="input 16">&nbsp; Update Permission </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="delete-permission" value="delete-permission" @php
                                if(in_array('delete-permission', $perm_arr)) echo 'checked' @endphp>
                            <label for="input 16">&nbsp; Delete Permission </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="list-role" value="list-role" @php
                                if(in_array('list-role', $perm_arr)) echo 'checked' @endphp>
                            <label for="input 16">&nbsp; List Role </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="create-role" value="create-role" @php
                                if(in_array('create-role', $perm_arr)) echo 'checked' @endphp>
                            <label for="input 16">&nbsp; Create Role </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="update-role" value="update-role" @php
                                if(in_array('update-role', $perm_arr)) echo 'checked' @endphp>
                            <label for="input 16">&nbsp; Update Role </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="delete-role" value="delete-role" @php
                                if(in_array('delete-role', $perm_arr)) echo 'checked' @endphp>
                            <label for="input 16">&nbsp; Delete Role </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="access-admin-panel" value="access-admin-panel"
                                @php if(in_array('access-admin-panel', $perm_arr)) echo 'checked' @endphp>
                            <label for="input 16">&nbsp; Access Admin Panel </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="access-dashboard" value="access-dashboard" @php
                                if(in_array('accress-dashboard', $perm_arr)) echo 'checked' @endphp>
                            <label for="input 16">&nbsp; Access Dashboard </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="update-settings" value="update-settings" @php
                                if(in_array('update-setings', $perm_arr)) echo 'checked' @endphp>
                            <label for="input 16">&nbsp; Update Settings </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="maintenace" value="maintenace" @php
                                if(in_array('maintenace', $perm_arr)) echo 'checked' @endphp>
                            <label for="input 16">&nbsp; Maintenace </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="list-post" value="list-post" @php
                                if(in_array('list-post', $perm_arr)) echo 'checked' @endphp>
                            <label for="input 16">&nbsp; List Post </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="update-post" value="update-post" @php
                                if(in_array('update-post', $perm_arr)) echo 'checked' @endphp>
                            <label for="input 16">&nbsp; Update Posts </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="list-users" value="list-users" @php
                                if(in_array('list-users', $perm_arr)) echo 'checked' @endphp>
                            <label for="input 16">&nbsp; List Users </label>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card-body">
                <div class="skin skin-flat">
                    <fieldset>
                        <div for="input-16" class="alert alert-primary">
                            <input type="checkbox" id="input-16" name="verification-handling"
                                value="verification-handling" @php if(in_array('verification-handling', $perm_arr))
                                echo 'checked' @endphp>
                            <label for="input 16">&nbsp; Verfication Handling </label>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="form-actions">
                <a type="button" href="{{ URL::previous() }}" class="btn btn-warning mr-1">
                    <i class="ft-x"></i> Cancel</a>
                <button type="submit" class="btn btn-primary">
                    <i class="la la-check-square-o"></i> Save
                </button>
            </div>
        </form>
    </div>
</div>
</div>

@endsection

