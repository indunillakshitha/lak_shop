@extends('shop.admin.layouts.main')
@section('content')

<div class="row">
    <div class="col-md-12 card">
        <br />
        <h3>Create a Role</h3>
        <br />
        <form class="form form-horizontal form-bordered" method="POST" action="{{url('create-role')}}">
        @csrf

                <div class="form-body">
                    <h4 class="form-section"><i class="ft-user"></i> Role Info</h4>
                    <div class="form-group row mx-auto">
                        <label class="col-md-3 label-control" for="roleName" ">Role Name</label>
                        <div class="col-md-9">
                            <input type="text" id="roleName" class="form-control" placeholder="ex: admin, editor, seller" name="roleName" value="">
                        </div>
                    </div>

                    <h4 class="form-section"><i class="ft-clipboard"></i>Permissions</h4>

                    <div class="card-body">
                        <div class="skin skin-flat">
                            <fieldset>
                                <div for="input-16" class="alert alert-primary">
                                    <input type="checkbox" id="switcherySize2" class="switchery-sm" name="list-permission" value="list-permission" >
                                    <label for="input 16">&nbsp; List Permission </label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="skin skin-flat">
                            <fieldset>
                                <div for="input-16" class="alert alert-primary">
                                    <input type="checkbox" id="switcherySize2" class="switchery-sm" name="create-permission" value="create-permission" >
                                    <label for="input 16">&nbsp; Create Permission</label>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="skin skin-flat">
                            <fieldset>
                                <div for="input-16" class="alert alert-primary">
                                    <input type="checkbox" id="switcherySize2" class="switchery-sm" name="update-permission" value="update-permission" >
                                    <label for="input 16">&nbsp; Update Permission </label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="skin skin-flat">
                            <fieldset>
                                <div for="input-16" class="alert alert-primary">
                                    <input type="checkbox" id="switcherySize2" name="delete-permission" value="delete-permission"  >
                                    <label for="input 16">&nbsp; Delete Permission </label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="skin skin-flat">
                            <fieldset>
                                <div for="input-16" class="alert alert-primary">
                                    <input type="checkbox" id="switcherySize2" class="switchery-sm" name="list-role" value="list-role"  >
                                    <label for="input 16">&nbsp; List Role </label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="skin skin-flat">
                            <fieldset>
                                <div for="input-16" class="alert alert-primary">
                                    <input type="checkbox" id="switcherySize2" class="switchery-sm" name="create-role" value="create-role"  >
                                    <label for="input 16">&nbsp; Create Role </label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="skin skin-flat">
                            <fieldset>
                                <div for="input-16" class="alert alert-primary">
                                    <input type="checkbox" id="switcherySize2" class="switchery-sm" name="update-role" value="update-role"  >
                                    <label for="input 16">&nbsp; Update Role </label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="skin skin-flat">
                            <fieldset>
                                <div for="input-16" class="alert alert-primary">
                                    <input type="checkbox" id="switcherySize2" class="switchery-sm" name="delete-role" value="delete-role"  >
                                    <label for="input 16">&nbsp; Delete Role </label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="skin skin-flat">
                            <fieldset>
                                <div for="input-16" class="alert alert-primary">
                                    <input type="checkbox" id="switcherySize2" class="switchery-sm" name="access-admin-panel" value="access-admin-panel"  >
                                    <label for="input 16">&nbsp; Access Admin Panel </label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="skin skin-flat">
                            <fieldset>
                                <div for="input-16" class="alert alert-primary">
                                    <input type="checkbox" id="switcherySize2" class="switchery-sm" name="access-dashboard" value="access-dashboard"  >
                                    <label for="input 16">&nbsp; Access Dashboard </label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="skin skin-flat">
                            <fieldset>
                                <div for="input-16" class="alert alert-primary">
                                    <input type="checkbox" id="switcherySize2" class="switchery-sm" name="update-settings" value="update-settings"  >
                                    <label for="input 16">&nbsp; Update Settings </label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="skin skin-flat">
                            <fieldset>
                                <div for="input-16" class="alert alert-primary">
                                    <input type="checkbox" id="switcherySize2" class="switchery-sm" name="maintenace" value="maintenace"  >
                                    <label for="input 16">&nbsp; Maintenace </label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="skin skin-flat">
                            <fieldset>
                                <div for="input-16" class="alert alert-primary">
                                    <input type="checkbox" id="switcherySize2" class="switchery-sm" name="list-post" value="list-post"  >
                                    <label for="input 16">&nbsp; List Post </label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="skin skin-flat">
                            <fieldset>
                                <div for="input-16" class="alert alert-primary">
                                    <input type="checkbox" id="switcherySize2" class="switchery-sm" name="update-post" value="update-post"  >
                                    <label for="input 16">&nbsp; Update Post </label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="skin skin-flat">
                            <fieldset>
                                <div for="input-16" class="alert alert-primary">
                                    <input type="checkbox" id="switcherySize2" class="switchery-sm" name="list-users" value="list-users"  >
                                    <label for="input 16">&nbsp; List Users</label>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="skin skin-flat">
                            <fieldset>
                                <div for="input-16" class="alert alert-primary">
                                    <input type="checkbox" id="switcherySize2" class="switchery-sm" name="verification-handling" value="verification-handling"  >
                                    <label for="input 16">&nbsp; Verification Handling </label>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button type="button" class="btn btn-warning mr-1">
                            <i class="ft-x"></i> Cancel
                        </button>
                        <button type="submit" class="btn btn-primary" >
                            <i class="la la-check-square-o"></i> Save
                        </button>
                    </div>
            </form>
        </div>
    </div>
</div>


@endsection
