@extends('shop.admin.layouts.main')
@section('content')

<div class="row">
    <div class="col-md-12 card">
        <br />
        <h3>Create a Role</h3>
        <br />
        <form class="form form-horizontal form-bordered" method="POST" action="{{url('create-permission')}}">
        @csrf

            <div class="form-body">
                <h4 class="form-section"><i class="ft-user"></i> Add a New Permission</h4>
                <div class="form-group row mx-auto">
                    <label class="col-md-3 label-control" for="roleName" ">Permission Name</label>
                    <div class="col-md-9">
                        <input type="text" id="permissionName" class="form-control" placeholder="ex:" name="permissionName" value="">
                    </div>
                </div>
                <div class="form-actions">
                <button type="button" class="btn btn-warning mr-1">
                    <i class="ft-x"></i> Cancel
                </button>
                <button type="submit" class="btn btn-primary" >
                    <i class="la la-check-square-o"></i> Save
                </button>
            </div>
        </form>
    </div>
</div>




@endsection
