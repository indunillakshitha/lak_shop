@extends('shop.admin.layouts.main')
@section('content')
<div class="row">
        <div class="col-md-12 card">
            <br />
            <h3>Permissions</h3>
            <br />
            <a type="button" class="btn btn-success mr-1 mb-1" href='add-permissions'>Add Permission</a>
            <div class="table-responsive">
                <table class="table mb-0">
                    <thead class="bg-yellow bg-lighten-4">
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Gurd-Name</th>
                        </tr>
                    </thead>
                    @foreach($permissions as $permission)
                    <tbody>
                        <tr>
                           <td>{{$permission->id}}</td>
                            <td>{{$permission->name}}</td>
                            <td>{{$permission->guard_name}}</td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
            </div>
        </div>
</div>

@endsection
