@extends('shop.admin.layouts.main')
@section('content')


<div class="row">
    <div class="col-md-12 card">
        <br />
        <h3>Edit Ad</h3>
        <br />
        <form class="form form-horizontal form-bordered" method="POST" action="/update-ad/{{ $ad[0]->id }}">
            @csrf

                <h4 class="form-section"><i class="ft-clipboard"></i>Selling Item  Details</h4>

                <div class="form-group row mx-auto">
                    <label class="col-md-3 label-control" for="projectinput3">Category</label>
                    <div class="col-md-9">
                        <select class="select2 form-control" id="category" name="category">
                            <option value="1">Male</option>
                            <option value="0">Female</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row mx-auto">
                    <label class="col-md-3 label-control" for="projectinput3">Sub Category</label>
                    <div class="col-md-9">
                        <select class="select2 form-control" id="sub_category" name="sub_category">
                            <option value="1">Male</option>
                            <option value="0">Female</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row mx-auto">
                         <label class="col-md-3 label-control" for="title" ">Title</label>
                        <div class=" col-md-9">
                            <input type="text" id="title" class="form-control"  name="title"
                                value="{{ $ad[0]->title }}" >
                        </div>
                </div>

                <div class="form-group row mx-auto">
                    <label class="col-md-3 label-control" for="subtitle" ">Subtitle</label>
                    <div class=" col-md-9">
                        <input type="text" id="subtitle" class="form-control" name="subtitle" value="{{ $ad[0]->subtitle }}">
                    </div>
                </div>

            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="projectinput1">Item Description</label>
                <div class="col-md-9">
                    <input type="text" id="item_description" class="form-control"  name="item_description"
                        value="{{ $ad[0]->item_description }}">
                </div>
            </div>
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="price">Price</label>
                <div class="col-md-9">
                    <input type="text" id="price" class="form-control"  name="price"
                        value="{{ $ad[0]->price }}">
                </div>
            </div>
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="projectinput3">Images</label>
            </div>

            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="projectinput3">Block Ad</label>
                <div class="col-md-9">
                    <div for="blocked" class="alert alert-normal">
                        <input type="checkbox" id="blocked" value='1' name="blocked" @if( $ad[0]->blocked ==1) checked
                        @endif>
                    </div>
                </div>
            </div>
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="projectinput3">Reviewed</label>
                <div class="col-md-9">
                    <div for="reviewed" class="alert alert-normal">
                        <input type="checkbox" id="reviewed" value='1' name="reviewed" >
                    </div>
                </div>
            </div>



            <h4 class="form-section"><i class="ft-user"></i>Seller Details</h4>

            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="first_name" ">Seller Name</label>
             <div class=" col-md-9">
                    <input type="text" id="first_name" class="form-control"  name="first_name" value="{{ $ad[0]->first_name }}" readonly>
            </div>
            </div>
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="shop_name" ">Shop Name</label>
             <div class=" col-md-9">
                    <input type="text" id="shop_name" class="form-control"  name="id" value="{{ $ad[0]->shop_name }}" readonly>
            </div>
            </div>
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="mobile" ">Mobile</label>
                         <div class=" col-md-9">
                    <input type="text" id="mobile" class="form-control" name="mobile" value="{{ $ad[0]->mobile }}" readonly>
            </div>
            </div>

            <div class="form-actions">
                <a type="button" href="{{ URL::previous() }}" class="btn btn-warning mr-1">
                    <i class="ft-x"></i> Cancel</a>
                <button type="submit" class="btn btn-primary">
                    <i class="la la-check-square-o"></i> Save
                </button>
            </div>
        </form>
    </div>
</div>
</div>

@endsection
