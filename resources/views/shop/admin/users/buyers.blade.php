
@extends('shop.admin.layouts.main')

@section('content')
<div class="content-header row">
</div>
<div class="content-overlay"></div>
<div class="content-wrapper">
    <div class="content-body">

        <!-- Material Data Tables -->
        <section id="material-datatables">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-0">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-2">
                                    <h4 class="card-title">Buyers</h4>
                                </div>

                            </div>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <table class="table material-table">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Email</th>

                                            <th>Verified Email </th>
                                            <th>Reviewed</th>

                                            <th>Action</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @isset($buy)
                                        @foreach($buy as $b)
                                        <tr>
                                            <td>{{$b->id}}</td>
                                            <td>{{$b->name}}</td>
                                            <td>{{$b->email}}</td>


                                            <td><input data-id="{{$b->id}}"  id="email" class="toggle-class" type="checkbox"  data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $b->email_verified_at ? 'checked' : '' }}>
                                            </td>
                                            <td><input data-id="{{$b->id}}"  id="reviewed" class="toggle-class" type="checkbox"  data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $b->about ? 'checked' : '' }}>
                                            </td>



                                            <td>
                                                 <a href="#" class="btn btn-sm btn-primary">DETAILS</a>
                                                 <a href="#" class="btn btn-sm btn-secondary">EDIT</a>
                                                <a href="#" class="btn btn-sm btn-danger">DELETE</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endisset
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Material Data Tables -->
    </div>
</div>
</div>
</div>


<script>


      $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });



    $(email ).change(function(){

        var status = $(this).prop('checked') == true ? 1 : 0;

        var user_id = $(this).data('id');

        console.log(status)
        console.log(user_id)


        $.ajax({

            type: 'GET',

            dataType: 'JSON',

            url: '{{('/buyers/view/changeStatusEmail')}}',

            data: {'status': status, 'user_id': user_id},

            success: function(data){

              console.log(data.success)

            }

        });
    })

    $(reviewed ).change(function(){

        var status = $(this).prop('checked') == true ? 1 : 0;

        var user_id = $(this).data('id');

        console.log(status)
        console.log(user_id)


        $.ajax({

            type: 'GET',

            dataType: 'JSON',

            url: '{{('/buyers/view/changeStatusReviewed')}}',

            data: {'status': status, 'user_id': user_id},

            success: function(data){

              console.log(data.success)

            }

        });
    })


</script>
@endsection

