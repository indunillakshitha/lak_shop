@extends('shop.admin.layouts.main')
@section('content')
<div class="content-header row">
</div>
<div class="content-overlay"></div>
<div class="content-wrapper">
    <div class="content-body">

        <!-- Material Data Tables -->
        <section id="material-datatables">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-0">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-2">
                                    <h3 >Seller Income Details</h3>
                                </div>
                                <div class="col-2">
                                    <div class="pull-right">
                                    </div>
                                </div>
                            </div>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <table class="table material-table">
                                    <thead>
                                        <tr>
                                            <th>Seller ID</th>
                                            <th>Shop Name</th>
                                            <th>Seller Name</th>
                                            <th>Seller NIC</th>
                                            <th>Joined</th>
                                            <th>Address</th>
                                            <th>Option</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($sellers as $seller)
                                        <tr>
                                            <td>{{ $seller->id }}</td>
                                            <td>{{ $seller->shop_name }}</td>
                                            <td>{{ $seller->first_name }} {{ $seller->last_name }}</td>
                                            <td>{{ $seller->nic }}</td>
                                            <td>{{ $seller->created_at }}</td>
                                            <td>{{ $seller->address }}</td>
                                            <td>
                                            <a type="button" href="/seller-transactions/{{$seller->id}}"  class="btn btn-sm btn-primary">Transactions</a>
                                            </td>

                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Material Data Tables -->
    </div>
</div>
</div>
</div>
@endsection
