@include('shop.admin.layouts.header')
@include('shop.admin.layouts.nav')
@include('shop.admin.layouts.leftmenu')

<div class="app-content content">
    <div class="container my-3">
    @yield('content')
</div>
</div>


@include('shop.admin.layouts.footer')
