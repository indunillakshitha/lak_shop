<!-- BEGIN: Main Menu-->

<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="active"><a href="/admin"><i class="la la-home"></i><span class="menu-title"
                        data-i18n="eCommerce Dashboard">Admin Dashboard</span></a>
            </li>
            <li class=" navigation-header"><span data-i18n="Ecommerce">Admin Panal</span><i class="la la-ellipsis-h"
                    data-toggle="tooltip" data-placement="right" data-original-title="Ecommerce"></i>
            </li>

            <li class=" nav-item"><a href="#"><i class="la la-clipboard"></i><span class="menu-title"
                        data-i18n="Invoice">Categories & Fields</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{route('category.index')}}"><i></i><span
                                data-i18n="Invoice Summary">Categories</span></a>
                    </li>
                    <li><a class="menu-item" href="{{route('fields.index')}}"><i></i><span
                                data-i18n="Invoice Template">Custom Fields</span></a>
                    </li> {{--
                    <li><a class="menu-item" href="invoice-list.html"><i></i><span data-i18n="Invoice List">Invoice List</span></a>
                    </li> --}}
                </ul>
            </li>

            <li class=" nav-item"><a href=""><i class="ft-list"></i><span class="menu-title"
                        data-i18n="Users">Users</span></a>
                <ul class="menu-content">
                    <li class=" nav-item"><a href="/user-details"><i class="la la-th-large"></i><span class="menu-title"
                                data-i18n="Shop">Users List</span></a>
                    </li>
                    <li class=" nav-item"><a href="/add-roles"><i class="la la-adjust"></i><span class="menu-title"
                                data-i18n="Product Detail">Create Roles</span></a>
                    </li>
                    <li class=" nav-item"><a href="/permissions"><i class="la la-shopping-cart"></i><span
                                class="menu-title" data-i18n="Shopping Cart">Permissions</span></a>
                </ul>
            </li>
            <li class=" nav-item"><a href="/coupon-index"><i class="la la-th-large"></i><span class="menu-title"
                        data-i18n="Shop">Coupons</span></a></li>

            <li class=" nav-item"><a href=""><i class="ft-list"></i><span class="menu-title"
                        data-i18n="Users">Ads</span></a>
                <ul class="menu-content">
                    <li class=" nav-item"><a href="/posted-ads"><i class="la la-th-large"></i><span class="menu-title"
                                data-i18n="Shop">List</span></a>
                    </li>
                    <!-- <li class=" nav-item"><a href="/add-roles"><i class="la la-adjust"></i><span class="menu-title"
                                data-i18n="Product Detail">Create Roles</span></a>
                    </li>
                    <li class=" nav-item"><a href="/permissions"><i class="la la-shopping-cart"></i><span
                                class="menu-title" data-i18n="Shopping Cart">Permissions</span></a> -->
                </ul>
            </li>
            <li class=" nav-item"><a href=""><i class="ft-list"></i><span class="menu-title"
                        data-i18n="Users">Orders</span></a>
                <ul class="menu-content">
                    <li class=" nav-item"><a href="/all-orders"><i class="la la-th-large"></i><span class="menu-title"
                                data-i18n="Shop">Orders List</span></a>
                    </li>
                    <!-- <li class=" nav-item"><a href="/add-roles"><i class="la la-adjust"></i><span class="menu-title"
                                data-i18n="Product Detail">Create Roles</span></a>
                    </li>
                    <li class=" nav-item"><a href="/permissions"><i class="la la-shopping-cart"></i><span
                                class="menu-title" data-i18n="Shopping Cart">Permissions</span></a> -->
                </ul>
            </li>

            <li class=" nav-item"><a href=""><i class="ft-list"></i><span class="menu-title"
                        data-i18n="Users">Seller</span></a>
                <ul class="menu-content">
                    <li class=" nav-item"><a href="/sellers-list"><i class="la la-th-large"></i><span class="menu-title"
                                data-i18n="Shop">Sellers List</span></a>
                    </li>
                    <!-- <li class=" nav-item"><a href="/add-roles"><i class="la la-adjust"></i><span class="menu-title"
                                data-i18n="Product Detail">Create Roles</span></a>
                    </li>
                    <li class=" nav-item"><a href="/permissions"><i class="la la-shopping-cart"></i><span
                                class="menu-title" data-i18n="Shopping Cart">Permissions</span></a> -->
                </ul>
            </li>

            <li class=" nav-item"><a href=""><i class="ft-list"></i><span class="menu-title"
                        data-i18n="Users">Income and Payments</span></a>
                <ul class="menu-content">
                    <li class=" nav-item"><a href="/sellers-income"><i class="la la-th-large"></i><span class="menu-title"
                                data-i18n="Shop">Seller Income</span></a>
                    </li>
                    <!-- <li class=" nav-item"><a href="/add-roles"><i class="la la-adjust"></i><span class="menu-title"
                                data-i18n="Product Detail">Create Roles</span></a>
                    </li>
                    <li class=" nav-item"><a href="/permissions"><i class="la la-shopping-cart"></i><span
                                class="menu-title" data-i18n="Shopping Cart">Permissions</span></a> -->
                </ul>
            </li>

            <li class="menu-item animate-dropdown"><a title="My Account" href=""><i class="ec ec-user"></i>My
                    Account</a></li>

            <li class="menu-item animate-dropdown"><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a></li>
                        <li class=" nav-item"><a href="delivery-option"><i class="la la-shopping-cart"></i><span class="menu-title" data-i18n="Shopping Cart">Dilivery Option</span></a>
                </li>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </ul>
    </div>
</div>

<!-- END: Main Menu-->
