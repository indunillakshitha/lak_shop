@extends('shop.admin.layouts.main')
@section('content')

<div class="row">
    <div class=col-md-12>
        <br />
        <h3>Create New Coupon</h3>
        <br />
        <form class="form form-horizontal form-bordered" method="POST" action="{{route('coupon.create')}}">
            @csrf
            <div class="form-body">
                <div class="form-group row mx-auto">
                    <label class="col-md-3 label-control" for="code" ">Coupon Code</label>
                    <div class="col-md-9">
                        <input type="text" id="code" class="form-control"  name="code" >
                    </div>
                </div>
                <div class="form-group row mx-auto">
                    <label class="col-md-3 label-control" for="type">Coupon Type</label>
                    <div class="col-md-9">
                        <select class="select2 form-control" id="type" name="type">
                            <option value="fixed">fixed value</option>
                            <option value="precentage">presentage</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row mx-auto">
                    <label class="col-md-3 label-control" for="value" ">Coupon value</label>
                    <div class="col-md-9">
                        <input type="text" id="value" class="form-control"  name="value" >
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" class="btn btn-warning mr-1">
                        <i class="ft-x"></i> Cancel
                    </button>
                    <button type="submit" class="btn btn-primary" >
                        <i class="la la-check-square-o"></i> Save
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>




@endsection
