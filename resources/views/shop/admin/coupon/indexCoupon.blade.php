@extends('shop.admin.layouts.main')
@section('content')
<div class="content-header row">
</div>
<div class="content-overlay"></div>
<div class="content-wrapper">
    <div class="content-body">

        <!-- Material Data Tables -->
        <section id="material-datatables">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-0">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-2">
                                    <h4 class="card-title">Coupons</h4>
                                </div>
                                <div class="col-2">
                                    <div class="pull-right">
                                        <h4 class="card-title"><a href="/create-coupon" class="btn btn-primary btn-glow ">CREATE COUPON</a>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <table class="table material-table">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Code</th>
                                            <th>Type</th>
                                            <th>Fixed</th>
                                            <th>Precentage</th>
                                            <th>Action</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($coupons as $coupon)
                                        <tr>
                                            <td>{{$coupon->id}}</td>
                                            <td>{{$coupon->code}}</td>
                                            <td>{{$coupon->type}}</td>
                                            <td>{{$coupon->fixed}}</td>
                                            <td>{{$coupon->percent_off}}</td>
                                            <td>
                                                <a class="btn btn-sm btn-primary" href="/edit-coupon/{{$coupon->id}}">Edit</a>
                                                <form action="/delete-coupon/{{ $coupon->id }}"  method="POST">
                                                    {{ csrf_field() }}
                                                    {{ method_field('Delete')}}
                                                    <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Material Data Tables -->
    </div>
</div>
</div>
</div>


@endsection
