@extends('shop.admin.layouts.main')
@section('content')

<div class="row">
    <div class=col-md-12>
        <br />
        <h3>Create New Coupon</h3>
        <br />
        <form class="form form-horizontal form-bordered" method="POST" action="/update-coupon/{{$coupon->id}}">
            @csrf
            <div class="form-body">
                <div class="form-group row mx-auto">
                    <label class="col-md-3 label-control" for="code" ">Coupon Code</label>
                    <div class="col-md-9">
                        <input type="text" id="code" class="form-control"  name="code"  value="{{$coupon->code}}">
                    </div>
                </div>
                <div class="form-group row mx-auto">
                    <label class="col-md-3 label-control" for="type">Coupon Type</label>
                    <div class="col-md-9">
                        <select class="select2 form-control" id="type" name="type" >
                            <option value="fixed" @if($coupon->type=='fixed') selected @endif >fixed value</option>
                            <option value="precentage" @if($coupon->type=='precentage') selected @endif >presentage</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row mx-auto">
                    <label class="col-md-3 label-control" for="value" ">Coupon value</label>
                    <div class="col-md-9">
                        <input type="text" id="value" class="form-control"  name="value"
                        @if($coupon->fixed=='') value="{{$coupon->percent_off}}"
                        @elseif($coupon->percent_off=='') value="{{$coupon->fixed}}"
                        @endif>
                    </div>
                </div>
                <div class="form-actions">
                    <a type="button" href="{{ URL::previous() }}" class="btn btn-warning mr-1">
                        <i class="ft-x"></i> Cancel</a>
                    <button type="submit" class="btn btn-primary" >
                        <i class="la la-check-square-o"></i> Save
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>




@endsection
