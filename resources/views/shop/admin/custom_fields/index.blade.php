
@extends('shop.admin.layouts.main')

@section('content')
<div class="content-header row">
</div>
<div class="content-overlay"></div>
<div class="content-wrapper">
    <div class="content-body">

        <!-- Material Data Tables -->
        <section id="material-datatables">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-0">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-6">
                                    <div class="col">
                                    </div>
                                    <div class="col">
                                        <h4 class="card-title"><a href="{{route('fields.create')}}" class="btn btn-primary btn-glow ">ADD FIELD</a></h4>

                                    </div>
                                </div>

                            </div>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <table class="table material-table">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>FIELD NAME</th>
                                            <th>TYPE</th>
                                            <th>VALUES</th>
                                            <th>ACTIONS</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @isset($fields)
                                        @foreach($fields as $field)
                                        <tr>
                                            <td>{{$field->id}}</td>
                                            <td>{{$field->field_name}}</td>
                                            <td>{{$field->type}}</td>
                                            <td>{{$field->values}}</td>
                                            </td>
                                            <td>
                                                <a href="{{route('fields.edit',$field->id)}}" class="btn btn-sm btn-warning">EDIT</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endisset
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Material Data Tables -->
    </div>
</div>
</div>
</div>
<script>


      $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });




    $('.toggle-class').change(function(){

        var status = $(this).prop('checked') == true ? 1 : 0;

        var user_id = $(this).data('id');

        console.log(status)
        console.log(user_id)


        $.ajax({

            type: 'GET',

            dataType: 'JSON',

            url: '{{('/category/view/changeStatus')}}',

            data: {'status': status, 'user_id': user_id},

            success: function(data){

              console.log(data.success)

            }

        });
    })


</script>
<!-- END: Content-->
@endsection
