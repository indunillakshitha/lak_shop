@extends('shop.admin.layouts.main')

@section('content')
<div class="content-header row">
</div>
<div class="content-overlay"></div>
<div class="content-wrapper">
    <div class="content-body">
        <div class="col-12">
            <div class="card mb-0">
                <div class="card-header">
                    <form action="{{route('fields.store')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="">Field Name</label>
                            <input type="text" class="form-control" name="field_name" required>
                        </div>
                        <div class="form-group">
                            <label for="">Field Type</label>
                            <select class="custom-select form-control"
                            id="eventLocation3" name="type">
                            <option value="">Select Type</option>
                            <option value="text">Text</option>
                            <option value="textarea">Text Area</option>
                            <option value="checkbox">Checkbox</option>
                        </select>
                        </div>
                        <div class="form-group">
                            <label for="">Field Values</label>
                            <input type="text" class="form-control" name="values" >
                        </div>

                        <button type="submit" class="btn btn-success pull-right">Add Field</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- END: Content-->


@endsection
