@extends('shop.admin.layouts.main')

@section('content')
<div class="content-header row">
</div>
<div class="content-overlay"></div>
<div class="content-wrapper">
    <div class="content-body">
        <div class="col-12">
            <div class="card mb-0">
                <div class="card-header">
                    <form action="" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="">Category Name</label>
                            <input type="text" value="{{$category->category}}" class="form-control" disabled>
                            <input type="hidden" value="{{$category->id}}" name="cat_id" id="cat_id"
                                class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Already Added</label>
                            {{-- <input type="text"  class="form-control" id="added" --}}
                            <div  id="added">
                                @foreach ($fields as $field )
                            @foreach ($this_cat_fields as $tcf)
                                @if($field->id == $tcf->field_id) <span class="badge badge-primary">{{$field->field_name}}</span> @endif
                            @endforeach
                            @endforeach
                            </div>


                            {{-- > --}}

                        </div>
                        <div class="form-group">
                            <label for="">Field Values</label>
                            <div class="row">
                                @isset($fields)
                                @foreach ($fields as $field)
                                <div class="ml-5">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" value="{{$field->id}}"
                                        onclick="update(this.value)"
                                        @foreach ($this_cat_fields as $tcf)

                                        @if($field->id == $tcf->field_id) checked @endif
                                        @endforeach
                                        >
                                        {{$field->field_name}}
                                    </label>
                                </div>
                                @endforeach
                                @endisset
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- END: Content-->
<script type="text/javascript">
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

function update(id){

    $.ajax({
            type: 'GET',
            url: '{{('/addfields')}}',
            data: {
                 'cat_id' : document.getElementById('cat_id').value,
                 'field_id' :id,

            },
            success: function(data){
                console.log(data);
                Swal.fire({
                // title: ' Added',
                title :data[1],
                text: 'Successfull',
                icon: 'success',
                timer: 2000
                 })


                // document.querySelector('#added').value.innerHTML = ``
                // // data.forEach(record => {
                //             html =
                //                 `adsadasa`
                // document.querySelector('#added').value.innerHTML += html
                .then(()=> { location.reload()
                })
            // })
            },

            })
}

</script>

@endsection
