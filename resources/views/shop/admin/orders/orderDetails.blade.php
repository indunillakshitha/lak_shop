@extends('shop.admin.layouts.main')
@section('content')


<div class="row">
    <div class="col-md-12 card">
        <br />
        <h3>Edit Ad</h3>
        <br />
        <form class="form form-horizontal form-bordered" method="POST" action="">
            @csrf

                 <h4 class="form-section"><i class="ft-user"></i>Customer Details</h4>

            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="name" ">Customer Name</label>
             <div class=" col-md-9">
                    <input type="text" id="" class="form-control"  name="" value="{{$customer[0]->name}}" readonly>
            </div>
            </div>
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="shop_name" ">Phone</label>
             <div class=" col-md-9">
                    <input type="text" id="" class="form-control"  name="" value="{{$customer[0]->phone}}" readonly>
            </div>
            </div>
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="" ">Address</label>
                         <div class=" col-md-9">
                    <input type="text" id="" class="form-control" name="" value="{{$customer[0]->address}}" readonly>
            </div>
            </div>

                <h4 class="form-section"><i class="ft-clipboard"></i>Item  Details</h4>


                <div class="form-group row mx-auto">
                         <label class="col-md-3 label-control" for="title" ">Order ID</label>
                        <div class=" col-md-9">
                            <input type="text" id="" class="form-control"  name=""
                                value="{{ $order[0]->order_id }}" readonly>
                        </div>
                </div>

                <div class="form-group row mx-auto">
                         <label class="col-md-3 label-control" for="title" ">Order Status</label>
                        <div class=" col-md-9">
                            <button class="btn-md btn-info mb-1 mr-1 waves-effect waves-light" type="button">{{$order[0]->status}}</button>
                        </div>
                </div>

                <div class="form-group row mx-auto">
                         <label class="col-md-3 label-control" for="" ">Item Name</label>
                        <div class=" col-md-9">
                            <input type="text" id="" class="form-control"  name=""
                                value="{{ $order[0]->product_name }}" readonly>
                        </div>
                </div>

                <div class="form-group row mx-auto">
                    <label class="col-md-3 label-control" for="" ">Amount</label>
                    <div class=" col-md-9">
                        <input type="text" id="" class="form-control" name="" value="{{ $order[0]->amount }}"  readonly>
                    </div>
                </div>

            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="">Quantity</label>
                <div class="col-md-9">
                    <input type="text" id="" class="form-control"  name=""
                        value="{{ $order[0]->quantity }}" readonly>
                </div>
            </div>
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="">Checked Out At</label>
                <div class="col-md-9">
                    <input type="text" id="" class="form-control"  name=""
                        value="{{ $order[0]->checked_out_at }}" readonly>
                </div>
            </div>

            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="price">Delivered</label>
                <div class="col-md-9">
                    <input type="text" id="" class="form-control"  name="price"
                        value="{{ $order[0]->out_for_delivery_at }}" readonly>
                </div>
            </div>

            <div class="form-group row mx-auto">
                         <label class="col-md-3 label-control" for="title" ">Received</label>
                        <div class=" col-md-9">
                            <input type="text" id="" class="form-control"  name=""
                                value="{{ $order[0]->order_received_at }}" readonly>
                        </div>
                </div>


            <h4 class="form-section"><i class="ft-user"></i>Seller Details</h4>

            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="first_name" ">Seller Name</label>
             <div class=" col-md-9">
                    <input type="text" id="" class="form-control"  name="" value="{{$seller[0]->first_name}}" readonly>
            </div>
            </div>
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="shop_name" ">Shop Name</label>
             <div class=" col-md-9">
                    <input type="text" id="shop_name" class="form-control"  name="" value="{{$seller[0]->shop_name}}" readonly>
            </div>
            </div>
            <div class="form-group row mx-auto">
                <label class="col-md-3 label-control" for="mobile" ">Mobile</label>
                         <div class=" col-md-9">
                    <input type="text" id="mobile" class="form-control" name="mobile" value="{{$seller[0]->mobile}}" readonly>
            </div>
            </div>

            <div class="form-actions">
                <a type="button" href="{{ URL::previous() }}" class="btn btn-warning mr-1">
                    <i class="ft-x"></i> Back</a>
            </div>
        </form>
    </div>
</div>
</div>


@endsection

