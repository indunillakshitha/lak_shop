@extends('shop.admin.layouts.main')
@section('content')
<div class="content-header row">
</div>
<div class="content-overlay"></div>
<div class="content-wrapper">
    <div class="content-body">

        <!-- Material Data Tables -->
        <section id="material-datatables">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-0">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-2">
                                    <h4 class="card-title">Orders</h4>
                                </div>
                                <div class="col-2">
                                    <div class="pull-right">
                                    </div>
                                </div>
                            </div>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <table class="table material-table">
                                    <thead>
                                        <tr>
                                            <th>Shop Name</th>
                                            <th>Seller Name</th>
                                            <th>Seller NIC</th>
                                            <th>Joined Date</th>
                                            <th>Status</th>
                                            <th>Options</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($sellers as $seller)
                                        <tr>
                                            <td>{{ $seller->shop_name }}</td>
                                            <td>{{ $seller->first_name }}</td>
                                            <td>{{ $seller->nic }}</td>
                                            <td>{{ $seller->created_at }}</td>
                                            <td>
                                                <div class="action-list">
                                                    <select id="sellerStatus" class="process select1 vdroplinks "
                                                        onchange="sellerStatus(this.value, {{ $seller->id }})" >
                                                        <option data-val="1" value="1" @if( $seller->status == '1' ) selected @endif>
                                                            Activated</option >
                                                        <option data-val="0" value="0" @if( $seller->status == '0' ) selected @endif>
                                                            Deactivated</option>
                                                    </select>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-sm-3 col-6">
                                                    <div class="btn-group mr-1 mb-1">
                                                        <button type="button" class="btn btn-sm btn-info btn-round dropdown-toggle" data-toggle="dropdown"
                                                            aria-haspopup="true" aria-expanded="false">
                                                            Action
                                                        </button>
                                                        <div class="dropdown-menu open-left arrow">
                                                            <button class="dropdown-item" type="button">Login as</button>
                                                            <a class="dropdown-item" href="/seller-details/{{ $seller->id }}" type="button">Seller Details</a>
                                                            <button class="dropdown-item" type="button">Edit</button>
                                                            <button class="dropdown-item" type="button">Delete Seller</button>
                                                            <div class="dropdown-divider"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>

                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Material Data Tables -->
    </div>
</div>
</div>
</div>

@endsection
<script>

    function sellerStatus(value, seller_id) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            url: "{{url('/updateSellerStatus')}}",
            data: { 'value': value, 'seller_id': seller_id },
            success: function (data) {
                console.log(data)
            },
            error: function (error) {
                console.log(error)
            },
        });
    }


</script>
