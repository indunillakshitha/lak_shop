@extends('shop.admin.layouts.main')
@section('content')
<div class="content-overlay"></div>
<div class="content-wrapper">
    <div class="content-body">
        <section id="book-appointment">
            <div class="card">
                <div class="card-header">
                    <h2>Seller Details</h2>
                </div>
                <div class="col-2">
                    <div class="pull-left">
                        <h4 class="card-title"><a href="{{ URL::previous() }}" class="btn btn-primary btn-info ">Back</a>

                        </h4>
                    </div>
                </div>
                <div class="card-body">
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstname">Seller Name: </span></label>
                                    <input type="text" class="form-control" value="{{ $seller->first_name }}" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="lastname">Shop Name: </span></label>
                                    <input type="text" class="form-control" value="{{ $seller->shop_name }}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstname">NIC no: </span></label>
                                    <input type="text" class="form-control" value="{{ $seller->nic }}" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">Email: </span></label>
                                    <input type="email" class="form-control" name="email" id="email"
                                        value="{{ $seller->email }}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">Shop Address: </span></label>
                                    <input type="email" class="form-control" value="{{ $seller->address }}" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="phone">Contact Number:</label>
                                    <input type="number" class="form-control" id="phone" name="phone"
                                        value="{{ $seller->mobile }}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="content-overlay"></div>
                        <div class="content-wrapper">
                            <div class="content-body">

                                <!-- Material Data Tables -->
                                <section id="material-datatables">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card mb-0">
                                                <div class="card-header">
                                                    <div class="row">
                                                        <div class="col-2">
                                                            <h3>Seller's Ads</h3>
                                                        </div>
                                                        <div class="col-2">
                                                            <div class="pull-right">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a class="heading-elements-toggle"><i
                                                            class="la la-ellipsis-v font-medium-3"></i></a>
                                                    <div class="heading-elements">
                                                        <ul class="list-inline mb-0">
                                                            <li><a data-action="collapse"><i class="ft-minus"></i></a>
                                                            </li>
                                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a>
                                                            </li>
                                                            <li><a data-action="expand"><i class="ft-maximize"></i></a>
                                                            </li>
                                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="card-content collapse show">
                                                    <div class="card-body">
                                                        <table class="table material-table">
                                                            <thead>
                                                                <tr>
                                                                    <th>Ad ID</th>
                                                                    <th>Title</th>
                                                                    <th>Stock</th>
                                                                    <th>Price</th>
                                                                    <th>Sold Count</th>
                                                                    <th>Action</th>

                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($ads as $ad)
                                                                <tr>
                                                                    <td>{{ $ad->id }}</td>
                                                                    <td>{{ $ad->id }}</td>
                                                                    <td>{{ $ad->id }}</td>
                                                                    <td>{{ $ad->id }}</td>
                                                                    <td>{{ $ad->id }}</td>
                                                                    <td>{{ $ad->id }}</td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <!-- Material Data Tables -->
                            </div>
                        </div>
                </div>

            </div>


            </form>
    </div>
</div>
</section>
</div>
</div>
</div>
@endsection
