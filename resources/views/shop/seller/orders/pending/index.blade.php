@extends('shop.seller.layouts.main')

@section('content')
<div class="content-header row">
</div>
<div class="content-overlay"></div>
<div class="content-wrapper">
    <div class="content-body">

        <!-- Material Data Tables -->
        <section id="material-datatables">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-0">
                        <div class="card-header">
                            <h4 class="card-title">Pending Orders</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <table class="table material-table">
                                    <thead>
                                        <tr>
                                            <th>Item Code</th>
                                            <th>Item Name</th>
                                            <th>Qty</th>
                                            <th>Buyer Name</th>
                                            <th>Ordered</th>
                                            <th>status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @isset($pending)
                                        @foreach ($pending as $pendin)
                                        <tr>
                                            <td>{{$pendin->SKU}}</td>
                                            <td>{{$pendin->title}}</td>
                                            <td>{{$pendin->quantity}}</td>
                                            <td>{{$pendin->first_name}}</td>
                                            <td>{{$pendin->created_at}}</td>
                                            <td>{{$pendin->order_status}}</td>
                                            <td>
                                            <a href="" target="_blank" class="btn btn-primary">VIEW</a>
                                            <a href="" target="_blank" class="btn btn-primary">CANSEL</a>
                                            </td>

                                        </tr>
                                        @endforeach
                                        @endisset
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Material Data Tables -->
    </div>
</div>
</div>
</div>
<!-- END: Content-->
@endsection
