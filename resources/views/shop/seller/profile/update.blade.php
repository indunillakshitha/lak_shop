@extends('shop.seller.layouts.main')

@section('content')
<div class="content-header row">
    <div class="content-header-dark bg-img col-12">
        <div class="row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <h3 class="content-header-title white">Profile</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">View</a>
                            </li>
                            <li class="breadcrumb-item active">My Profile
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="content-header-right col-md-3 col-12">
                <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                    <button class="btn btn-primary round dropdown-toggle dropdown-menu-right box-shadow-2 px-2 mb-1"
                        id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">Action</button>
                    <div class="dropdown-menu"><a class="dropdown-item" href="component-alerts.html"> Alerts</a><a
                            class="dropdown-item" href="material-component-cards.html"> Cards</a><a
                            class="dropdown-item" href="component-progress.html"> Progress</a>
                        <div class="dropdown-divider"></div><a class="dropdown-item" href="register-with-bg-image.html">
                            Register</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-overlay"></div>
@if (session('suc'))
<div class="alert alert-info">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="material-icons">close</i>
    </button>
    <span>
        <b> Success - </b> {{session('suc')}}</span>
</div>
@elseif (session('er'))
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="material-icons">close</i>
    </button>
    <span>
        <b> Failed - </b> {{session('er')}}</span>
</div>
@endif
<div class="content-wrapper">
    <div class="content-detached ">
        <div class="content-body">
            <div class="product-shop">
                <form action="{{route('myprofile.update',([Auth::user()->id]))}}" method="POST" >
                    @csrf
                    @method('PUT')
                    <div class="card">
                        <div class="card-body">
                            <div class="">
                                <div class="row mt-5">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="firstName3">
                                                First Name :
                                                <span class="danger">*</span>
                                            </label>
                                        <input type="text" class="form-control required" value="{{$mydetails->first_name}}" id="firstName3"
                                                name="first_name">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="lastName3">
                                                Last Name :
                                                <span class="danger">*</span>
                                            </label>
                                            <input type="text" class="form-control required" value="{{$mydetails->last_name}}" id="lastName3"
                                                name="last_name">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-5">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="emailAddress5">
                                                Shop Name
                                                <span class="danger">*</span>
                                            </label>
                                            <input type="text" class="form-control required" value="{{$mydetails->shop_name}}" id="emailAddress5"
                                                name="shop_name">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="emailAddress5">
                                                Email Address :
                                                <span class="danger">*</span>
                                            </label>
                                        <input type="email" class="form-control required" value="{{Auth::user()->email}}"  id="emailAddress5"
                                                name="emailaddress">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-5">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="phoneNumber3">Phone Number :</label>
                                            <input type="tel" class="form-control" id="phoneNumber3" value="{{$mydetails->mobile}}" name="mobile">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="phoneNumber3">LP Number :</label>
                                            <input type="tel" class="form-control" id="phoneNumber3" value="{{$mydetails->mobile2}}" name="mobile2">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-5">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="proposalTitle3">
                                                Address Line 1 :
                                                <span class="danger">*</span>
                                            </label>
                                            <input type="text" class="form-control required" value="{{$mydetails->address}}" id="proposalTitle3"
                                                name="address">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="emailAddress6">
                                                Province :
                                                <span class="danger">*</span>
                                            </label>
                                            <select class="custom-select form-control required"
                                                {{-- onclick="get_provinces()" --}} id="location"  name="province_id">
                                                <option value="{{$mydetails->province_id}}">Select Province</option>

                                                @foreach ($provinces as $p)
                                                <option value="{{$p->id}}">{{$p->name_en}}</option>
                                                @endforeach
                                                {{-- <option value="">Select City</option>
                                            <option value="Amsterdam">Amsterdam</option>
                                            <option value="Berlin">Berlin</option>
                                            <option value="Frankfurt">Frankfurt</option> --}}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-5">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="videoUrl3">Distict :</label>
                                            <select class="custom-select form-control " required id="location"
                                                oninput="get_cities(this.value)" name="district_id">
                                                @foreach ($districts as $d)
                                                <option value="{{$d->id}}">{{$d->name}}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="jobTitle5">
                                                City :
                                                <span class="danger">*</span>
                                            </label>
                                            <select class="custom-select form-control " required id="city_id"
                                                name="city_id">
                                                <option value="1">Select City</option>
                                                <option value="2">Select City</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-5">
                                    <div class="col">
                                        <div class="form-group">Postal Code :</label>
                                            <select class="custom-select form-control " required id="zposta_code"
                                                name="postal_code">
                                                <option value="1">Select Postal</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="jobTitle5">
                                                City :
                                                <span class="danger">*</span>
                                            </label>
                                            <select class="custom-select form-control " required id="city_id"
                                                name="city_id">
                                                <option value="1">Select City</option>
                                                <option value="2">Select City</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="">
                                {{-- <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="eventName3">
                                                Bank Name :
                                                <span class="danger">*</span>
                                            </label>
                                        <input type="text" class="form-control required" id="eventName3" value="{{$bank->bank_name}}"
                                                name="bank_name">
                                        </div>
                                        <div class="form-group">
                                            <label for="eventType3">
                                                Bank Branch :
                                                <span class="danger">*</span>
                                            </label>
                                            <select class="custom-select form-control required" id="eventType3" value="{{$bank->bank_branch}}"
                                                name="bank_branch">
                                                <option value="Banquet">Banquet</option>

                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="eventLocation3">Branch Code :</label>
                                            <select class="custom-select form-control" id="eventLocation3"
                                                name="branch_code">
                                                <option value="not">Select City</option>

                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="eventDate">
                                                Bank Holder Name :
                                                <span class="danger">*</span>
                                            </label>

                                            <input type="text" class="form-control required" id="eventName3" value="{{$bank->bank_holder_name}}"
                                                name="bank_holder_name">
                                        </div>
                                        <div class="form-group">
                                            <label for="eventDate">
                                                Bank Account No :
                                                <span class="danger">*</span>
                                            </label>

                                            <input type="text" class="form-control required" id="eventName3"
                                                name="bank_acc_no" value="{{$bank->bank_acc_no}}">
                                        </div>

                                    </div>
                                </div> --}}
                                <input type="submit" class="btn btn-success">
                            </div>
                        </div>
                    </div>
                    <div class="row match-height">
                </form>
                <hr>
            </div>
        </div>

    </div>
</div>
<!-- END: Content-->

<script>
    window.onload = get_cities()
function get_cities(district){
    // console.log(1);
    city.innerHTML =''
    postal_code.innerHTML =''
    $.ajax({
    type: 'GET',
    url: '{{('/get-cities')}}',
    data: {
        district
    } ,
    success: function(data){
        console.log(data);

        // data.forEach(sub_cat => {
        //     html = `
        //     <a >${sub_cat.sub_category_name}</a> <br>
        //     `
        //     modal_body.innerHTML += html
        // })
        // return
        data.forEach(record => {
            city_html = `
            <option value="${record.name}">${record.name}</option>
            `
            city.innerHTML += city_html

            postal_code_html = `
            <option value="${record.postal_code}">${record.postal_code}</option>
            `
            postal_code.innerHTML += postal_code_html

        })
        // console.log(city);
        return

    }
})
}
</script>

<script src="http://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'ckeditor' );
</script>
@endsection
