@extends('shop.seller.layouts.main')

@section('content')
<div class="content-header row">
    <div class="content-header-dark bg-img col-12">
        <div class="row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <h3 class="content-header-title white">Profile</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">Change Password</a>
                            </li>
                            <li class="breadcrumb-item active">Update
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="content-header-right col-md-3 col-12">
                <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">

                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-overlay"></div>
@if (session('suc'))
<div class="alert alert-info">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="material-icons">close</i>
    </button>
    <span>
        <b> Success - </b> {{session('suc')}}</span>
</div>
@elseif (session('er'))
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="material-icons">close</i>
    </button>
    <span>
        <b> Failed - </b> {{session('er')}}</span>
</div>
@endif
<div class="content-wrapper">
    <div class="content-detached ">
        <div class="content-body">
            <div class="product-shop">
                <form action="/updatepassword" method="POST">
                    @csrf
                    <div class="card">
                        <div class="card-body">
                            <div class="">
                                <div class="row mt-5">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="firstName3">
                                                Old Password :
                                                <span class="danger">*</span>
                                            </label>
                                            <input type="text" class="form-control required" id="firstName3" name="old">
                                        </div>
                                    </div>
                                    <div class="col">

                                    </div>
                                </div>
                                <div class="row mt-5">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="emailAddress5">
                                                New PAssword
                                                <span class="danger">*</span>
                                            </label>
                                            <input type="text" class="form-control required" name="new">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="emailAddress5">
                                                Confirm New Password :
                                                <span class="danger">*</span>
                                            </label>
                                            <input type="text" class="form-control " id="emailAddress5" name="new_c">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="">
                                <input type="submit" class="btn btn-success">
                            </div>
                        </div>
                    </div>
                    <div class="row match-height">
                </form>
                <hr>
            </div>
        </div>

    </div>
</div>
<!-- END: Content-->

<script>
    <script src="http://cdn.ckeditor.com/4.14.0/standard/ckeditor.js">
</script>
<script>
    CKEDITOR.replace( 'ckeditor' );
</script>
@endsection
