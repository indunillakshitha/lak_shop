@extends('shop.seller.layouts.main')

@section('content')
<div class="content-header row">
</div>
<div class="content-overlay"></div>
<div class="content-wrapper">
    <div class="content-body">

        <!-- Material Data Tables -->
        <section id="material-datatables">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-0">
                        <div class="card-header">
                            <h4 class="card-title">My Ads</h4>
                            <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <table class="table material-table">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Item Code</th>
                                            <th>Item Name</th>
                                            <th>Image</th>
                                            <th>Available Qty</th>
                                            <th>Sold Qty</th>
                                            <th>Viewed</th>
                                            <th>Price</th>
                                            <th>status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @isset($ads)
                                        {{$i=0}}
                                        @foreach($ads as $ad)
                                        {{$i++}}
                                        <tr>
                                        <th>{{$i}}</th>
                                            <th>{{$ad->SKU}}</th>
                                            <th>{{$ad->title}} </th>
                                            <th><img src="{{asset(env('IMAGE_LOCATION').$ad->img_1)}}" width="55px"></th>
                                            <th>{{$ad->quantity}} </th>
                                            <th>{{$ad->sold_count}}</th>
                                            <th>{{$ad->view_count}}</th>
                                            <th>{{$ad->price}}</th>
                                            <th></th>
                                            <th>
                                                <a href="http://" class="btn btn-primary">DEACTIVATE</a>
                                            </th>
                                        </tr>
                                        @endforeach
                                        @endisset
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Material Data Tables -->
    </div>
</div>
</div>
</div>
<!-- END: Content-->
@endsection
