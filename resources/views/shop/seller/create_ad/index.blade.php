@extends('shop.seller.layouts.main')

@section('content')
<div class="content-header row">
    <div class="content-header-dark bg-img col-12">
        <div class="row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <h3 class="content-header-title white">Shops</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">Ads</a>
                            </li>
                            <li class="breadcrumb-item active">New Ad
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="content-header-right col-md-3 col-12">
                <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                    <button class="btn btn-primary round dropdown-toggle dropdown-menu-right box-shadow-2 px-2 mb-1"
                        id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">Action</button>
                    <div class="dropdown-menu"><a class="dropdown-item" href="component-alerts.html"> Alerts</a><a
                            class="dropdown-item" href="material-component-cards.html"> Cards</a><a
                            class="dropdown-item" href="component-progress.html"> Progress</a>
                        <div class="dropdown-divider"></div><a class="dropdown-item" href="register-with-bg-image.html">
                            Register</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-overlay"></div>
<div class="content-wrapper">
    <div class="content-detached ">
        <div class="content-body">
            <div class="product-shop">
                <div class="card">
                    <div class="card-body">
                        <div class="">
                            <form action="/submit-ad" method="POST" id="form_add" enctype="multipart/form-data">
                                @csrf
                                <div class="row mt-5">
                                    <div class="col">
                                        <div class="form-group">

                                            <label for="">Title</label>
                                            <input type="text" class="form-control" name="title">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label  for="">Subtitle</label>
                                            <input type="text" class="form-control" name="subtitle">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-5">
                                    <div class="col">
                                        <div class="form-group">
                                            <label
                                            for="">Category</label>
                                            <br>
                                            <select name="category"  id="category" class="form-control"
                                            oninput="get_subs({{$cats}}, this.value)"
                                            >
                                            <option  >Select a Category</option>
                                                @foreach ($cats as $c)
                                                @if ($c->parent_id == 0)
                                                <option  value="{{$c->id}}">{{$c->category}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="">Sub Category</label>
                                            <br>
                                            <select name="sub_category"   class="form-control"
                                            id="subs_select"
                                            oninput="get_dynamics({{$cats}}, this.value, {{$brands}})"
                                            >

                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-5">
                                    <div class="col">
                                        <div id="dynamic_area"></div>
                                    </div>
                                    <div class="col">
                                        <div id="brands_area"></div>
                                    </div>
                                </div>


                                <div id="cus_fields_area"></div>
                                {{-- <div class="form-group">
                                    <label for=""></label>
                                    <br>
                                    <select name="sub_category"   class="form-control"
                                    id="subs_select"
                                    >
                                    </select>
                                </div> --}}
                                <br>
                                <br>
                                <div class="form-group">

                                    <label for="">Condition</label>
                                    <input type="text" class="form-control" name="condition">
                                </div>
                                <div class="form-group">
                                    <label for="">Condition Description</label>

                                    <textarea class="form-control" name="condition_description"   cols="100"
                                        rows="3"></textarea>

                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="">Image 1</label>
                                            <input class="xform-control" type="file" name="imgs[]" id="img_1" >
                                            {{-- <input class="xform-control" type="file" name="img_1"  > --}}
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="">Image 2</label>
                                            <input class="xform-control" type="file" name="imgs[]"  >
                                            {{-- <input class="xform-control" type="file" name="img_2"  > --}}
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="">Image 3</label>
                                            <input class="xform-control" type="file" name="imgs[]"  >
                                            {{-- <input class="xform-control" type="file" name="img_3"  > --}}
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="">Image 4</label>
                                            <input class="xform-control" type="file" name="imgs[]"  >
                                            {{-- <input class="xform-control" type="file" name="img_4"  > --}}
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="">Image 5</label>
                                            <input class="xform-control" type="file" name="imgs[]"  >
                                            {{-- <input class="xform-control" type="file" name="img_5"  > --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label for="">Item Description</label>
                                        <textarea name="item_description" id="ckeditor" cols="30" class="form-control" rows="10"></textarea>
                                </div>
                                <div class="form-group col-6">
                                    <label for="">Price(Rs.)</label>
                                    <input class="form-control" type="number" name="price"  >
                                </div>

                                <div class="form-group">
                                    <label for="">Duration</label>
                                    <input class="form-control" type="text" name="duration"  >
                                </div>

                                <div class="form-group">
                                    <label for="">Quantity</label>
                                    <input class="form-control" type="number" name="quantity"  >
                                </div>
                                {{-- <div class="form-group">
                                    <label for="">Payment Options</label>
                                    <input class="form-control" type="number" name=""  >
                                </div> --}}

                                <a onclick="validate_form()" class="btn btn-success">Submit Ad</a>
                            </form>
                            <hr>
                        </div>
                    </div>
                </div>
                <div class="row match-height">

                </div>
            </div>

        </div>
    </div>
    <!-- END: Content-->

    <script>

        function validate_form(){
            if(category.value==='Select a Category'
            ||subs_select.value === 'Select Sub Category'
            //  || brand_select.value === 'Select Brand'
             ) {
                return Swal.fire('Please fill all fields')
            }
            if(!img_1.files[0]) {
                // console.log(img_1.files[0]);
                return Swal.fire('Please upload at least 1 image')
            }
            return form_add.submit()
        }


            var brands = {!! json_encode($brands, JSON_HEX_TAG) !!};
            var cus_fields = {!! json_encode($cus_fields, JSON_HEX_TAG) !!}
            var cats_have_fields = {!! json_encode($cats_have_fields, JSON_HEX_TAG) !!}



            // window.onload =
            function check_cats_have_fields(cat_id){
                cus_fields_area.innerHTML = null

                cats_have_fields.forEach(chf => {
                    // console.log(parseInt(cat_id) === chf.cat_id);

                    if(parseInt(cat_id) === chf.cat_id){
                        // cus_fields_area.innerHTML = null
                        return create_custom_fields(chf.field_id)
                    }
                    // cus_fields.forEach(cf => {

                    // })
                })
            }

            // window.onload = console.log(cats_have_fields);
            // window.onload = console.log(cus_fields);

            function create_custom_fields(field_id){
                console.log(field_id);
                // console.log(cat_id);
                let val_arr = []
                // cus_fields_area.innerHTML = null


                cus_fields.forEach(cf => {
                    // console.log(cf.id === field_id);
                    // console.log(cf.values.split(','));

                    if(cf.id === field_id ){

                        if(cf.values !== null){
                        val_arr = cf.values.split(',')
                        // console.log(val_arr);
                        let div = document.createElement('div')
                        div.classList.add('form-group')
                        let divLabel = document.createElement('label')
                        divLabel.appendChild(document.createTextNode(`${cf.field_name}`))
                        div.appendChild(divLabel)

                        let j = 0
                       val_arr.forEach(i => {
                        let input = document.createElement('input')
                        input.type = `${cf.type}`
                        input.name = `cus_fields[${cf.id}][]`
                        input.value = `${i}`
                        input.id = `${i}`
                        input.classList.add('form-control')

                        let label = document.createElement('label')
                        label.htmlFor = `${input.id}`
                        label.appendChild(document.createTextNode(`${i}`));

                        div.appendChild(input)
                        div.appendChild(label)
                        cus_fields_area.appendChild(div)
                        j++
                    })

                    } else{
                        let div = document.createElement('div')
                        div.classList.add('form-group')
                        let divLabel = document.createElement('label')
                        divLabel.appendChild(document.createTextNode(`${cf.field_name}`))
                        div.appendChild(divLabel)

                        let input = document.createElement('input')
                        input.type = `${cf.type}`
                        input.name = `cus_fields[${cf.id}]`
                        // input.value = `${cf.field_name}`
                        input.id = `${cf.field_name}`
                        input.classList.add('form-control')

                        // let label = document.createElement('label')
                        // label.htmlFor = `${input.id}`
                        // label.appendChild(document.createTextNode(`${cf.field_name}`));

                        div.appendChild(input)
                        // div.appendChild(label)
                        cus_fields_area.appendChild(div)
                    }

                    }

                })
            }






        function get_subs(cats, parent_id){
            let subs_arr = [];
            // console.log(cats, parent_id);
            subs_select.innerHTML ='<option >Select Sub Category</option>'
            dynamic_area.innerHTML = null
            brands_area.innerHTML = null
            cus_fields_area.innerHTML = null

            cats.forEach(cat => {
                if(cat.parent_id == parent_id){
                    subs_arr.push(cat)
                }
            })
            // console.log(subs_arr);
            subs_arr.forEach(i => {
                html = `
                <option value="${i.id}">${i.category}</option>
                `
                subs_select.innerHTML += html
            })


        }

        function get_dynamics(cats, parent_id, brands){
            // console.log(1);
            let dyn_arr = [];
            dynamic_area.innerHTML = null
            brands_area.innerHTML = null
            cus_fields_area.innerHTML = null

            cats.forEach(cat => {
                if(cat.parent_id == parent_id){
                    dyn_arr.push(cat)
                }
            })
            if(dyn_arr.length > 0){
                dynamic_area.innerHTML = `
                <label> Select More </label>
                <select name="sub_category"   class="form-control"
                oninput="print_brands(this.value)"
                                    id="dyn_select_1"
                                    >
                                    <option >Select More</option>
                                    </select>`
            }else {
                    return print_brands(parent_id)
            }

            dyn_arr.forEach(i => {
                // console.log(i);
                html = `
                <option value="${i.id}">${i.category}</option>
                `
                dyn_select_1.innerHTML += html
            })
        }

        function print_brands(cat_id){
            // console.log(cat_id);

            let filtered = brands.filter(b => {
                return b.category == cat_id
            })
            // console.log(filtered);
            if(filtered.length > 0){

                    brands_area.innerHTML = `
                <label> Select Brand </label>
                <select name="brand"   class="form-control"
                                    id="brand_select"
                                    >
                                    <option >Select Brand</option>
                                    </select>`
            brands.forEach(b => {
                if(b.category == cat_id){
                    html = `
                <option value="${b.id}">${b.brand}</option>
                `
                brand_select.innerHTML += html
                }
            })

            }
            check_cats_have_fields(cat_id)
        }
    </script>

<script src="http://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script>
CKEDITOR.replace( 'ckeditor' );
</script>
    @endsection

