<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description"
        content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords"
        content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="PIXINVENT">
    <title>Lak.lk Seller</title>
    <link rel="apple-touch-icon" href="{{asset('main/shop/admin/app-assets/images/ico/apple-icon-120.png')}}">
    {{-- <link rel="shortcut icon" type="image/x-icon" href="{{asset('main/shop/admin/app-assets/images/ico/favicon.ico')}}"> --}}
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700"
        rel="stylesheet">
    <link rel="stylesheet" type="text/css"
        href="{{asset('main/shop/admin/app-assets/fonts/material-icons/material-icons.css')}}">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css"
        href="{{asset('main/shop/admin/app-assets/vendors/css/material-vendors.min.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('main/shop/admin/app-assets/vendors/css/weather-icons/climacons.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('main/shop/admin/app-assets/fonts/meteocons/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('main/shop/admin/app-assets/vendors/css/charts/morris.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('main/shop/admin/app-assets/vendors/css/charts/chartist.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('main/shop/admin/app-assets/vendors/css/charts/chartist-plugin-tooltip.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('main/shop/admin/app-assets/css/material.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('main/shop/admin/app-assets/css/components.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('main/shop/admin/app-assets/css/bootstrap-extended.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('main/shop/admin/app-assets/css/material-extended.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('main/shop/admin/app-assets/css/material-colors.css')}}">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css"
        href="{{asset('main/shop/admin/app-assets/css/core/menu/menu-types/material-vertical-menu-modern.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('main/shop/admin/app-assets/fonts/simple-line-icons/style.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('main/shop/admin/app-assets/css/core/colors/palette-gradient.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('main/shop/admin/app-assets/css/pages/timeline.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('main/shop/admin/app-assets/css/pages/dashboard-ecommerce.css')}}">
    <!-- END: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('main/shop/admin/app-assets/css/pages/ecommerce-shop.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('main/shop/app-assets/css/core/colors/palette-noui.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('main/shop/app-assets/css/pages/ecommerce-shop.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('main/shop/app-assets/css/plugins/forms/checkboxes-radios.css')}}">
    <!-- END: Page CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('main/shop/admin/assets/css/style.css')}}">
    <!-- END: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('main/shop/admin/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    {{-- <script src="https://cdn.ckeditor.com/ckeditor5/23.1.0/inline/ckeditor.js"></script> --}}


</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern material-vertical-layout material-layout 2-columns   fixed-navbar"
    data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" onload="getData()">
