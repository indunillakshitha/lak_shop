@include('shop.seller.layouts.header')
@include('shop.seller.layouts.nav')
@include('shop.seller.layouts.leftmenu')

<!-- BEGIN: Content-->
<div class="app-content content">
    @yield('content')
</div>
</div>


@include('shop.seller.layouts.footer')
