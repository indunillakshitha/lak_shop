<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description"
        content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords"
        content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="PIXINVENT">
    <title>Seller Details</title>
    {{-- <link rel="apple-touch-icon" href="{{asset('main/shop/admin/app-assets/images/logo/logo.png')}}"> --}}
    {{-- <link rel="shortcut icon" type="image/x-icon" href="{{asset('main/shop/admin/app-assets/images/ico/favicon.ico')}}">
    --}}
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700"
        rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('main/shop/admin/app-assets/vendors/css/vendors.min.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('main/shop/admin/app-assets/vendors/css/pickers/daterange/daterangepicker.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('main/shop/admin/app-assets/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('main/shop/admin/app-assets/css/bootstrap-extended.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('main/shop/admin/app-assets/css/colors.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('main/shop/admin/app-assets/css/components.css')}}">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css"
        href="{{asset('main/shop/admin/app-assets/css/core/menu/menu-types/vertical-menu-modern.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('main/shop/admin/app-assets/css/core/colors/palette-gradient.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('main/shop/admin/app-assets/css/plugins/forms/wizard.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('main/shop/admin/app-assets/css/plugins/pickers/daterange/daterange.css')}}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('main/shop/admin/assets/css/style.css')}}">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 2-columns   fixed-navbar" data-open="click"
    data-menu="vertical-menu-modern">
    <div class="user-info text-center pb-2"><img class="user-img img-fluid e w-20
         mt-2" src="{{asset('main/shop/admin/app-assets/images/logo/logo.png')}}" alt="" />


        <!-- BEGIN: Content-->
        <div class="app-content content jus" style="margin-left: 10px">
            <div class="content-overlay"></div>
            <div class="content-wrapper">
                <div class="content-header row">
                    <!-- Form wizard with step validation section start -->
                    <section id="validation">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Please Fill Below Details</h4>
                                        <a class="heading-elements-toggle"><i
                                                class="la la-ellipsis-h font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body">
                                            <form action="{{route('sellerdetails.store')}}" method="POST"
                                                enctype="multipart/form-data" class="steps-validation wizard-circle">
                                                @csrf
                                                <!-- Step 1 -->
                                                <h6>Personal Details</h6>
                                                <fieldset>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="firstName3">
                                                                    First Name :
                                                                    <span class="danger">*</span>
                                                                </label>
                                                                <input type="text" class="form-control required"
                                                                    id="firstName3" name="first_name">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="lastName3">
                                                                    Last Name :
                                                                    <span class="danger">*</span>
                                                                </label>
                                                                <input type="text" class="form-control required"
                                                                    id="lastName3" name="last_name">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="emailAddress5">
                                                                    Shop Name
                                                                    <span class="danger">*</span>
                                                                </label>
                                                                <input type="text" class="form-control required"
                                                                    id="emailAddress5" name="shop_name">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="emailAddress5">
                                                                    Email Address :
                                                                    <span class="danger">*</span>
                                                                </label>
                                                                <input type="email" class="form-control required"
                                                                    id="emailAddress5" name="emailaddress">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="phoneNumber3">Phone Number :</label>
                                                                <input type="tel" class="form-control" id="phoneNumber3"
                                                                    name="mobile">
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="phoneNumber3">LP Number :</label>
                                                                <input type="tel" class="form-control" id="phoneNumber3"
                                                                    name="mobile2">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>

                                                <!-- Step 2 -->
                                                <h6>Location Details</h6>
                                                <fieldset>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="proposalTitle3">
                                                                    Address Line 1 :
                                                                    <span class="danger">*</span>
                                                                </label>
                                                                <input type="text" class="form-control required"
                                                                    id="proposalTitle3" name="address">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="emailAddress6">
                                                                    Province :
                                                                    <span class="danger">*</span>
                                                                </label>
                                                                <select class="custom-select form-control required"
                                                                    {{-- onclick="get_provinces()" --}} id="location"
                                                                    name="province_id">
                                                                    <option value="">Select Province</option>

                                                                    @foreach ($provinces as $p)
                                                                    <option value="{{$p->id}}">{{$p->name_en}}</option>
                                                                    @endforeach
                                                                    {{-- <option value="">Select City</option>
                                                                <option value="Amsterdam">Amsterdam</option>
                                                                <option value="Berlin">Berlin</option>
                                                                <option value="Frankfurt">Frankfurt</option> --}}
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="videoUrl3">Distict :</label>
                                                                <select class="custom-select form-control " required
                                                                    id="location" oninput="get_cities(this.value)"
                                                                    name="district_id">
                                                                    @foreach ($districts as $d)
                                                                    <option value="{{$d->id}}">{{$d->name}}</option>
                                                                    @endforeach

                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="jobTitle5">
                                                                    City :
                                                                    <span class="danger">*</span>
                                                                </label>
                                                                <select class="custom-select form-control " required
                                                                    id="city_id" name="city_id">
                                                                    <option value="1">Select City</option>
                                                                    <option value="2">Select City</option>

                                                                </select>
                                                            </div>
                                                            <div class="form-group">Postal Code :</label>
                                                                <select class="custom-select form-control " required
                                                                    id="zposta_code" name="postal_code">
                                                                    <option value="1">Select Postal</option>

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </fieldset>

                                                <!-- Step 3 -->
                                                <h6>Bank Details</h6>
                                                <fieldset>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="eventName3">
                                                                    Bank Name :
                                                                    <span class="danger">*</span>
                                                                </label>
                                                                <input type="text" class="form-control required"
                                                                    id="eventName3" name="bank_name">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="eventType3">
                                                                    Bank Branch :
                                                                    <span class="danger">*</span>
                                                                </label>
                                                                <select class="custom-select form-control required"
                                                                    id="eventType3" name="bank_branch">
                                                                    <option value="Banquet">Banquet</option>

                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="eventLocation3">Branch Code :</label>
                                                                <select class="custom-select form-control"
                                                                    id="eventLocation3" name="branch_code">
                                                                    <option value="">Select City</option>
                                                                    <option value="Amsterdam">Amsterdam</option>
                                                                    <option value="Berlin">Berlin</option>
                                                                    <option value="Frankfurt">Frankfurt</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="eventDate">
                                                                    Bank Holder Name :
                                                                    <span class="danger">*</span>
                                                                </label>

                                                                <input type="text" class="form-control required"
                                                                    id="eventName3" name="bank_holder_name">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="eventDate">
                                                                    Bank Account No :
                                                                    <span class="danger">*</span>
                                                                </label>

                                                                <input type="text" class="form-control required"
                                                                    id="eventName3" name="bank_acc_no">
                                                            </div>

                                                        </div>
                                                    </div>
                                                </fieldset>

                                                <!-- Step 4 -->
                                                <h6>Identification </h6>
                                                <fieldset>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="meetingName3">
                                                                    NIC Frontside Image :
                                                                    <span class="danger">*</span>
                                                                </label>
                                                                <input type="file" class="form-control required"
                                                                    id="meetingName3" name="nic_front_image">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="meetingLocation3">
                                                                    NIC Back Image :
                                                                    <span class="danger">*</span>
                                                                </label>
                                                                <input type="file" class="form-control required"
                                                                    id="meetingLocation3" name="nic_back_image">
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="participants3">Bank Passbook</label>
                                                                <input type="file" class="form-control required"
                                                                    id="meetingLocation3" name="img_passbook">
                                                            </div>
                                                        </div>

                                                        {{-- <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="decisions3">Decisions Reached</label>
                                                            <textarea name="decisions" id="decisions3" rows="4"
                                                                class="form-control"></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Agenda Items :</label>
                                                            <div class="c-inputs-stacked">
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" name="agenda3"
                                                                        class="custom-control-input" id="item31">
                                                                    <label class="custom-control-label" for="item31">1st
                                                                        item</label>
                                                                </div>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" name="agenda3"
                                                                        class="custom-control-input" id="item32">
                                                                    <label class="custom-control-label" for="item32">2nd
                                                                        item</label>
                                                                </div>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" name="agenda3"
                                                                        class="custom-control-input" id="item33">
                                                                    <label class="custom-control-label" for="item33">3rd
                                                                        item</label>
                                                                </div>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" name="agenda3"
                                                                        class="custom-control-input" id="item34">
                                                                    <label class="custom-control-label" for="item34">4th
                                                                        item</label>
                                                                </div>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" name="agenda3"
                                                                        class="custom-control-input" id="item35">
                                                                    <label class="custom-control-label" for="item35">5th
                                                                        item</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> --}}
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- Form wizard with step validation section end -->


                </div>
            </div>
        </div>
        <!-- END: Content-->

        <div class="sidenav-overlay"></div>
        <div class="drag-target"></div>




        <!-- BEGIN: Vendor JS-->
        <script src="{{asset('main/shop/admin/app-assets/vendors/js/vendors.min.js')}}"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="{{asset('main/shop/admin/app-assets/vendors/js/extensions/jquery.steps.min.js')}}"></script>
        <script src="{{asset('main/shop/admin/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js')}}">
        </script>
        <script src="{{asset('main/shop/admin/app-assets/vendors/js/pickers/daterange/daterangepicker.js')}}"></script>
        <script src="{{asset('main/shop/admin/app-assets/vendors/js/forms/validation/jquery.validate.min.js')}}">
        </script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="{{asset('main/shop/admin/app-assets/js/core/app-menu.js')}}"></script>
        <script src="{{asset('main/shop/admin/app-assets/js/core/app.js')}}"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="{{asset('main/shop/admin/app-assets/js/scripts/forms/wizard-steps.js')}}"></script>
        <!-- END: Page JS-->

        <script>
            window.onload = get_cities()
        function get_cities(district){
            // console.log(1);
            city.innerHTML =''
            postal_code.innerHTML =''
            $.ajax({
            type: 'GET',
            url: '{{('/get-cities')}}',
            data: {
                district
            } ,
            success: function(data){
                console.log(data);

                // data.forEach(sub_cat => {
                //     html = `
                //     <a >${sub_cat.sub_category_name}</a> <br>
                //     `
                //     modal_body.innerHTML += html
                // })
                // return
                data.forEach(record => {
                    city_html = `
                    <option value="${record.name}">${record.name}</option>
                    `
                    city.innerHTML += city_html

                    postal_code_html = `
                    <option value="${record.postal_code}">${record.postal_code}</option>
                    `
                    postal_code.innerHTML += postal_code_html

                })
                // console.log(city);
                return

            }
        })
        }
        </script>

</body>
<!-- END: Body-->

</html>
