<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bid 2 Win</title>

    <!-- Bootstrap v4.5 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- Font Awesome Icon 4.7 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <link rel="stylesheet" href="{{asset('sellerlanding/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('sellerlanding/assets/css/responsive.css')}}">

</head>

<body>

    {{-- <!-- Start - Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light pl-5 pr-5 fixed-top navbar-fixed-top">
        <a class="navbar-brand p-0" href="#">
            <h5 style="color: white;">LOGO</h5>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-bars" style="color: white;"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
            </ul>



        </div>
    </nav>
    <!-- End - Navbar --> --}}

    <!-- Start - section 1 -->
    <div class="section-1">

        <div class="row m-0">
            <div class="col-md-6 m-0 p-0">
                <img src="{{asset('sellerlanding/assets/img/section-1-bg-NEW.png')}}" alt="Image" class="section-1-bg">

                <div class="section-1-content">
                    <div class="row m-0">
                        <div class="col text-center section-1-content-txt">
                            <!-- <h1>START ONLINE SELLING WITH LAK</h1>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus culpa accusantium
                                rerum
                                itaque
                                labore dolorum eum, sunt, blanditiis voluptatem dolor quo magnam atque quisquam nulla
                                adipisci
                                placeat quod impedit consequuntur.</p> -->

                            <!-- <div class="mt-4">
                                <button class="btn btn-primary mr-4 section-1-btn">Sign In</button>
                                <a href="#" style="font-weight: 600; color: white;">Buy Packages</a>
                            </div> -->
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-6 m-0 pl-5 pr-5 text-center">
                <div class="row m-0 p-0 pt-5 pb-5 mt-5 section-1-img-row">
                    <div class="col m-0 p-0">
                        <img src="{{asset('sellerlanding/assets/img/section-1-img.png')}}" alt="Image" class="section-1-img">
                    </div>
                </div>
                <div class="row m-0 p-0">
                    <div class="col text-right p-0">
                        <a href="" class="mr-3" style="color: gray;"><i class="fa fa-facebook"></i></a>
                        <a href="" class="mr-3" style="color: gray;"><i class="fa fa-instagram"></i></a>
                        <a href="" class="mr-3" style="color: gray;"><i class="fa fa-google-plus"></i></a>
                        <a href="" style="color: gray;"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- End - section 1 -->

    <!-- Start - Section 2 -->
    <div class="section-2">
        <div class="row m-0">
            <div class="col-md-6 p-5">
                <img src="{{asset('sellerlanding/assets/img/section-2-img.png')}}" alt="Image" class="w-100 section-2-img">
            </div>
            <div class="col-md-6 p-5 section-2-txt-col">
                <h1>WHAT IS lak</h1>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga saepe et possimus totam, dignissimos
                    optio nulla eum inventore cum amet repellat molestiae tempora vero perferendis dolore adipisci eaque
                    quibusdam quisquam! Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet placeat quibusdam
                    magnam ea exercitationem cupiditate nemo. Ex voluptate, cum nobis laborum, ea eius, dolor repellat
                    odio rerum accusantium quis at! Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam
                    perferendis fugiat labore optio veniam reprehenderit ex amet ducimus provident autem nobis itaque
                    eligendi, voluptas debitis porro vitae suscipit aut nostrum!</p>

                <!-- <button class="btn btn-primary mr-4">Sign In</button>
                <a href="#" style="font-weight: 600;">Buy Packages</a> -->
            </div>
        </div>
    </div>
    <!-- End - Section 2 -->

    <!-- Start - Section 3 -->
    <div class="section-3 p-5">
        <div class="row m-0">
            <div class="col m-0 text-center">
                <h5 style="color: #2659EE;">WHAT YOU WILL GET</h5>
                <h1>OUR BEST FEATURES</h1>
            </div>
        </div>

        <div class="row m-0 mt-4 text-center">

            <div class="card-deck m-0">
                <div class="card border-0 section-3-card">
                    <div class="card-body">
                        <img src="{{asset('sellerlanding/assets/img/section-3-security.png')}}" alt="Image" class="w-100 section-3-card-img">
                        <h5 class="mt-3">Card title</h5>
                        <p>This is a longer card with supporting text below as a natural lead-in to
                            additional content. This content is a little bit longer.</p>
                    </div>
                </div>
                <div class="card border-0 section-3-card">
                    <div class="card-body">
                        <img src="{{asset('sellerlanding/assets/img/section-3-speed.png')}}" alt="Image" class="w-100 section-3-card-img">
                        <h5 class="mt-3">Card title</h5>
                        <p>This is a longer card with supporting text below as a natural lead-in to
                            additional content. This content is a little bit longer.</p>
                    </div>
                </div>
                <div class="card border-0 section-3-card">
                    <div class="card-body">
                        <img src="{{asset('sellerlanding/assets/img/section-3-accessibility.png')}}" alt="Image" class="w-100 section-3-card-img">
                        <h5 class="mt-3">Card title</h5>
                        <p>This is a longer card with supporting text below as a natural lead-in to
                            additional content. This content is a little bit longer.</p>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <!-- End - Section 3 -->

    <!-- Start - Section 4 -->
    <div class="section-4 mt-5">

        <div class="row m-0">
            <div class="col-md-6 m-0 p-5 section-4-txt-col">
                <h1>Join With Lak.lk Seller Community</h1>
                <P class="mb-4">We offers to consumers the largest variety of assortment from reliable local sellers who create a delightful shopping experience</P>

            <a href="/seller/reg" class="btn btn-primary mr-4">Register to sell on Lak.lk</a>

            </div>

            <div class="col-md-6 m-0 p-1">
                <img src="{{asset('sellerlanding/assets/img/section-4-img.png')}}" alt="Image" class="w-100 section-4-img">
            </div>
        </div>
    </div>
    <!-- End - Section 4 -->





    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
    </script>

    <script src="{{asset('sellerlanding/assets/js/template.js')}}"></script>
</body>

</html>
