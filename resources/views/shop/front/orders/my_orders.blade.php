@extends('shop.front.layouts.main')

@section('content')
<div class="container my-5">
    <div class="card">
        <div class="card-header">
            <h1>My Orders</h1>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <th></th>
                    <th>Order ID</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Total</th>
                    <th></th>
                </thead>
                <tbody>
                    @php $i=1 @endphp
                    @foreach ($orders as $order)
                    <tr>
                        <td> {{$i}} </td>
                        <td> {{$order->order_id}} </td>
                        <td> {{$order->created_at}} </td>
                        <td> {{$order->status}} </td>
                        <td> Rs. {{$order->total}} for {{$order->quantity}} items</td>
                        <td> <a href="/view-order/{{$order->order_id}}" class="btn btn-primary">View Order</a> </td>
                        @php $i++ @endphp
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
