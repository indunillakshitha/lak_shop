@extends('shop.front.layouts.main')

@section('content')
<div class="container my-5">
    <div class="card">
        <div class="card-header">
            <h1>My Orders</h1>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <th>Product</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Total</th>
                </thead>
                <tbody>
                    @foreach ($orders as $order)
                    <tr>
                        <td> {{$order->product_name}} </td>
                        <td> {{($order->amount/$order->quantity)}} </td>
                        <td> {{$order->quantity}} </td>
                        <td> {{$order->amount}} </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
