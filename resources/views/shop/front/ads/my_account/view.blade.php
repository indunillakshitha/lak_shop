@extends('shop.front.layouts.main')

@section('content')
<div class="container my-5">
    <div class="card">
        <div class="card-header">
            <h1>My Account</h1>
        </div>
        <div class="card-body">
            <table class="table">
                <tr class="col">
                    <th>ID</th>
                    <td> {{$user->id}} </td>
                </tr>
                <tr class="col">
                    <th>Username</th>
                    <td> {{$user->name}} </td>
                </tr>
                <tr class="col">
                    <th>Email</th>
                    <td> {{$user->email}} </td>
                </tr>
            </table>
        </div>
        <div class="card-footer">
        <a href="/edit-account/{{$user->id}}" class="btn btn-primary">Edit Details</a>
        </div>
    </div>
</div>
@endsection
