@extends('shop.front.layouts.main')

@section('content')
<div class="container my-5">
    <div class="card">
        <div class="card-header">
            <h1>Edit Account</h1>
        </div>
        <div class="card-body">
           <form action="/update-account/{{$user->id}}" method="POST">
            @csrf
            <table class="table">
                <tr class="col">
                    <th>ID</th>
                    <td> <input type="text"  value="{{$user->id}}" readonly> </td>
                </tr>
                <tr class="col">
                    <th>Username</th>
                    <td> <input type="text" name="name" value="{{$user->name}}" >  </td>
                </tr>
                <tr class="col">
                    <th>Email</th>
                    <td> <input type="email" name="email" value="{{$user->email}}" > </td>
                </tr>
            </table>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Update Details</button>
        </div>
    </form>
    </div>
</div>
@endsection
