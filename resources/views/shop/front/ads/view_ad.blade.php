@extends('shop.front.layouts.main')

@section('content')
<!-- ========== MAIN CONTENT ========== -->
<main id="content" role="main">

<br> <br> <hr>

    <div class="container">
        <!-- Single Product Body -->
        <div class="mb-14">
            <div class="row">
                <div class="col-md-6 col-lg-4 col-xl-5 mb-4 mb-md-0">
                    <div id="sliderSyncingNav" class="js-slick-carousel u-slick mb-2"
                        data-infinite="true"
                        data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-classic u-slick__arrow-centered--y rounded-circle"
                        data-arrow-left-classes="fas fa-arrow-left u-slick__arrow-classic-inner u-slick__arrow-classic-inner--left ml-lg-2 ml-xl-4"
                        data-arrow-right-classes="fas fa-arrow-right u-slick__arrow-classic-inner u-slick__arrow-classic-inner--right mr-lg-2 mr-xl-4"
                        data-nav-for="#sliderSyncingThumb">
                        @for($i =1 ; $i <= $ad->imgs_count; $i++)
                        @php
                            $img = 'img_'.$i
                        @endphp
                        <div class="js-slide">
                            <img class="img-fluid" width="720px" height="660px" src={{asset(env('IMAGE_LOCATION').$ad->$img)}} alt="Image Description">
                        </div>
                        @endfor
                    </div>

                    <div id="sliderSyncingThumb" class="js-slick-carousel u-slick u-slick--slider-syncing u-slick--slider-syncing-size u-slick--gutters-1 u-slick--transform-off"
                        data-infinite="true"
                        data-slides-show="5"
                        data-is-thumbs="true"
                        data-nav-for="#sliderSyncingNav">
                        @for($i =1 ; $i <= $ad->imgs_count; $i++)
                        @php
                            $img = 'img_'.$i
                        @endphp
                        <div class="js-slide">
                            <div class="js-slide" style="cursor: pointer;">
                                <img class="img-fluid" src={{asset(env('IMAGE_LOCATION').$ad->$img)}} alt="Image Description">
                            </div>
                        </div>
                        @endfor

                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xl-4 mb-md-6 mb-lg-0">
                    <div class="mb-2">
                        <a href="#" class="font-size-12 text-gray-5 mb-2 d-inline-block"></a>
                        <h2 class="font-size-25 text-lh-1dot2">{{$ad->title}}</h2>
                        {{-- <div class="mb-2">
                            <a class="d-inline-flex align-items-center small font-size-15 text-lh-1" href="#">
                                <div class="text-warning mr-2">
                                    <small class="fas fa-star"></small>
                                    <small class="fas fa-star"></small>
                                    <small class="fas fa-star"></small>
                                    <small class="fas fa-star"></small>
                                    <small class="far fa-star text-muted"></small>
                                </div>
                                <span class="text-secondary font-size-13">(3 customer reviews)</span>
                            </a>
                        </div> --}}
                        {{-- <a href="#" class="d-inline-block max-width-150 ml-n2 mb-2"><img class="img-fluid" src="../../assets/img/200X60/img1.png" alt="Image Description"></a> --}}
                        {{-- <div class="mb-2">
                            <ul class="font-size-14 pl-3 ml-1 text-gray-110">
                                <li>4.5 inch HD Touch Screen (1280 x 720)</li>
                                <li>Android 4.4 KitKat OS</li>
                                <li>1.4 GHz Quad Core™ Processor</li>
                                <li>20 MP Electro and 28 megapixel CMOS rear camera</li>
                            </ul>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                        <p><strong>SKU</strong>: FW511948218</p> --}}
                        {{-- {!!$ad->cus_fields!!} --}}
                        {{-- @foreach ($ad->cus_fields as $key => $value)
                            {{$key}} - {{$value}}
                        @endforeach --}}
                        {{-- @foreach ($ad->cus_fields as $item)
                            {{$item}}
                        @endforeach --}}
                            <div id="cus_fields_div"></div>
                            <p><strong>SKU</strong>: {{$ad->SKU}} </p>
                            {!!$ad->item_description!!}
                    </div>
                </div>
                <div class="mx-md-auto mx-lg-0 col-md-6 col-lg-4 col-xl-3">
                    <div class="mb-2">
                        <div class="card p-5 border-width-2 border-color-1 borders-radius-17">
                            <div class="text-gray-9 font-size-14 pb-2 border-color-1 border-bottom mb-3">Availability: <span class="text-green font-weight-bold">{{$ad->quantity}} in stock</span></div>
                            <div class="mb-3">
                                {{-- Rs.<div class="font-size-36" id="price"></div> --}}
                                <h6 class="font-size-14">Price</h6>
                            <div class="border rounded-pill py-1 w-md-60 height-35 px-3 border-color-1">
                                <div class="js-quantity row align-items-center">
                                    <div class="col">
                                        <input id="price" class="js-result form-control h-auto border-0 rounded p-0 shadow-none" type="number" value={{$ad->price}} readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <h6 class="font-size-14">Quantity</h6>
                                <!-- Quantity -->
                                <div class="border rounded-pill py-1 w-md-60 height-35 px-3 border-color-1">
                                    <div class="js-quantity row align-items-center">
                                        <div class="col">
                                            <input id="quantity" class="js-result form-control h-auto border-0 rounded p-0 shadow-none" type="number" value="1" min="1"  max="{{$ad->quantity}}"
                                            oninput="price.value = this.value*{{$ad->price}}">
                                        </div>
                                        {{-- <div class="col-auto pr-1">
                                            <a
                                            onclick="quantity.value--"
                                            class="js-minus btn btn-icon btn-xs btn-outline-secondary rounded-circle border-0" >
                                                <small class="fas fa-minus btn-icon__inner"></small>
                                            </a>
                                            <a
                                            onclick="quantity.value++"
                                            class="js-plus btn btn-icon btn-xs btn-outline-secondary rounded-circle border-0" >
                                                <small class="fas fa-plus btn-icon__inner"></small>
                                            </a>
                                        </div> --}}
                                    </div>
                                </div>
                                <!-- End Quantity -->
                            </div>
                            {{-- <div class="mb-3">
                                <h6 class="font-size-14">Color</h6>
                                <!-- Select -->
                                <select class="js-select selectpicker dropdown-select btn-block col-12 px-0"
                                    data-style="btn-sm bg-white font-weight-normal py-2 border">
                                    <option value="one" selected>White with Gold</option>
                                    <option value="two">Red</option>
                                    <option value="three">Green</option>
                                    <option value="four">Blue</option>
                                </select>
                                <!-- End Select -->
                            </div> --}}
                            <div class="mb-2 pb-0dot5">
                                <button
                                onclick="add_to_cart({{$ad->id}},quantity.value, price.value, quantity.max)"
                                class="btn btn-block btn-primary-dark-w btn-wide transition-3d-hover"><i class="ec ec-add-to-cart mr-2 font-size-20"></i> Add to Cart</button>
                            </div>
                            <div class="mb-3">
                                <a href="#" class="btn btn-block btn-dark">Buy Now</a>
                            </div>
                            <div class="flex-content-center flex-wrap">
                                <a
                                onclick="add_to_wishlist({{$ad->id}})"
                                 class="font-size-13 mr-2 btn btn-primary"><i class="ec ec-favorites mr-1 font-size-15"></i> Wishlist</a>
                                {{-- <a href="#" class="text-gray-6 font-size-13 ml-2"><i class="ec ec-compare mr-1 font-size-15"></i> Compare</a> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Single Product Body -->
    </div>
    <div class="bg-gray-7 pt-6 pb-3 mb-6">
        <div class="container">
            <div class="js-scroll-nav">
                <div class="bg-white pt-4 pb-6 px-xl-11 px-md-5 px-4 mb-6">

                <div class="bg-white pt-4 pb-6 px-xl-11 px-md-5 px-4 mb-6 overflow-hidden">
                    <div id="Description" class="mx-md-2">
                        <div class="position-relative mb-6">
                            <ul class="nav nav-classic nav-tab nav-tab-lg justify-content-xl-center mb-6 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble border-lg-down-bottom-0 pb-1 pb-xl-0 mb-n1 mb-xl-0">
                                <li class="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                                    <a class="nav-link active" href="#Description">
                                        <div class="d-md-flex justify-content-md-center align-items-md-center">
                                            Description
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                                    <a class="nav-link" href="#Reviews">
                                        <div class="d-md-flex justify-content-md-center align-items-md-center">
                                            Reviews
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="mx-md-4 pt-1">
                            @if ($ad->item_description)
                            {!!$ad->item_description!!}
                            @else
                                <h3>No description Available</h3>
                            @endif
                            @for($i =1 ; $i <= $ad->imgs_count; $i++)
                                @php
                                    $img = 'img_'.$i
                                @endphp
                                <div class="js-slide">
                                    <img class="img-fluid" width="720px" height="660px" src={{asset(env('IMAGE_LOCATION').$ad->$img)}} alt="Image Description">
                                </div>
                                @endfor
                        </div>
                    </div>
                </div>

                <div class="bg-white py-4 px-xl-11 px-md-5 px-4 mb-6">
                    <div id="Reviews" class="mx-md-2">
                        <div class="position-relative mb-6">
                            <ul class="nav nav-classic nav-tab nav-tab-lg justify-content-xl-center mb-6 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble border-lg-down-bottom-0 pb-1 pb-xl-0 mb-n1 mb-xl-0">

                                <li class="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                                    <a class="nav-link" href="#Description">
                                        <div class="d-md-flex justify-content-md-center align-items-md-center">
                                            Description
                                        </div>
                                    </a>
                                </li>

                                <li class="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                                    <a class="nav-link active" href="#Reviews">
                                        <div class="d-md-flex justify-content-md-center align-items-md-center">
                                            Reviews
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="mb-4 px-lg-4">
                            <div class="row mb-8">
                                @if($review_sys_data)
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <h3 class="font-size-18 mb-6">Based on {{ $review_sys_data->total_reviews }} reviews</h3>

                                        <h2 class="font-size-30 font-weight-bold text-lh-1 mb-0">{{ $review_sys_data->total_star_value }}</h2>
                                        <div class="text-lh-1">overall</div>
                                    </div>

                                    <!-- Ratings -->
                                    <ul class="list-unstyled">
                                        <li class="py-1">
                                            <a class="row align-items-center mx-gutters-2 font-size-1" href="javascript:;">
                                                <div class="col-auto mb-2 mb-md-0">
                                                    <div class="text-warning text-ls-n2 font-size-16" style="width: 80px;">
                                                        <small class="fas fa-star"></small>
                                                        <small class="fas fa-star"></small>
                                                        <small class="fas fa-star"></small>
                                                        <small class="fas fa-star"></small>
                                                        <small class="fas fa-star "></small>
                                                    </div>

                                                </div>
                                                <div class="col-auto mb-2 mb-md-0">
                                                    <div class="progress ml-xl-5" style="height: 10px; width: 200px;">
                                                        <div class="progress-bar" role="progressbar" style="width: {{($review_sys_data->s5/$review_sys_data->total_reviews)*100}}%;"
                                                        aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-auto text-right">
                                                    <span class="text-gray-90">{{ $review_sys_data->s5 }}</span>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="py-1">
                                            <a class="row align-items-center mx-gutters-2 font-size-1" href="javascript:;">
                                                <div class="col-auto mb-2 mb-md-0">
                                                    <div class="text-warning text-ls-n2 font-size-16" style="width: 80px;">
                                                        <small class="fas fa-star"></small>
                                                        <small class="fas fa-star"></small>
                                                        <small class="fas fa-star"></small>
                                                        <small class="fas fa-star"></small>
                                                        <small class="far fa-star text-muted"></small>
                                                    </div>

                                                </div>
                                                <div class="col-auto mb-2 mb-md-0">
                                                    <div class="progress ml-xl-5" style="height: 10px; width: 200px;">
                                                        <div class="progress-bar" role="progressbar" style="width: {{($review_sys_data->s4/$review_sys_data->total_reviews)*100}}%;"
                                                        aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-auto text-right">
                                                    <span class="text-gray-90">{{ $review_sys_data->s4 }}</span>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="py-1">
                                            <a class="row align-items-center mx-gutters-2 font-size-1" href="javascript:;">
                                                <div class="col-auto mb-2 mb-md-0">
                                                    <div class="text-warning text-ls-n2 font-size-16" style="width: 80px;">
                                                        <small class="fas fa-star"></small>
                                                        <small class="fas fa-star"></small>
                                                        <small class="fas fa-star"></small>
                                                        <small class="far fa-star text-muted"></small>
                                                        <small class="far fa-star text-muted"></small>
                                                    </div>
                                                </div>
                                                <div class="col-auto mb-2 mb-md-0">
                                                    <div class="progress ml-xl-5" style="height: 10px; width: 200px;">
                                                        <div class="progress-bar" role="progressbar" style="width: {{($review_sys_data->s3/$review_sys_data->total_reviews)*100}}%;"
                                                        aria-valuenow="53" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-auto text-right">
                                                    <span class="text-gray-90">{{ $review_sys_data->s3 }}</span>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="py-1">
                                            <a class="row align-items-center mx-gutters-2 font-size-1" href="javascript:;">
                                                <div class="col-auto mb-2 mb-md-0">
                                                    <div class="text-warning text-ls-n2 font-size-16" style="width: 80px;">
                                                        <small class="fas fa-star"></small>
                                                        <small class="fas fa-star"></small>
                                                        <small class="far fa-star text-muted"></small>
                                                        <small class="far fa-star text-muted"></small>
                                                        <small class="far fa-star text-muted"></small>
                                                    </div>
                                                </div>

                                                <div class="col-auto mb-2 mb-md-0">
                                                    <div class="progress ml-xl-5" style="height: 10px; width: 200px;">
                                                        <div class="progress-bar" role="progressbar" style="width: {{($review_sys_data->s2/$review_sys_data->total_reviews)*100}}%;"
                                                        aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-auto text-right">
                                                    <span class="text-gray-90">{{ $review_sys_data->s2 }}</span>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="py-1">
                                            <a class="row align-items-center mx-gutters-2 font-size-1" href="javascript:;">
                                                <div class="col-auto mb-2 mb-md-0">
                                                    <div class="text-warning text-ls-n2 font-size-16" style="width: 80px;">
                                                        <small class="fas fa-star"></small>
                                                        <small class="far fa-star text-muted"></small>
                                                        <small class="far fa-star text-muted"></small>
                                                        <small class="far fa-star text-muted"></small>
                                                        <small class="far fa-star text-muted"></small>
                                                    </div>
                                                </div>
                                                <div class="col-auto mb-2 mb-md-0">
                                                    <div class="progress ml-xl-5" style="height: 10px; width: 200px;">
                                                        <div class="progress-bar" role="progressbar" style="width: {{($review_sys_data->s1/$review_sys_data->total_reviews)*100}}%;"
                                                        aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-auto text-right">
                                                    <span class="text-gray-90">{{ $review_sys_data->s1 }}</span>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="py-1">
                                            <a class="row align-items-center mx-gutters-2 font-size-1" href="javascript:;">
                                                <div class="col-auto mb-2 mb-md-0">
                                                    <div class="text-warning text-ls-n2 font-size-16" style="width: 80px;">
                                                        <small class="far fa-star text-muted"></small>
                                                        <small class="far fa-star text-muted"></small>
                                                        <small class="far fa-star text-muted"></small>
                                                        <small class="far fa-star text-muted"></small>
                                                        <small class="far fa-star text-muted"></small>
                                                    </div>
                                                </div>
                                                <div class="col-auto mb-2 mb-md-0">
                                                    <div class="progress ml-xl-5" style="height: 10px; width: 200px;">
                                                        <div class="progress-bar" role="progressbar" style="width: {{($review_sys_data->s0/$review_sys_data->total_reviews)*100}}%;"
                                                        aria-valuenow="1" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div class="col-auto text-right">
                                                    <span class="text-gray-90">{{ $review_sys_data->s0 }}</span>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                    <!-- End Ratings -->
                                </div>
                                @else
                                <div class="col-md-6"> <h3>No reviews yet</h3>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <h3 class="font-size-18 mb-6"></h3>

                                            <h2 class="font-size-30 font-weight-bold text-lh-1 mb-0">5.0</h2>
                                            <div class="text-lh-1">overall</div>
                                        </div>
                                    </div>
                                    <ul class="list-unstyled">
                                        <li class="py-1">
                                            <a class="row align-items-center mx-gutters-2 font-size-1" href="javascript:;">
                                                <div class="col-auto mb-2 mb-md-0">
                                                    <div class="text-warning text-ls-n2 font-size-16" style="width: 80px;">
                                                        <small class="fas fa-star"></small>
                                                        <small class="fas fa-star"></small>
                                                        <small class="fas fa-star"></small>
                                                        <small class="fas fa-star"></small>
                                                        <small class="fas fa-star "></small>
                                                    </div>

                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                @endif
                                <div class="col-md-6">
                                    <h3 class="font-size-18 mb-5">Add a review</h3>
                                    <!-- Form -->
                                    <form class="js-validate" action="/add-review" method="POST">
                                        @csrf
                                        <div class="row align-items-center mb-4">
                                            {{-- <div class="col-md-4 col-lg-3">
                                                <label for="rating" class="form-label mb-0">Your Review</label>
                                            </div> --}}
                                            {{-- <div class="col-md-8 col-lg-9"> --}}
                                                {{-- <a href="#" class="d-block"> --}}
                                                    {{-- <div class="text-warning text-ls-n2 font-size-16">
                                                        <small class="far fa-star text-muted"></small>
                                                        <small class="far fa-star text-muted"></small>
                                                        <small class="far fa-star text-muted"></small>
                                                        <small class="far fa-star text-muted"></small>
                                                        <small class="far fa-star text-muted"></small>
                                                    </div> --}}
                                                    <div class="col ></div>
                                                    <div class="col">
                                                    <div class="star-container">
                                                        <div class="post">
                                                            <div class="text" id="submitted_text"></div>
                                                            <div class="edit d-none">Edit Review</div>
                                                            {{-- <div class="text">Thanks for your review</div>
                                                            <div class="edit">Edit Review</div> --}}
                                                        </div>
                                                        <div class="star-widget" id="star_widget">
                                                            <input type="radio" name="star_value" id="rate-5" value="5"  >
                                                            <label for="rate-5" class="fa fa-star"></label>

                                                            <input type="radio" name="star_value" id="rate-4" value="4"  >
                                                            <label for="rate-4" class="fa fa-star"></label>

                                                            <input type="radio" name="star_value" id="rate-3" value="3"  >
                                                            <label for="rate-3" class="fa fa-star"></label>

                                                            <input type="radio" name="star_value" id="rate-2" value="2"  >
                                                            <label for="rate-2" class="fa fa-star"></label>

                                                            <input type="radio" name="star_value" id="rate-1" value="1"  >
                                                            <label for="rate-1" class="fa fa-star"></label>

                                                        </div>


                                                    </div>
                                                </div>
                                                {{-- </a> --}}
                                            {{-- </div> --}}
                                        </div>
                                        <div class="js-form-message form-group mb-3 row">
                                            <div class="col-md-4 col-lg-3">
                                                <label for="descriptionTextarea" class="form-label">Your Review</label>
                                            </div>
                                            <div class="col-md-8 col-lg-9">
                                                <textarea class="form-control" rows="3"
                                                name="review_content"
                                                id="descriptionTextarea"
                                                data-msg="Please enter your message."
                                                data-error-class="u-has-error"
                                                data-success-class="u-has-success"></textarea>
                                            </div>
                                        </div>
                                        <div class="js-form-message form-group mb-3 row">
                                            <div class="col-md-4 col-lg-3">
                                                <label for="inputName" class="form-label">Name <span class="text-danger">*</span></label>
                                            </div>
                                            <div class="col-md-8 col-lg-9">
                                                <input type="text" class="form-control" name="username" id="inputName" aria-label="Alex Hecker" required
                                                data-msg="Please enter your name."
                                                data-error-class="u-has-error"
                                                data-success-class="u-has-success">
                                            </div>
                                        </div>
                                        <div class="js-form-message form-group mb-3 row">
                                            <div class="col-md-4 col-lg-3">
                                                <label for="email" class="form-label">Email <span class="text-danger">*</span></label>
                                            </div>
                                            <div class="col-md-8 col-lg-9">
                                                <input type="email" class="form-control" name="email" id="emailAddress" aria-label="alexhecker@pixeel.com" required
                                                data-msg="Please enter a valid email address."
                                                data-error-class="u-has-error"
                                                data-success-class="u-has-success">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="offset-md-4 offset-lg-3 col-auto">
                                                <button type="submit" class="btn btn-primary-dark btn-wide transition-3d-hover">Add Review</button>
                                            </div>
                                        </div>
                                        <input type="hidden" name="ad_id" value="{{$ad->id}}">
                                    </form>
                                    <!-- End Form -->
                                </div>
                            </div>
                            @foreach($reviews as $r)
                            <!-- Review -->
                            <div class="border-bottom border-color-1 pb-4 mb-4">
                                <!-- Review Rating -->
                                <div class="d-flex justify-content-between align-items-center text-secondary font-size-1 mb-2">
                                    <div class="text-warning text-ls-n2 font-size-16" style="width: 80px;">
                                        {{-- <small class="fas fa-star"></small>
                                        <small class="fas fa-star"></small>
                                        <small class="fas fa-star"></small>
                                        <small class="far fa-star text-muted"></small>
                                        <small class="far fa-star text-muted"></small> --}}
                                        @for($i=0; $i < $r->star_value; $i++)
                                            <small class="fas fa-star"></small>
                                        @endfor
                                        @php $i = 5-$i @endphp
                                        @for($j=0; $j < $i; $j++)
                                            <small class="fas fa-star text-muted"></small>
                                        @endfor
                                    </div>
                                </div>
                                <!-- End Review Rating -->

                                <p class="text-gray-90">{{$r->review_content}}</p>

                                <!-- Reviewer -->
                                <div class="mb-2">
                                    <strong>{{$r->username}}</strong>
                                    <span class="font-size-13 text-gray-23">{{$r->date}}</span>
                                </div>
                                <!-- End Reviewer -->
                            </div>
                            <!-- End Review -->

                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <!-- Related products -->
        <div class="mb-6">
            <div class="d-flex justify-content-between align-items-center border-bottom border-color-1 flex-lg-nowrap flex-wrap mb-4">
                <h3 class="section-title mb-0 pb-2 font-size-22">Related products</h3>
            </div>
            <ul class="row list-unstyled products-group no-gutters">
                @foreach($related as $r)
                <li class="col-6 col-md-3 col-xl-2gdot4-only col-wd-2 product-item">
                    <div class="product-item__outer h-100">
                        <div class="product-item__inner px-xl-4 p-3">
                            <div class="product-item__body pb-xl-2">
                                <div class="mb-2"><a href="../shop/product-categories-7-column-full-width.html" class="font-size-12 text-gray-5">Speakers</a></div>
                            <h5 class="mb-1 product-item__title"><a href="/view-ad/{{$ad->id}}" class="text-blue font-weight-bold">{{$r->title}}</a></h5>
                                <div class="mb-2">
                                    <a href="/view-ad/{{$r->id}}" class="d-block text-center"><img class="img-fluid"
                                        src={{asset(env('IMAGE_LOCATION').$r->img_1)}} alt="Image Description"></a>
                                </div>
                                <div class="flex-center-between mb-1">
                                    <div class="prodcut-price">
                                    <div class="text-gray-100">Rs.{{$r->price}}</div>
                                    </div>
                                    <div class="d-none d-xl-block prodcut-add-cart">
                                        <a
                                        onclick="add_to_cart({{$r->id}}, 1, {{$r->price}}, {{$r->quantity}})"
                                        class="btn-add-cart btn-primary transition-3d-hover">
                                            <i class="ec ec-add-to-cart"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item__footer">
                                <div class="border-top pt-2 flex-center-between flex-wrap">
                                    {{-- <a href="../shop/compare.html" class="text-gray-6 font-size-13"><i class="ec ec-compare mr-1 font-size-15"></i> Compare</a> --}}
                                    <a
                                    title="Add to Wishlist"
                                    onclick="add_to_wishlist({{$r->id}})"
                                    class=" btn btn-primary">
                                    <i
                                    class="ec ec-favorites mr-1 font-size-15"></i> Add to Wishlist</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
        <!-- End Related products -->
        <!-- Brand Carousel -->
        {{-- <div class="mb-8">
            <div class="py-2 border-top border-bottom">
                <div class="js-slick-carousel u-slick my-1"
                    data-slides-show="5"
                    data-slides-scroll="1"
                    data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-normal u-slick__arrow-centered--y"
                    data-arrow-left-classes="fa fa-angle-left u-slick__arrow-classic-inner--left z-index-9"
                    data-arrow-right-classes="fa fa-angle-right u-slick__arrow-classic-inner--right"
                    data-responsive='[{
                        "breakpoint": 992,
                        "settings": {
                            "slidesToShow": 2
                        }
                    }, {
                        "breakpoint": 768,
                        "settings": {
                            "slidesToShow": 1
                        }
                    }, {
                        "breakpoint": 554,
                        "settings": {
                            "slidesToShow": 1
                        }
                    }]'>
                    <div class="js-slide">
                        <a href="#" class="link-hover__brand">
                            <img class="img-fluid m-auto max-height-50" src="../../assets/img/200X60/img1.png" alt="Image Description">
                        </a>
                    </div>
                    <div class="js-slide">
                        <a href="#" class="link-hover__brand">
                            <img class="img-fluid m-auto max-height-50" src="../../assets/img/200X60/img2.png" alt="Image Description">
                        </a>
                    </div>
                    <div class="js-slide">
                        <a href="#" class="link-hover__brand">
                            <img class="img-fluid m-auto max-height-50" src="../../assets/img/200X60/img3.png" alt="Image Description">
                        </a>
                    </div>
                    <div class="js-slide">
                        <a href="#" class="link-hover__brand">
                            <img class="img-fluid m-auto max-height-50" src="../../assets/img/200X60/img4.png" alt="Image Description">
                        </a>
                    </div>
                    <div class="js-slide">
                        <a href="#" class="link-hover__brand">
                            <img class="img-fluid m-auto max-height-50" src="../../assets/img/200X60/img5.png" alt="Image Description">
                        </a>
                    </div>
                    <div class="js-slide">
                        <a href="#" class="link-hover__brand">
                            <img class="img-fluid m-auto max-height-50" src="../../assets/img/200X60/img6.png" alt="Image Description">
                        </a>
                    </div>
                </div>
            </div>
        </div> --}}
        <!-- End Brand Carousel -->
    </div>

    <style>
        .star-container{
            position: relative;
            width: 400px;
            /* background: #111; */
            /* padding: 20px 30px; */
            /* border: 1px solid #444; */
            border-radius: 5px;
            /* left:  100px; */
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
        }

        .star-widget input{
            display: none;
        }
        .star-widget label{
            font-size: 40px;
            color: #444;
            padding: 5px;
            float: right;
            transition: all .2s ease;
        }

        input:not(:checked) ~ label:hover,
        input:not(:checked) ~ label:hover ~ label{
            color: #fd4;
        }

        input:checked ~ label{
            color: #fd4;
        }
        input#rate-5:checked ~ label{
            color: #fe7;
            text-shadow: 0 0 20px rgb(255, 0, 0);
        }

    </style>

<script>
    let cus_fields = {!! json_encode($ad->cus_fields, JSON_HEX_TAG) !!}
    // console.log(typeof cus_fields);
    let results = [];

    for(let i in cus_fields){
        results.push([i, cus_fields[i]])
    }

    console.log(results);

    results.forEach(i => {
        console.log(i[0]);
        cus_fields_div.innerHTML += `${i[0]} : `
        cus_fields_div.innerHTML += `${i[1]} <hr> `
    })

</script>

</main>
<!-- ========== END MAIN CONTENT ========== -->
@endsection
