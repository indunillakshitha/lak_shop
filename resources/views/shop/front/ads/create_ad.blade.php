@extends('shop.front.layouts.main')

@section('content')

<div class="container">

    <hr>

    <form action="/submit-ad" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">

            <label for="">Title</label>
            <input type="text" class="form-control" name="title">
        </div>
        <div class="form-group">

            <label for="">Subtitle</label>
            <input type="text" class="form-control" name="subtitle">
        </div>
        <div class="form-group">
            <label for="">Category</label>
            <br>
            <select name="category" id="" class="form-control">
                @foreach ($cats as $c)
                <option value="">{{$c}}</option>

                @endforeach
            </select>
        </div>
        <br>
        <br>
        <div class="form-group">

            <label for="">Condition</label>
            <input type="text" class="form-control" name="condition">
        </div>
        <div class="form-group">
            <label for="">Condition Description</label>
            <textarea class="form-control"  name="condition_description" id="" cols="100" rows="3"></textarea>
        </div>
        <div class="form-group">
            <label for="">Image 1</label>
            <input class="xform-control" type="file" name="imgs[]" id="">
            {{-- <input class="xform-control" type="file" name="img_1" id=""> --}}
        </div>
        <div class="form-group">
            <label for="">Image 2</label>
            <input class="xform-control" type="file" name="imgs[]" id="">
            {{-- <input class="xform-control" type="file" name="img_2" id=""> --}}
        </div>
        <div class="form-group">
            <label for="">Image 3</label>
            <input class="xform-control" type="file" name="imgs[]" id="">
            {{-- <input class="xform-control" type="file" name="img_3" id=""> --}}
        </div>
        <div class="form-group">
            <label for="">Image 4</label>
            <input class="xform-control" type="file" name="imgs[]" id="">
            {{-- <input class="xform-control" type="file" name="img_4" id=""> --}}
        </div>
        <div class="form-group">
            <label for="">Image 5</label>
            <input class="xform-control" type="file" name="imgs[]" id="">
            {{-- <input class="xform-control" type="file" name="img_5" id=""> --}}
        </div>
        <div class="form-group">
            <label for="">Item Description</label>
            <input class="form-control" type="text" name="item_description" id="">
        </div>
        <div class="form-group">
            <label for="">Duration</label>
            <input class="form-control" type="text" name="duration" id="">
        </div>
        <div class="form-group">
            <label for="">Price</label>
            <input class="form-control" type="number" name="price" id="">
        </div>
        <div class="form-group">
            <label for="">Quantity</label>
            <input class="form-control" type="number" name="quantity" id="">
        </div>
        {{-- <div class="form-group">
            <label for="">Payment Options</label>
            <input class="form-control" type="number" name="" id="">
        </div> --}}

        <button type="submit" class="primary-btn">Submit Ad</button>
    </form>
    <hr>
</div>


@endsection
