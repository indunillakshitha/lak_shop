<!-- Vertical Menu -->
<div class="col-md-auto d-none d-xl-flex align-items-end">
    <div class="max-width-270 min-width-270">
        <!-- Basics Accordion -->
        <div id="basicsAccordion">
            <!-- Card -->
            <div class="card border-0 rounded-0">
                <div class="card-header bg-primary rounded-0 card-collapse border-0" id="basicsHeadingOne">
                    <button type="button" class="btn-link btn-remove-focus btn-block d-flex card-btn py-3 text-lh-1 px-4 shadow-none btn-primary rounded-top-lg border-0 font-weight-bold text-gray-90"
                        data-toggle="collapse"
                        data-target="#basicsCollapseOne"
                        aria-expanded="true"
                        aria-controls="basicsCollapseOne">
                        <span class="pl-1 text-gray-90">Shop By Department</span>
                        <span class="text-gray-90 ml-3">
                            <span class="ec ec-arrow-down-search"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div id="basicsCollapseOne" class="collapse vertical-menu v1 "
            aria-labelledby="basicsHeadingOne"
            data-parent="#basicsAccordion">
            <div class="card-body p-0">
                @include('shop.front.layouts.accordian')
            </div>
            </div>
        </div>
        <!-- End Basics Accordion -->
    </div>
</div>
<!-- End Vertical Menu -->
