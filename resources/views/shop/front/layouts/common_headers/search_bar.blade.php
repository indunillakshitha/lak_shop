<!-- Search bar -->
<div class="col align-self-center">
    <!-- Search-Form -->
    <form class="js-focus-state" action="/serach-products" method="POST">
        @csrf
        <label class="sr-only" for="searchProduct">Search</label>
        <div class="input-group">
            <input type="text" class="form-control py-2 pl-5 font-size-15 border-0 height-40 rounded-left-pill" name="search" id="searchProduct" placeholder="Search for Products" aria-label="Search for Products" aria-describedby="searchProduct1" >
            <div class="input-group-append">
                <!-- Select -->
                <select class="js-select selectpicker dropdown-select custom-search-categories-select"
                    data-style="btn height-40 text-gray-60 font-weight-normal border-0 rounded-0 bg-white px-5 py-2">
                    <option value="one" selected>All Categories</option>
                </select>
                <!-- End Select -->
                <button class="btn btn-dark height-40 py-2 px-3 rounded-right-pill" type="submit" id="searchProduct1">
                    <span class="ec ec-search font-size-24"></span>
                </button>
            </div>
        </div>
    </form>
    <!-- End Search-Form -->
</div>
<!-- End Search bar -->
