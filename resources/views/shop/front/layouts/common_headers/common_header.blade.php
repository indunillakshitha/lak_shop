@include('shop.front.layouts.header')
        <!-- ========== HEADER ========== -->
        <header id="header" class="u-header u-header-left-aligned-nav">
            <div class="u-header__section">
                @include('shop.front.layouts.top_bar')
                @include('shop.front.layouts.common_headers.logo_and_menu')


                <!-- Vertical-and-Search-Bar -->
                <div class="d-none d-xl-block bg-primary">
                    <div class="container">
                        <div class="row align-items-stretch min-height-50">
                            @include('shop.front.layouts.common_headers.vertical_menu')
                            @include('shop.front.layouts.common_headers.search_bar')
                            @include('shop.front.layouts.common_headers.header_icons')
                        </div>
                    </div>
                </div>
                <!-- End Vertical-and-secondary-menu -->
            </div>
        </header>
        <!-- ========== END HEADER ========== -->
