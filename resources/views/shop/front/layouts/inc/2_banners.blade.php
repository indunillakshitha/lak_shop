<div class="container">
     <!-- Banner -->
 <div class="mb-5">
    <div class="row">
        <div class="col-md-6 mb-4 mb-xl-0">
            <a  class="d-block">
                <img class="img-fluid" src={{asset('/images/slide3.jpg')}} alt="Image Description">
            </a>
        </div>
        <div class="col-md-6 mb-4 mb-xl-0">
            <a  class="d-block">
                <img class="img-fluid" src={{asset('/images/slide4.jpg')}} alt="Image Description">
            </a>
        </div>
    </div>
</div>
<!-- End Banner -->
</div>
