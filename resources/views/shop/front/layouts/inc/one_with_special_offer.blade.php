<div class="container">
    <!-- Catch Daily Deals! -->
    <div class="mb-4">
        <!-- Nav nav-pills -->
        <div class="position-relative z-index-2">
            <div class=" d-flex justify-content-between border-bottom border-color-1 flex-lg-nowrap flex-wrap border-md-down-top-0 border-md-down-bottom-0 mb-4">
                <h3 class="section-title section-title__full mb-0 pb-2 font-size-22">Catch Daily Deals!</h3>

                <ul class="w-100 w-lg-auto nav nav-pills nav-tab-pill nav-tab-pill-fill mb-3 mb-lg-2 pt-3 pt-lg-0 border-top border-color-1 border-lg-top-0 align-items-center font-size-15 font-size-15-lg flex-nowrap flex-lg-wrap overflow-auto overflow-lg-visble pr-0" id="pills-tab-4" role="tablist">
                    <li class="nav-item flex-shrink-0 flex-lg-shrink-1">
                        <a class="nav-link rounded-pill active" id="Bpills-one-example1-tab" data-toggle="pill" href="#Bpills-one-example1" role="tab" aria-controls="Bpills-one-example1" aria-selected="true">-80% off</a>
                    </li>
                    <li class="nav-item flex-shrink-0 flex-lg-shrink-1">
                        <a class="nav-link rounded-pill" id="Bpills-two-example1-tab" data-toggle="pill" href="#Bpills-two-example1" role="tab" aria-controls="Bpills-two-example1" aria-selected="false">-65%</a>
                    </li>
                    <li class="nav-item flex-shrink-0 flex-lg-shrink-1">
                        <a class="nav-link rounded-pill" id="Bpills-three-example1-tab" data-toggle="pill" href="#Bpills-three-example1" role="tab" aria-controls="Bpills-three-example1" aria-selected="false">-45%</a>
                    </li>
                    <li class="nav-item flex-shrink-0 flex-lg-shrink-1">
                        <a class="nav-link rounded-pill" id="Bpills-four-example1-tab" data-toggle="pill" href="#Bpills-four-example1" role="tab" aria-controls="Bpills-four-example1" aria-selected="false">-25%</a>
                    </li>
                </ul>

                <a class="d-block text-gray-16" href="../shop/product-categories-7-column-full-width.html">Go to Daily Deals Section <i class="ec ec-arrow-right-categproes"></i></a>
            </div>
            <div class="row">
                <div class="col-md-auto col-md-5 col-xl-4 col-wd-3gdot3 mb-6 mb-md-0">
                    <!-- Deal -->
                    <div class="p-3 border border-width-2 border-primary borders-radius-20 bg-white min-width-370">
                        <div class="d-flex justify-content-between align-items-center m-1 ml-2">
                            <h3 class="font-size-22 mb-0 font-weight-normal text-lh-28">Special Offer</h3>
                            <div class="d-flex align-items-center flex-column justify-content-center bg-primary rounded-pill height-75 width-75 text-lh-1">
                                <span class="font-size-12">Save</span>
                                <div class="font-size-20 font-weight-bold">$120</div>
                            </div>
                        </div>
                        <div class="mb-2">
                            <a  class="d-block text-center"><img class="img-fluid" src={{asset("front_new/img/320X300/img1.jpg")}} alt="Image Description"></a>
                        </div>
                        <h5 class="mb-2 font-size-14 text-center mx-auto text-lh-18"><a href class="text-blue font-weight-bold">Game Console Controller + USB 3.0 Cable</a></h5>
                        <div class="d-flex align-items-center justify-content-center mb-2">
                            <del class="font-size-18 mr-2 text-gray-2">$99,00</del>
                            <ins class="font-size-30 text-red text-decoration-none">$79,00</ins>
                        </div>
                        <div class="mb-3 mx-2">
                            <div class="d-flex justify-content-between align-items-center mb-2">
                                <span class="">Availavle: <strong>6</strong></span>
                                <span class="">Already Sold: <strong>28</strong></span>
                            </div>
                            <div class="rounded-pill bg-gray-3 height-14 position-relative">
                                <span class="position-absolute left-0 top-0 bottom-0 rounded-pill w-30 bg-primary"></span>
                            </div>
                        </div>
                        <div class="mb-2 mx-2 d-xl-flex align-items-xl-center justify-content-xl-between">
                            <h6 class="font-size-15 text-gray-2 text-center text-xl-left mb-3 mb-xl-0 max-width-100-xl">Hurry Up! Offer ends in:</h6>
                            <div class="js-countdown d-flex justify-content-center"
                                data-end-date="2020/11/30"
                                data-hours-format="%H"
                                data-minutes-format="%M"
                                data-seconds-format="%S">
                                <div class="text-lh-1">
                                    <div class="text-gray-2 font-size-30 bg-gray-4 py-2 px-2 rounded-sm mb-2">
                                        <span class="js-cd-hours"></span>
                                    </div>
                                    <div class="text-gray-2 font-size-12 font-weight-semi-bold text-center">HOURS</div>
                                </div>
                                <div class="mx-1 pt-1 text-gray-2 font-size-24">:</div>
                                <div class="text-lh-1">
                                    <div class="text-gray-2 font-size-30 bg-gray-4 py-2 px-2 rounded-sm mb-2">
                                        <span class="js-cd-minutes"></span>
                                    </div>
                                    <div class="text-gray-2 font-size-12 font-weight-semi-bold text-center">MINS</div>
                                </div>
                                <div class="mx-1 pt-1 text-gray-2 font-size-24">:</div>
                                <div class="text-lh-1">
                                    <div class="text-gray-2 font-size-30 bg-gray-4 py-2 px-2 rounded-sm mb-2">
                                        <span class="js-cd-seconds"></span>
                                    </div>
                                    <div class="text-gray-2 font-size-12 font-weight-semi-bold text-center">SECS</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Deal -->
                </div>
                <div class="col col-md-7 col-xl-8 col-wd-8gdot7">
                    <!-- Tab Content -->
                    <div class="tab-content" id="Bpills-tabContent">
                        <div class="tab-pane fade show active" id="Bpills-one-example1" role="tabpanel" aria-labelledby="Bpills-one-example1-tab">
                            <div class="js-slick-carousel u-slick overflow-hidden u-slick-overflow-visble pt-1 pb-6 px-1"
                                data-pagi-classes="text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-4">
                                <div class="js-slide">
                                    <div class="row products-group no-gutters">
                                        @foreach($loaded_ads as $ad)
                                        <div class="col-6 col-md-4 col-xl-3 col-wd-2gdot4 product-item">
                                            <div class="product-item__outer h-100">
                                                <div class="product-item__inner px-wd-4 p-2 p-md-3">
                                                    <div class="product-item__body pb-xl-2">
                                                        <div class="mb-2"><a href="/view-ad/{{$ad->id}}"class="font-size-12 text-gray-5">Speakers</a></div>
                                                        <h5 class="mb-1 product-item__title"><a href="/view-ad/{{$ad->id}}" class="text-blue font-weight-bold">{{$ad->title}}</a></h5>
                                                        <div class="mb-2">
                                                            <a href="/view-ad/{{$ad->id}}" class="d-block text-center"><img class="img-fluid"
                                                                src={{asset(env('IMAGE_LOCATION').$ad->img_1)}} alt="Image Description"></a>
                                                        </div>
                                                        <div class="flex-center-between mb-1">
                                                            <div class="prodcut-price">
                                                                <div class="text-gray-100">Rs.{{$ad->price}}</div>
                                                            </div>
                                                            <div class="d-none d-xl-block prodcut-add-cart">
                                                                <a
                                                                onclick="add_to_cart({{$ad->id}}, 1, {{$ad->price}}, {{$ad->quantity}})" class="btn-add-cart btn-primary transition-3d-hover"><i class="ec ec-add-to-cart"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="product-item__footer">
                                                        <div class="border-top pt-2 flex-center-between flex-wrap">
                                                            {{-- <a href="../shop/compare.html" class="text-gray-6 font-size-13"><i class="ec ec-compare mr-1 font-size-15"></i> Compare</a> --}}
                                                            <a
                                                title="Add to Wishlist"
                                                onclick="add_to_wishlist({{$ad->id}})"
                                                class=" btn  btn-primary-dark-w">
                                                <i
                                                class="ec ec-favorites mr-1 "></i> Add to Wishlist</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="js-slide">
                                    <div class="row products-group no-gutters">

                                        @foreach($loaded_ads as $ad)
                                        <div class="col-6 col-md-4 col-xl-3 col-wd-2gdot4 product-item">
                                            <div class="product-item__outer h-100">
                                                <div class="product-item__inner px-wd-4 p-2 p-md-3">
                                                    <div class="product-item__body pb-xl-2">
                                                        <div class="mb-2"><a href="/view-ad/{{$ad->id}}"class="font-size-12 text-gray-5">Speakers</a></div>
                                                        <h5 class="mb-1 product-item__title"><a href="/view-ad/{{$ad->id}}" class="text-blue font-weight-bold">{{$ad->title}}</a></h5>
                                                        <div class="mb-2">
                                                            <a href="/view-ad/{{$ad->id}}" class="d-block text-center"><img class="img-fluid"
                                                                src={{asset(env('IMAGE_LOCATION').$ad->img_1)}} alt="Image Description"></a>
                                                        </div>
                                                        <div class="flex-center-between mb-1">
                                                            <div class="prodcut-price">
                                                                <div class="text-gray-100">Rs.{{$ad->price}}</div>
                                                            </div>
                                                            <div class="d-none d-xl-block prodcut-add-cart">
                                                                <a
                                                                onclick="add_to_cart({{$ad->id}}, 1, {{$ad->price}}, {{$ad->quantity}})" class="btn-add-cart btn-primary transition-3d-hover"><i class="ec ec-add-to-cart"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="product-item__footer">
                                                        <div class="border-top pt-2 flex-center-between flex-wrap">
                                                            {{-- <a href="../shop/compare.html" class="text-gray-6 font-size-13"><i class="ec ec-compare mr-1 font-size-15"></i> Compare</a> --}}
                                                            <a
                                                            title="Add to Wishlist"
                                                            onclick="add_to_wishlist({{$ad->id}})"
                                                            class=" btn  btn-primary-dark-w">
                                                            <i
                                                            class="ec ec-favorites mr-1 "></i> Add to Wishlist</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Tab Content -->
                </div>
            </div>
        </div>
        <!-- End Nav Pills -->
    </div>
    <!-- End
</div>
