 <!-- ========== FOOTER ========== -->
 <footer>
    <!-- Footer-top-widget -->
    @php
        $featured_products = App\Ad::orderBy('created_at')->skip(0)->take(5)->get();
    @endphp
    <div class="container d-none d-lg-block mb-3">
        <div class="row">
            <div class="col-wd-3 col-lg-4">
                <div class="widget-column">
                    <div class="border-bottom border-color-1 mb-5">
                        <h3 class="section-title section-title__sm mb-0 pb-2 font-size-18">Featured Products</h3>
                    </div>
                    <ul class="list-unstyled products-group">
                        @foreach($featured_products as $ad)
                        <li class="product-item product-item__list row no-gutters mb-6 remove-divider">
                            <div class="col-auto">
                                <a href="/view-ad/{{$ad->id}}" class="d-block width-75 text-center"><img class="img-fluid" src={{asset(env('IMAGE_LOCATION').$ad->img_1)}} alt="Image Description"></a>
                            </div>
                            <div class="col pl-4 d-flex flex-column">
                            <h5 class="product-item__title mb-0"><a href="/view-ad/{{$ad->id}}" class="text-blue font-weight-bold">{{$ad->title}}</a></h5>
                                <div class="prodcut-price mt-auto">
                                    <div class="font-size-15">Rs.{{$ad->price}}</div>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @php
                $onsale_products = App\Ad::orderBy('created_at', 'desc')->skip(0)->take(5)->get();
            @endphp
            <div class="col-wd-3 col-lg-4">
                <div class="border-bottom border-color-1 mb-5">
                    <h3 class="section-title section-title__sm mb-0 pb-2 font-size-18">Onsale Products</h3>
                </div>
                <ul class="list-unstyled products-group">
                    @foreach($onsale_products as $ad)
                        <li class="product-item product-item__list row no-gutters mb-6 remove-divider">
                            <div class="col-auto">
                                <a href="/view-ad/{{$ad->id}}" class="d-block width-75 text-center"><img class="img-fluid" src={{asset(env('IMAGE_LOCATION').$ad->img_1)}} alt="Image Description"></a>
                            </div>
                            <div class="col pl-4 d-flex flex-column">
                                <h5 class="product-item__title mb-0"><a href="/view-ad/{{$ad->id}}" class="text-blue font-weight-bold">{{$ad->title}}</a></h5>
                                    <div class="prodcut-price mt-auto">
                                        <div class="font-size-15">Rs.{{$ad->price}}</div>
                                    </div>
                                </div>
                        </li>
                        @endforeach
                </ul>
            </div>

            @php
            $top_rated_products = Illuminate\Support\Facades\DB::table('product_reviews')
            ->leftjoin('ads', 'ads.id', 'product_reviews.ad_id')
            ->orderBy('product_reviews.created_at', 'desc')->skip(0)->take(5)
            ->get();
            @endphp
            <div class="col-wd-3 col-lg-4">
                <div class="border-bottom border-color-1 mb-5">
                    <h3 class="section-title section-title__sm mb-0 pb-2 font-size-18">Top Rated Products</h3>
                </div>
                <ul class="list-unstyled products-group">
                    @foreach($top_rated_products as $ad)
                    <li class="product-item product-item__list row no-gutters mb-6 remove-divider">
                        <div class="col-auto">
                            <a href="/view-ad/{{$ad->id}}" class="d-block width-75 text-center"><img class="img-fluid" src={{asset(env('IMAGE_LOCATION').$ad->img_1)}} alt="Image Description"></a>
                        </div>
                        <div class="col pl-4 d-flex flex-column">
                            <h5 class="product-item__title mb-0"><a href="/view-ad/{{$ad->id}}" class="text-blue font-weight-bold">{{$ad->title}}</a></h5>
                            <div class="text-warning mb-2">
                                <small class="fas fa-star"></small>
                                <small class="fas fa-star"></small>
                                <small class="fas fa-star"></small>
                                <small class="fas fa-star"></small>
                                <small class="fas fa-star"></small>
                            </div>
                            <div class="prodcut-price mt-auto">
                                <div class="font-size-15">Rs.{{$ad->price}}</div>
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-wd-3 d-none d-wd-block">
                <a href="../shop/shop.html" class="d-block"><img class="img-fluid" src={{asset("front_new/img/330X360/img1.jpg")}} alt="Image Description"></a>
            </div>
        </div>
    </div>
    <!-- End Footer-top-widget -->
