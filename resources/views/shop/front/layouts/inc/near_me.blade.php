<!-- Slider Section -->

<div class="container">
    <div class="mb-4 position-relative">
        <div class="js-slick-carousel u-slick u-slick--gutters-0 position-static overflow-hidden u-slick-overflow-visble pb-5 pt-2 px-1"
            data-arrows-classes="u-slick__arrow u-slick__arrow--flat u-slick__arrow-centered--y rounded-circle"
            data-arrow-left-classes="fas fa-arrow-left u-slick__arrow-inner u-slick__arrow-inner--left ml-lg-2 ml-xl-n3"
            data-arrow-right-classes="fas fa-arrow-right u-slick__arrow-inner u-slick__arrow-inner--right mr-lg-2 mr-xl-n3"
            data-pagi-classes="text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-3 pt-1"
            data-slides-show="7"
            data-slides-scroll="1"
            data-responsive='[{
              "breakpoint": 1400,
              "settings": {
                "slidesToShow": 5
              }
            }, {
                "breakpoint": 1200,
                "settings": {
                  "slidesToShow": 3
                }
            }, {
              "breakpoint": 992,
              "settings": {
                "slidesToShow": 2
              }
            }, {
              "breakpoint": 768,
              "settings": {
                "slidesToShow": 2
              }
            }, {
              "breakpoint": 554,
              "settings": {
                "slidesToShow": 2
              }
            }]'>
            
            @foreach($near_me as $ad)
            <div class="js-slide products-group">
                <div class="product-item mx-1 remove-divider">
                    <div class="product-item__outer h-100">
                        <div class="product-item__inner bg-white px-wd-3 p-2 p-md-3">
                            <div class="product-item__body pb-xl-2">
                                <div class="mb-2"><a href="/view-ad/{{$ad->id}}" class="font-size-12 text-gray-5">Speakers</a></div>
                            <h5 class="mb-1 product-item__title"><a href="/view-ad/{{$ad->id}}" class="text-blue font-weight-bold">{{$ad->title}}</a></h5>
                                <div class="mb-2">
                                    <a href="/view-ad/{{$ad->id}}" class="d-block text-center"><img class="img-fluid"
                                        src={{asset(env('IMAGE_LOCATION').$ad->img_1)}} alt="Image Description"></a>
                                </div>
                                <div class="flex-center-between mb-1">
                                    <div class="prodcut-price">
                                        <div class="text-gray-100">Rs.{{$ad->price}}</div>
                                    </div>
                                    <div class="d-none d-xl-block prodcut-add-cart">
                                        <a
                                        onclick="add_to_cart({{$ad->id}}, 1, {{$ad->price}}, {{$ad->quantity}})"
                                        class="btn-add-cart btn-primary transition-3d-hover"><i class="ec ec-add-to-cart"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="product-item__footer">
                                <div class="border-top pt-2 flex-center-between flex-wrap">
                                    {{-- <a href="../shop/wishlist.html" class="text-gray-6 font-size-13"><i class="ec ec-favorites mr-1 font-size-15"></i> Wishlist</a> --}}
                                    <a
                                        title="Add to Wishlist"
                                        onclick="add_to_wishlist({{$ad->id}})"
                                        class=" btn  btn-primary-dark-w">
                                        <i
                                        class="ec ec-favorites mr-1 "></i> Add to Wishlist</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</div>
<!-- End Slider Section -->
