<div class="container">
                    <!-- Categories Carousel -->
                    <div class="mb-5">
                        <div class="position-relative">
                            <div class="js-slick-carousel u-slick u-slick--gutters-0 position-static overflow-hidden u-slick-overflow-visble pb-5 pt-2 px-1"
                                data-arrows-classes="d-none d-xl-block u-slick__arrow-normal u-slick__arrow-centered--y rounded-circle text-black font-size-30 z-index-2"
                                data-arrow-left-classes="fa fa-angle-left u-slick__arrow-inner--left left-n16"
                                data-arrow-right-classes="fa fa-angle-right u-slick__arrow-inner--right right-n20"
                                data-pagi-classes="d-xl-none text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-3 pt-1"
                                data-slides-show="10"
                                data-slides-scroll="1"
                                data-responsive='[{
                                  "breakpoint": 1400,
                                  "settings": {
                                    "slidesToShow": 8
                                  }
                                }, {
                                    "breakpoint": 1200,
                                    "settings": {
                                      "slidesToShow": 6
                                    }
                                }, {
                                  "breakpoint": 992,
                                  "settings": {
                                    "slidesToShow": 5
                                  }
                                }, {
                                  "breakpoint": 768,
                                  "settings": {
                                    "slidesToShow": 3
                                  }
                                }, {
                                  "breakpoint": 554,
                                  "settings": {
                                    "slidesToShow": 2
                                  }
                                }]'>
                                <div class="js-slide">
                                    <a href= "#" class="d-block text-center bg-on-hover width-122 mx-auto">
                                        <div class="bg pt-4 rounded-circle-top width-122 height-75">
                                            <i class="ec ec-laptop font-size-40"></i>
                                        </div>
                                        <div class="bg-white px-2 pt-2 width-122">
                                            <h6 class="font-weight-semi-bold font-size-14 text-gray-90 mb-0 text-lh-1dot2">Accessories</h6>
                                        </div>
                                    </a>
                                </div>
                                <div class="js-slide">
                                    <a href= "#" class="d-block text-center bg-on-hover width-122 mx-auto">
                                        <div class="bg pt-4 rounded-circle-top width-122 height-75">
                                            <i class="ec ec-smartwatch font-size-40"></i>
                                        </div>
                                        <div class="bg-white px-2 pt-2 width-122">
                                            <h6 class="font-weight-semi-bold font-size-14 text-gray-90 mb-0 text-lh-1dot2">Smart Watch</h6>
                                        </div>
                                    </a>
                                </div>
                                <div class="js-slide">
                                    <a href= "#" class="d-block text-center bg-on-hover width-122 mx-auto">
                                        <div class="bg pt-4 rounded-circle-top width-122 height-75">
                                            <i class="ec ec-gamepad font-size-40"></i>
                                        </div>
                                        <div class="bg-white px-2 pt-2 width-122">
                                            <h6 class="font-weight-semi-bold font-size-14 text-gray-90 mb-0 text-lh-1dot2">Game Joy stick</h6>
                                        </div>
                                    </a>
                                </div>
                                <div class="js-slide">
                                    <a href= "#" class="d-block text-center bg-on-hover width-122 mx-auto">
                                        <div class="bg pt-4 rounded-circle-top width-122 height-75">
                                            <i class="ec ec-headphones font-size-40"></i>
                                        </div>
                                        <div class="bg-white px-2 pt-2 width-122">
                                            <h6 class="font-weight-semi-bold font-size-14 text-gray-90 mb-0 text-lh-1dot2">Headphones</h6>
                                        </div>
                                    </a>
                                </div>
                                <div class="js-slide">
                                    <a href= "#" class="d-block text-center bg-on-hover width-122 mx-auto">
                                        <div class="bg pt-4 rounded-circle-top width-122 height-75">
                                            <i class="ec ec-tvs font-size-40"></i>
                                        </div>
                                        <div class="bg-white px-2 pt-2 width-122">
                                            <h6 class="font-weight-semi-bold font-size-14 text-gray-90 mb-0 text-lh-1dot2">LED TV</h6>
                                        </div>
                                    </a>
                                </div>
                                <div class="js-slide">
                                    <a href= "#" class="d-block text-center bg-on-hover width-122 mx-auto">
                                        <div class="bg pt-4 rounded-circle-top width-122 height-75">
                                            <i class="ec ec-drone font-size-40"></i>
                                        </div>
                                        <div class="bg-white px-2 pt-2 width-122">
                                            <h6 class="font-weight-semi-bold font-size-14 text-gray-90 mb-0 text-lh-1dot2">Drone</h6>
                                        </div>
                                    </a>
                                </div>
                                <div class="js-slide">
                                    <a href= "#" class="d-block text-center bg-on-hover width-122 mx-auto">
                                        <div class="bg pt-4 rounded-circle-top width-122 height-75">
                                            <i class="ec ec-cameras font-size-40"></i>
                                        </div>
                                        <div class="bg-white px-2 pt-2 width-122">
                                            <h6 class="font-weight-semi-bold font-size-14 text-gray-90 mb-0 text-lh-1dot2">DSLR Camera</h6>
                                        </div>
                                    </a>
                                </div>
                                <div class="js-slide">
                                    <a href= "#" class="d-block text-center bg-on-hover width-122 mx-auto">
                                        <div class="bg pt-4 rounded-circle-top width-122 height-75">
                                            <i class="ec ec-speaker font-size-40"></i>
                                        </div>
                                        <div class="bg-white px-2 pt-2 width-122">
                                            <h6 class="font-weight-semi-bold font-size-14 text-gray-90 mb-0 text-lh-1dot2">Audio Speakers</h6>
                                        </div>
                                    </a>
                                </div>
                                <div class="js-slide">
                                    <a href= "#" class="d-block text-center bg-on-hover width-122 mx-auto">
                                        <div class="bg pt-4 rounded-circle-top width-122 height-75">
                                            <i class="ec ec-smartphones font-size-40"></i>
                                        </div>
                                        <div class="bg-white px-2 pt-2 width-122">
                                            <h6 class="font-weight-semi-bold font-size-14 text-gray-90 mb-0 text-lh-1dot2">Smartphones</h6>
                                        </div>
                                    </a>
                                </div>
                                <div class="js-slide">
                                    <a href= "#" class="d-block text-center bg-on-hover width-122 mx-auto">
                                        <div class="bg pt-4 rounded-circle-top width-122 height-75">
                                            <i class="ec ec-laptop font-size-40"></i>
                                        </div>
                                        <div class="bg-white px-2 pt-2 width-122">
                                            <h6 class="font-weight-semi-bold font-size-14 text-gray-90 mb-0 text-lh-1dot2">Accessories</h6>
                                        </div>
                                    </a>
                                </div>
                                <div class="js-slide">
                                    <a href= "#" class="d-block text-center bg-on-hover width-122 mx-auto">
                                        <div class="bg pt-4 rounded-circle-top width-122 height-75">
                                            <i class="ec ec-cameras font-size-40"></i>
                                        </div>
                                        <div class="bg-white px-2 pt-2 width-122">
                                            <h6 class="font-weight-semi-bold font-size-14 text-gray-90 mb-0 text-lh-1dot2">DSLR Camera</h6>
                                        </div>
                                    </a>
                                </div>
                                <div class="js-slide">
                                    <a href= "#" class="d-block text-center bg-on-hover width-122 mx-auto">
                                        <div class="bg pt-4 rounded-circle-top width-122 height-75">
                                            <i class="ec ec-gamepad font-size-40"></i>
                                        </div>
                                        <div class="bg-white px-2 pt-2 width-122">
                                            <h6 class="font-weight-semi-bold font-size-14 text-gray-90 mb-0 text-lh-1dot2">Game Joy stick</h6>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Categories Carousel -->

</div>
