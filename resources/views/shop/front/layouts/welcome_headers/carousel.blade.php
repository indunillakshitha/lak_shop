<div class="home-v1-slider" >
    <!-- ========================================== SECTION – HERO : END========================================= -->

    <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">

        <div class="item" style="background-image: url({{asset('/images/slide1.jpg')}});">
        </div><!-- /.item -->


        <div class="item" style="background-image: url({{asset('/images/slide2.jpg')}});">
        </div><!-- /.item -->

        <div class="item" style="background-image: url({{asset('/images/slide3.jpg')}});">
            {{-- <div class="container">
                <div class="row">
                    <div class="col-md-offset-3 col-md-5">
                        <div class="caption vertical-center text-left">
                            <div class="hero-subtitle-v2 fadeInLeft-1">
                                shop to get what you loves
                            </div>

                            <div class="hero-2 fadeInRight-1">
                                Timepieces that make a statement up to <strong>40% Off</strong>
                             </div>

                            <div class="hero-action-btn fadeInLeft-2">
                                <a href="single-product.html" class="big le-button ">Start Buying</a>
                            </div>
                        </div><!-- /.caption -->
                    </div>
                </div>
            </div><!-- /.container --> --}}
        </div><!-- /.item -->
        <div class="item" style="background-image: url({{asset('/images/slide4.jpg')}});">
        </div><


    </div><!-- /.owl-carousel -->

    <!-- ========================================= SECTION – HERO : END ========================================= -->

</div><!-- /.home-v1-slider -->
