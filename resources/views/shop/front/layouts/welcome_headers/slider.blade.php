                        <!-- Slider-Category Section -->
                        <div class="col">
                            <div class="max-width-890-wd max-width-660-xl">
                                <!-- Slider -->
                                <div class="slider-bg max-height-345-xl max-height-348-wd">
                                    <div class="overflow-hidden">
                                        <div class="js-slick-carousel u-slick"
                                            data-pagi-classes="text-center position-absolute right-0 bottom-0 left-0 u-slick__pagination u-slick__pagination--long justify-content-start mb-3 mb-md-5 ml-3 ml-md-4 ml-lg-9 ml-xl-4 ml-wd-9">
                                            <div class="js-slide">
                                                <div class="py-6 py-md-4 px-3 px-md-4 px-lg-9 px-xl-4 px-wd-9">
                                                    <div class="row no-gutters">
                                                        <div class="col-xl-6 col-6 mt-md-5">
                                                            <h1 class="font-size-58-sm text-lh-57 font-weight-light"
                                                                data-scs-animation-in="fadeInUp">
                                                                LAK.LK <span class="d-block font-size-58-sm"></span>
                                                            </h1>
                                                            <h6 class="font-size-15-sm font-weight-bold mb-2 mb-md-3"
                                                                data-scs-animation-in="fadeInUp"
                                                                data-scs-animation-delay="200">ONLINE SHOPPING AT IT'S BEST 
                                                                <br>
                                                            </h6>
                                                            <div class="mb-2 mb-md-4"
                                                                data-scs-animation-in="fadeInUp"
                                                                data-scs-animation-delay="300">
                                                                <span class="font-size-13">FROM</span>
                                                                <div class="font-size-50 font-weight-bold text-lh-45">Rs.749</div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-6 col-6 d-flex align-items-center"
                                                            data-scs-animation-in="zoomIn"
                                                            data-scs-animation-delay="400">
                                                            <img class="img-fluid max-width-300-md" src={{asset('/images/slide1.jpg')}} alt="Image Description">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="js-slide">
                                                <div class="py-6 py-md-4 px-3 px-md-4 px-lg-9 px-xl-4 px-wd-9">
                                                    <div class="row no-gutters">
                                                        <div class="col-xl-6 col-6 mt-md-5">
                                                            <h1 class="font-size-58-sm text-lh-57 font-weight-light"
                                                                data-scs-animation-in="slideInLeft">
                                                                THE NEW <span class="d-block font-size-58-sm">STANDARD</span>
                                                            </h1>
                                                            <h6 class="font-size-15-sm font-weight-bold mb-2 mb-md-3"
                                                                data-scs-animation-in="slideInLeft"
                                                                data-scs-animation-delay="200">UNDER FAVORABLE SMARTWATCHES
                                                            </h6>
                                                            <div class="mb-2 mb-md-4"
                                                                data-scs-animation-in="slideInLeft"
                                                                data-scs-animation-delay="300">
                                                                <span class="font-size-13">FROM</span>
                                                                <div class="font-size-50 font-weight-bold text-lh-45">Rs.749</div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-6 col-6 d-flex align-items-center"
                                                            data-scs-animation-in="slideInRight"
                                                            data-scs-animation-delay="400">
                                                            <img class="img-fluid max-width-300-md" src={{asset('/images/slide2.jpg')}} alt="Image Description">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="js-slide">
                                                <div class="py-6 py-md-4 px-3 px-md-4 px-lg-9 px-xl-4 px-wd-9">
                                                    <div class="row no-gutters">
                                                        <div class="col-xl-6 col-6 mt-md-5">
                                                            <h1 class="font-size-58-sm text-lh-57 font-weight-light"
                                                                data-scs-animation-in="fadeInUp">
                                                                THE NEW <span class="d-block font-size-58-sm">STANDARD</span>
                                                            </h1>
                                                            <h6 class="font-size-15-sm font-weight-bold mb-2 mb-md-3"
                                                                data-scs-animation-in="fadeInUp"
                                                                data-scs-animation-delay="200">UNDER FAVORABLE SMARTWATCHES
                                                            </h6>
                                                            <div class="mb-2 mb-md-4"
                                                                data-scs-animation-in="fadeInUp"
                                                                data-scs-animation-delay="300">
                                                                <span class="font-size-13">FROM</span>
                                                                <div class="font-size-50 font-weight-bold text-lh-45">Rs.749</div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-6 col-6 d-flex align-items-center"
                                                            data-scs-animation-in="zoomIn"
                                                            data-scs-animation-delay="400">
                                                            <img class="img-fluid max-width-300-md" src={{asset('/images/slide3.jpg')}} alt="Image Description">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Slider -->
                                <!-- Category -->
                                <ul class="list-group list-group-horizontal-sm position-relative z-index-2 flex-row overflow-auto overflow-md-visble">
                                    <li class="list-group-item py-2 px-3 px-xl-4 px-wd-5 flex-horizontal-center shadow-on-hover-1 rounded-0 border-top-0 border-bottom-0 flex-shrink-0 flex-md-shrink-1">
                                        <a href="../shop/product-categories-7-column-full-width.html" class="d-block py-2 text-center">
                                            <img class="img-fluid mb-1 max-width-100-sm" src={{asset('/images/slide1.jpg')}} alt="Image Description">
                                            <h6 class="font-size-14 mb-0 text-gray-90 font-weight-semi-bold">Accessories</h6>
                                        </a>
                                    </li>
                                    <li class="list-group-item py-2 px-3 px-xl-4 px-wd-5 flex-horizontal-center shadow-on-hover-1 rounded-0 border-top-0 border-bottom-0 flex-shrink-0 flex-md-shrink-1">
                                        <a href="../shop/product-categories-7-column-full-width.html" class="d-block py-2 text-center">
                                            <img class="img-fluid mb-1 max-width-100-sm" src={{asset('/images/slide2.jpg')}} alt="Image Description">
                                            <h6 class="font-size-14 mb-0 text-gray-90 font-weight-semi-bold">Laptop Speakers</h6>
                                        </a>
                                    </li>
                                    <li class="list-group-item py-2 px-3 px-xl-4 px-wd-5 flex-horizontal-center shadow-on-hover-1 rounded-0 border-top-0 border-bottom-0 flex-shrink-0 flex-md-shrink-1">
                                        <a href="../shop/product-categories-7-column-full-width.html" class="d-block py-2 text-center">
                                            <img class="img-fluid mb-1 max-width-100-sm" src={{asset('/images/slide3.jpg')}} alt="Image Description">
                                            <h6 class="font-size-14 mb-0 text-gray-90 font-weight-semi-bold">All in One</h6>
                                        </a>
                                    </li>
                                    <li class="list-group-item py-2 px-3 px-xl-4 px-wd-5 flex-horizontal-center shadow-on-hover-1 rounded-0 border-top-0 border-bottom-0 flex-shrink-0 flex-md-shrink-1">
                                        <a href="../shop/product-categories-7-column-full-width.html" class="d-block py-2 text-center">
                                            <img class="img-fluid mb-1 max-width-100-sm" src={{asset('/images/slide4.jpg')}} alt="Image Description">
                                            <h6 class="font-size-14 mb-0 text-gray-90 font-weight-semi-bold">Audio Speakers</h6>
                                        </a>
                                    </li>
                                    <li class="list-group-item py-2 px-3 px-xl-4 px-wd-5 flex-horizontal-center shadow-on-hover-1 rounded-0 border-top-0 border-bottom-0 flex-shrink-0 flex-md-shrink-1 d-xl-none d-wd-block">
                                        <a href="../shop/product-categories-7-column-full-width.html" class="d-block py-2 text-center">
                                            <img class="img-fluid mb-1 max-width-100-sm" src={{asset('/images/slide1.jpg')}} alt="Image Description">
                                            <h6 class="font-size-14 mb-0 text-gray-90 font-weight-semi-bold">Camera</h6>
                                        </a>
                                    </li>
                                </ul>
                                <!-- End Category -->
                            </div>
                        </div>
                        <!-- End Slider-Category Section -->
                        <!-- Banner -->
                        <div class="col-md-auto">
                            <div class="max-width-240-xl">
                                <div class="d-md-flex d-xl-block">
                                    <div class="bg-white border-top border-xl-top-0">
                                        <a href="../shop/shop.html" class="text-gray-90 position-relative d-block overflow-hidden">
                                            <div class="position-absolute transform-rotate-16-banner">
                                                <img class="img-fluid" src={{asset('/images/slide1.jpg')}}  alt="Image Description">
                                            </div>
                                            <div class="px-4 py-6 min-height-172">
                                                <div class="mb-2 pb-1 font-size-18 font-weight-light text-ls-n1 text-lh-23">
                                                    CATCH BIG <strong>DEALS</strong> ON THE CAMERAS
                                                </div>
                                                <div class="link text-gray-90 font-weight-bold font-size-15" href="#">
                                                    Shop now
                                                    <span class="link__icon ml-1">
                                                        <span class="link__icon-inner"><i class="ec ec-arrow-right-categproes"></i></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="bg-white border-top">
                                        <a href="../shop/shop.html" class="text-gray-90 position-relative d-block overflow-hidden">
                                            <div class="position-absolute transform-rotate-16-banner">
                                                <img class="img-fluid" src={{asset('/images/slide2.jpg')}}  alt="Image Description">
                                            </div>
                                            <div class="px-4 py-6 min-height-172">
                                                <div class="mb-2 pb-1 font-size-18 font-weight-light text-ls-n1 text-lh-23">
                                                    CATCH BIG <strong>DEALS</strong> ON THE CAMERAS
                                                </div>
                                                <div class="link text-gray-90 font-weight-bold font-size-15" href="#">
                                                    Shop now
                                                    <span class="link__icon ml-1">
                                                        <span class="link__icon-inner"><i class="ec ec-arrow-right-categproes"></i></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="bg-white border-top">
                                        <a href="../shop/shop.html" class="text-gray-90 position-relative d-block overflow-hidden">
                                            <div class="position-absolute transform-rotate-16-banner">
                                                <img class={{asset('/images/slide1.jpg')}}  alt="Image Description">
                                            </div>
                                            <div class="px-4 py-6 min-height-172">
                                                <div class="mb-2 pb-1 font-size-18 font-weight-light text-ls-n1 text-lh-23">
                                                    CATCH BIG <strong>DEALS</strong> ON THE CAMERAS
                                                </div>
                                                <div class="link text-gray-90 font-weight-bold font-size-15" href="#">
                                                    Shop now
                                                    <span class="link__icon ml-1">
                                                        <span class="link__icon-inner"><i class="ec ec-arrow-right-categproes"></i></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Banner -->