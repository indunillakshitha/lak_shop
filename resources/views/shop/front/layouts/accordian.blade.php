

        <nav
            class="js-mega-menu  navbar navbar-expand-xl u-header__navbar u-header__navbar--no-space hs-menu-initialized">
            <div id="navBar" class="collapse  navbar-collapse u-header__navbar-collapse">
                <ul class="navbar-nav u-header__navbar-nav border-primary border-top-0">
                    <li class="nav-item u-header__nav-item" data-event="hover" data-position="left">
                        <a href="#" class="nav-link u-header__nav-link font-weight-bold">Value of the Day</a>
                    </li>
                    <li class="nav-item u-header__nav-item" data-event="hover" data-position="left">
                        <a href="#" class="nav-link u-header__nav-link font-weight-bold">Top 100 Offers</a>
                    </li>
                    <li class="nav-item u-header__nav-item" data-event="hover" data-position="left">
                        <a href="#" class="nav-link u-header__nav-link font-weight-bold">New Arrivals</a>
                    </li>
                    <!-- Nav Item MegaMenu -->
                    @php
                    $cats = App\Category::where('status', 1)->where('parent_id', 0)->get();
                    @endphp
                    @foreach($cats as $c)
                    @php
                    $sub_cats = App\Category::where('status', 1)->where('parent_id', $c->id)->get();
                    @endphp
                    <li class="nav-item hs-has-mega-menu u-header__nav-item " data-event="hover"
                        data-animation-in="slideInUp" data-animation-out="fadeOut" data-position="left">
                        <a id="basicMegaMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle"
                            href="javascript:;" aria-haspopup="true" aria-expanded="false"> {{$c->category}}</a>


                        <!-- Nav Item - Mega Menu -->
                        <div class="hs-mega-menu  vmm-tfw u-header__sub-menu" aria-labelledby="basicMegaMenu">
                            <div class="vmm-bg">
                                {{-- <img class="img-fluid" src="../../assets/img/500X400/img1.png" alt="Image Description"> --}}
                            </div>
                            <div class="row u-header__mega-menu-wrapper">
                                <div class="col mb-3 mb-sm-0">
                                    <span class="u-header__sub-menu-title"> {{$c->category}} </span>
                                    <ul class="u-header__sub-menu-nav-group mb-3">
                                        @foreach($sub_cats as $sub)
                                        <li><a class="nav-link u-header__sub-menu-nav-link"
                                                href="/search-category/{{$sub->category}}">{{$sub->category}}</a></li>
                                        @endforeach
                                        <li>
                                            {{-- <a class="nav-link u-header__sub-menu-nav-link u-nav-divider border-top pt-2 flex-column align-items-start"
                                                href="#">
                                                <div class="">All Electronics</div>
                                                <div class="u-nav-subtext font-size-11 text-gray-30">Discover more
                                                    products</div>
                                            </a> --}}
                                        </li>
                                    </ul>
                                </div>

                                {{-- <div class="col mb-3 mb-sm-0">
                                            <span class="u-header__sub-menu-title">Office & Stationery</span>
                                            <ul class="u-header__sub-menu-nav-group">
                                                <li><a class="nav-link u-header__sub-menu-nav-link" href="#">All Office & Stationery</a></li>
                                            </ul>
                                        </div> --}}
                            </div>
                        </div>
                        <!-- End Nav Item - Mega Menu -->
                    </li>
                    <!-- End Nav Item MegaMenu-->
                    @endforeach

                </ul>
            </div>
        </nav>
