                <!-- Topbar -->
                <div class="u-header-topbar py-2 d-none d-xl-block">
                    <div class="container">
                        <div class="d-flex align-items-center">
                            <div class="topbar-left">
                                <a >Welcome to Largest Online Strore in Sri Lanka</a>
                            </div>
                            <div class="topbar-right ml-auto">
                                <ul class="list-inline mb-0">
                                    @if (Auth::check())
                                    <li class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                        <a href="/seller" class="u-header-topbar__nav-link"><i class="ec ec-map-pointer mr-1"></i> Sell on Lak</a>
                                    </li>
                                    @endif
                                    <li class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                        <a href="#" class="u-header-topbar__nav-link"><i class="ec ec-map-pointer mr-1"></i> Store Locator</a>
                                    </li>
                                    <li class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                        <a href="/my-orders" class="u-header-topbar__nav-link"><i class="ec ec-transport mr-1"></i> My Orders</a>
                                    </li>


                                    {{-- @guest
                                    <li class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                        <!-- Account Sidebar Toggle Button -->
                                        <a id="sidebarNavToggler" href="javascript:;" role="button" class="u-header-topbar__nav-link"
                                            aria-controls="sidebarContent"
                                            aria-haspopup="true"
                                            aria-expanded="false"
                                            data-unfold-event="click"
                                            data-unfold-hide-on-scroll="false"
                                            data-unfold-target="#sidebarContent"
                                            data-unfold-type="css-animation"
                                            data-unfold-animation-in="fadeInRight"
                                            data-unfold-animation-out="fadeOutRight"
                                            data-unfold-duration="500">
                                            <i class="ec ec-user mr-1"></i> Register <span class="text-gray-50">or</span> Sign in
                                        </a>
                                        <!-- End Account Sidebar Toggle Button -->
                                    </li>
                                    @else
                                    <li class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                        <a
                                        class="u-header-topbar__nav-link"
                                        href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="ec ec-user mr-1"></i>
                                    {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                            style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                    @endguest --}}
                                    @guest

                                    <li class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                        <a
                                        class="u-header-topbar__nav-link"
                                        href="{{ route('login') }}"><i class="ec ec-user mr-1"></i> Login</a>
                                    </li>
                                        @if (Route::has('register'))
                                        <li class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                        <a
                                        class="u-header-topbar__nav-link"
                                        href="{{ route('register') }}">
                                        <i class="ec ec-user mr-1"></i>
                                        {{ __('Register') }}</a>
                                    </li>
                                        @endif
                                    @else
                                    <li class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                        <a href="/my-account/{{Auth::user()->id}}" class="u-header-topbar__nav-link"><i class="ec ec-user mr-1"></i> My Account</a>
                                        </li>
                                    <li class="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                        <a
                                        class="u-header-topbar__nav-link"
                                        href="{{ route('logout') }}" onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                                             <i class="far fa-sign-out"></i>
                                            {{ __('Logout') }}
                                        </a>
                                    </li>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                            style="display: none;">
                                            @csrf
                                        </form>
                                    @endguest
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Topbar -->
