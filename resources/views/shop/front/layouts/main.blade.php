@include('shop.front.layouts.common_headers.common_header')
@include('layouts.inc.messages')
@yield('content')

@include('shop.front.layouts.footer')
