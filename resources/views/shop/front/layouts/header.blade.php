<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Title -->
        <title>Lak.lk</title>

        <!-- Required Meta Tags Always Come First -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">

        <!-- CSS Implementing Plugins -->
        <link rel="stylesheet" href={{asset("/front_new/vendor/font-awesome/css/fontawesome-all.min.css")}}>
        <link rel="stylesheet" href={{asset("/front_new/css/font-electro.css")}}>

        <link rel="stylesheet" href={{asset("/front_new/vendor/animate.css/animate.min.css")}}>
        <link rel="stylesheet" href={{asset("/front_new/vendor/hs-megamenu/src/hs.megamenu.css")}}>
        <link rel="stylesheet" href={{asset("/front_new/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css")}}>
        <link rel="stylesheet" href={{asset("/front_new/vendor/fancybox/jquery.fancybox.css")}}>
        <link rel="stylesheet" href={{asset("/front_new/vendor/slick-carousel/slick/slick.css")}}>
        <link rel="stylesheet" href={{asset("/front_new/vendor/bootstrap-select/dist/css/bootstrap-select.min.css")}}>

        <!-- CSS Electro Template -->
        <link rel="stylesheet" href={{asset("/front_new/css/theme.css")}}>

        {{-- ajax --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    {{-- sweet alert --}}
    {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.js"></script>
    </head>

    <body>
