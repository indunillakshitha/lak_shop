@extends('shop.front.layouts.main')

@section('content')

<!-- ========== MAIN CONTENT ========== -->
<main id="content" role="main" class="cart-page">
    <!-- breadcrumb -->
    <div class="bg-gray-13 bg-md-transparent">
        <div class="container">
            <!-- breadcrumb -->
            {{-- <div class="my-md-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="../home/index.html">Home</a></li>
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Wishlist</li>
                    </ol>
                </nav>
            </div> --}}
            <!-- End breadcrumb -->
        </div>
    </div>
    <!-- End breadcrumb -->

    <div class="container">
        <div class="my-6">
            <h1 class="text-center">Wishlist</h1>
        </div>
        <div class="mb-16 wishlist-table">
            <form class="mb-4" action="#" method="post">
                <div class="table-responsive">
                    <table class="table" cellspacing="0">
                        <thead>
                            <tr>
                                <th class="product-thumbnail">&nbsp;</th>
                                <th class="product-name">Product</th>
                                <th class="product-price">Unit Price</th>
                                <th class="product-Stock w-lg-15">Stock Status</th>
                                <th class="product-subtotal min-width-200-md-lg">&nbsp;</th>
                                <th class="product-remove">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($wishlist as $w)
                            <tr>

                                <td class="d-none d-md-table-cell">
                                    <a href="/view-ad/{{$w->ad_id}}"><img class="img-fluid max-width-100 p-1 border border-color-1" src={{asset(env('IMAGE_LOCATION').$w->img_1)}} alt="Image Description"></a>
                                </td>

                                <td data-title="Product">
                                    <a href="/view-ad/{{$w->ad_id}}" class="text-gray-90"> {{$w->title}} </a>
                                </td>

                                <td data-title="Unit Price">
                                <span class="">Rs.{{$w->price}}</span>
                                </td>

                                <td data-title="Stock Status">
                                    <!-- Stock Status -->
                                    <span>In stock</span>
                                    <!-- End Stock Status -->
                                </td>

                                {{-- <td>
                                    <button type="button" class="btn btn-primary-dark-w mb-3 mb-md-0 font-weight-normal px-5 px-md-4 px-lg-5 w-100 w-md-auto">Add to Cart</button>
                                </td> --}}
                                <td class="text-center">
                                    <a href="#"
                                    onclick="alert_remove({{$w->id}}, 'The item will be removed from the Wishlist', '/remove-wishlist-item')"
                                    class="text-gray-32 font-size-26">×</a>
                                </td>
                            </tr>
                           @endforeach
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>
</main>
<!-- ========== END MAIN CONTENT ========== -->
@endsection
