@extends('shop.front.layouts.main')

@section('content')
<!-- ========== MAIN CONTENT ========== -->
<main id="content" role="main" class="cart-page">
   <br><br><hr>

    <div class="container">
        <div class="mb-4">
            <h1 class="text-center">Cart</h1>
        </div>
        <div class="mb-10 cart-table">
            <form class="mb-4" action="#" method="post">
                <table class="table" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="product-thumbnail">&nbsp;</th>
                            <th class="product-name">Product</th>
                            <th class="product-price">Price</th>
                            <th class="product-quantity w-lg-15">Quantity</th>
                            <th class="product-subtotal">Total</th>
                            <th class="product-remove">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                                $show_total= 0
                        @endphp
                        @foreach ($cart as $item)
                            
                       
                        <tr class="">
                            <td class="d-none d-md-table-cell">
                                <a href="/view-ad/{{$item->ad_id}}"</a><img class="img-fluid max-width-100 p-1 border border-color-1" src={{asset(env('IMAGE_LOCATION').$item->img_1)}} alt="Image Description"></a>
                            </td>

                            <td data-title="Product">
                                <a href="/view-ad/{{$item->ad_id}}"class="text-gray-90">{{$item->title}}</a>
                            </td>

                            <td data-title="Price">
                                <span class="" id="price_{{$item->ad_id}}">Rs.{{$item->price}}</span>
                            </td>

                            <td data-title="Quantity">
                                <span class="sr-only">Quantity</span>
                                <!-- Quantity -->
                                <div class="border rounded-pill py-1 width-122 w-xl-80 px-3 border-color-1">
                                    <div class="js-quantity row align-items-center">
                                        <div class="col">
                                            <input 
                                            id="qty_{{$item->ad_id}}""
                                            class="js-result form-control h-auto border-0 rounded p-0 shadow-none" 
                                            min="0" max="{{$item->adQty}}"
                                             type="number" value={{$item->quantity}}

                                             oninput="item_total_{{$item->ad_id}}.textContent=this.value*{{$item->price}},

                                                update_cart(
                                                    {{$item->ad_id}},
                                                    qty_{{$item->ad_id}},
                                                    item_total_{{$item->ad_id}},
                                                    {{$item->price}},
                                                    this
                                                )

                                                cal_total()
                                                "
                                             >
                                        </div>

                                    </div>
                                </div>
                                <!-- End Quantity -->
                            </td>

                            <td data-title="Total">
                                Rs.<span class="shoping__cart__total" id="item_total_{{$item->ad_id}}">{{$item->total}}</span>
                            </td>
                            <td class="text-center">
                                <a href="#" 
                                onclick="alert_remove({{$item->id}}, 'The item will be removed from the Cart', '/remove-cart-item')"
                                class="text-gray-32 font-size-26">×</a>
                            </td>
                        </tr>
                        @php
                            $show_total += $item->total
                        @endphp
                        @endforeach
                        
                        <tr>
                            <td colspan="6" class="border-top space-top-2 justify-content-center">
                                <div class="pt-md-3">
                                    <div class="d-block d-md-flex flex-center-between">
                                        <div class="mb-3 mb-md-0 w-xl-40">
                                            <!-- Apply coupon Form -->
                                            <form class="js-focus-state">
                                                <label class="sr-only" for="subscribeSrEmailExample1">Coupon code</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="text" id="subscribeSrEmailExample1" placeholder="Coupon code" aria-label="Coupon code" aria-describedby="subscribeButtonExample2" required>
                                                    <div class="input-group-append">
                                                        <button class="btn btn-block btn-dark px-4" type="button" id="subscribeButtonExample2"><i class="fas fa-tags d-md-none"></i><span class="d-none d-md-inline">Apply coupon</span></button>
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- End Apply coupon Form -->
                                        </div>
                                        <div class="d-md-flex">
                                            {{-- <button type="button" class="btn btn-soft-secondary mb-3 mb-md-0 font-weight-normal px-5 px-md-4 px-lg-5 w-100 w-md-auto">Update cart</button> --}}
                                            <a href="/checkout/{{Auth::user()->id}}" class="btn btn-primary-dark-w ml-md-2 px-5 px-md-4 px-lg-5 w-100 w-md-auto d-none d-md-inline-block">Proceed to checkout</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        
                    </tbody>
                </table>
            </form>
        </div>
        <div class="mb-8 cart-total">
            <div class="row">
                <div class="col-xl-5 col-lg-6 offset-lg-6 offset-xl-7 col-md-8 offset-md-4">
                    <div class="border-bottom border-color-1 mb-3">
                        <h3 class="d-inline-block section-title mb-0 pb-2 font-size-26">Cart totals</h3>
                    </div>
                    <table class="table mb-3 mb-md-0">
                        <tbody>
                            <tr class="cart-subtotal">
                                <th>Subtotal</th>
                                <td data-title="Subtotal"><span class="amount">Rs. {{$show_total}}</span></td>
                            </tr>
                            
                            <tr class="order-total">
                                <th>Total</th>
                                <td data-title="Total"><strong><span id="show_total" class="amount">Rs. {{$show_total}}</span></strong></td>
                            </tr>
                        </tbody>
                    </table>
                    <a 
                    href="/checkout/{{Auth::user()->id}}"
                    type="button" class="btn btn-primary-dark-w ml-md-2 px-5 px-md-4 px-lg-5 w-100 w-md-auto d-md-none">Proceed to checkout</a>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- ========== END MAIN CONTENT ========== -->

<script>

    $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
    
                //add item to cart (can use globally)
                function update_cart(ad_id, quantityElement, totalElement, ad_price, input){
    
                    let quantity = parseInt(quantityElement.value)
                    let price = parseInt(totalElement.textContent)
    
                    // console.log(quantity);
                    if(input.max < quantity) {
                        console.log('Insufficient_Quantity');;
                        quantityElement.value = input.max
                        totalElement.textContent = quantityElement.value*ad_price
                        return
                    }
                    $.ajax({
                        type: 'GET',
                        url: '{{('/update-cart-item')}}',
                        data: {
                            ad_id,
                            quantity ,
                            price,
                        } ,
                        success: function(data){
                            // console.log(data);
                            return timer_swal()
                        }
                    })
                }
    
                function cal_total(){
                    let arr = document.querySelectorAll('.shoping__cart__total')
    
                    let total = 0;
                    arr.forEach(a => {
                        total += parseFloat(a.textContent)
                    })
                    show_total.textContent = `Rs.${total}`
                    // console.log(total);
                }
    
    </script>
@endsection