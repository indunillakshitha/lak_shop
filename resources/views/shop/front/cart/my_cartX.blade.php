@extends('shop.front.layouts.main')

@section('content')
 <!-- Shoping Cart Section Begin -->
 <section class="shoping-cart spad" >
    <div class="container" >
        <div class="row">
            <div class="col-lg-12">
                <div class="shoping__cart__table">
                    <table>
                        <thead>
                            <tr>
                                <th class="shoping__product">Products</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Total</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $show_total= 0
                            @endphp
                            @foreach ($cart as $c_item)
                            <tr>
                                <td class="shoping__cart__item">
                                    <img src="{{asset(env('IMAGE_LOCATION').$c_item->img_1)}}" alt="" width="100px" height="100px">
                                    <h5><a href="/view-ad/{{$c_item->ad_id}}">{{$c_item->title}} </a> </h5>
                                </td>
                            <td class="shoping__cart__price" id="price_{{$c_item->ad_id}}">
                                    Rs.{{$c_item->price}}
                                </td>
                                <td class="shoping__cart__quantity">
                                    <div class="quantity"
                                    {{-- style="
                                    width: 140px;
                                    height: 50px;
                                    display: inline-block;
                                    position: relative;
                                    text-align: center;
                                    background: #f5f5f5;
                                    margin-bottom: 5px;

                                    " --}}
                                    >
                                        {{-- <div class="pro-qty"> --}}

                                        <input min="0" max="{{$c_item->adQty}}" id="qty_{{$c_item->ad_id}}"



                                        style="
                                        height: 50px;
                                        width: 66px;
                                        font-size: 16px;
                                        color: #6f6f6f;
                                        border: none;
                                        background: #f5f5f5;
                                        text-align: center;
                                        "
                                        type="number" value="{{$c_item->quantity}}"

                                        oninput="
                                        item_total_{{$c_item->ad_id}}.textContent=this.value*{{$c_item->price}},

                                        update_cart(
                                            {{$c_item->ad_id}},
                                            qty_{{$c_item->ad_id}},
                                            item_total_{{$c_item->ad_id}},
                                            {{$c_item->price}},
                                            this
                                        )

                                        cal_total()
                                        // show_total.value =
                                        "

                                            {{-- // onchange="update_cart(
                                            //     {{$c_item->ad_id}},
                                            //     qty_{{$c_item->ad_id}},
                                            //     item_total_{{$c_item->ad_id}},
                                            //     {{$c_item->price}},
                                            //     this
                                            //     )" --}}
                                        >
                                        {{-- </div> --}}
                                    </div>
                                </td>
                            <td class="shoping__cart__total" id="item_total_{{$c_item->ad_id}}">
                                    Rs.{{$c_item->total}}
                                {{-- <input type="text" > --}}
                                </td>
                                <td class="shoping__cart__item__close">
                                    <span class="icon_close"
                                    onclick="alert_remove({{$c_item->id}}, 'The item will be removed from the Cart', '/remove-cart-item')"
                                    ></span>
                                </td>
                            </tr>
                             @php
                                 $show_total += $c_item->total
                             @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                {{-- <div class="shoping__cart__btns">
                    <a href="/" class="primary-btn cart-btn-primary "><< CONTINUE SHOPPING</a>
                    <a href="#" class="primary-btn pull-right"><span class="icon_loading"></span>
                        Update Cart</a>
                </div> --}}
            </div>
            <div class="col-lg-6">
                <div class="shoping__continue">
                    <div class="shoping__discount">
                        <h5>Discount Codes</h5>
                        <form action="#">
                            <input type="text" placeholder="Enter your coupon code">
                            <button type="submit" class="site-btn">APPLY COUPON</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="shoping__checkout">
                    <h5>Cart Total</h5>
                    <ul>
                        {{-- <li>Subtotal <span  >Rs. {{$show_total}} </span></li> --}}
                        <li>Total <span id="show_total">Rs. {{$show_total}}  </span></li>
                    </ul>
                <a href="/checkout/{{Auth::user()->id}}" class="primary-btn">PROCEED TO CHECKOUT</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Shoping Cart Section End -->

<script>

$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //add item to cart (can use globally)
            function update_cart(ad_id, quantityElement, totalElement, ad_price, input){

                let quantity = parseInt(quantityElement.value)
                let price = parseInt(totalElement.textContent)

                // console.log(quantity);
                if(input.max < quantity) {
                    console.log('Insufficient_Quantity');;
                    quantityElement.value = input.max
                    totalElement.textContent = quantityElement.value*ad_price
                    return
                }
                $.ajax({
                    type: 'GET',
                    url: '{{('/update-cart-item')}}',
                    data: {
                        ad_id,
                        quantity ,
                        price,
                    } ,
                    success: function(data){
                        // console.log(data);
                        return timer_swal()
                    }
                })
            }

            function cal_total(){
                let arr = document.querySelectorAll('.shoping__cart__total')

                let total = 0;
                arr.forEach(a => {
                    total += parseFloat(a.textContent)
                })
                show_total.textContent = `Rs.${total}`
                // console.log(total);
            }

</script>
@endsection
