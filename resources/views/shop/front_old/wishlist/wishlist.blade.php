@extends('shop.front.layouts.main')

@section('content')
<!-- Shoping Cart Section Begin -->
<section class="shoping-cart spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="shoping__cart__table">
                    <table>
                        <thead>
                            <tr>
                                <th class="shoping__product">Products</th>
                                <th>Price</th>
                                {{-- <th></th> --}}
                                <th>Quantity in Stock</th>
                                {{-- <th>Total</th> --}}
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($wishlist as $w)
                            <tr>
                                <td class="shoping__cart__item">
                                    <img src={{asset(env('IMAGE_LOCATION').$w->img_1)}} alt="" width="100px" image="100px">
                                    <h5><a href="/view-ad/{{$w->ad_id}}">{{$w->title}} </a></h5>
                                </td>
                                <td class="shoping__cart__price">
                                    Rs.{{$w->price}}
                                </td>
                                {{-- <td></td> --}}
                                <th class="">
                                        {{$w->quantity}}
                                </th>
                                {{-- <td class="shoping__cart__total">
                                    Rs.{{$w->total}}
                                </td> --}}
                                <td class="shoping__cart__item__close">
                                    <span
                                    onclick="alert_remove({{$w->id}}, 'The item will be removed from the Wishlist', '/remove-wishlist-item')"
                                    class="icon_close" title="Remove from Wishlist" ></span>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- Shoping Cart Section End -->
@endsection
