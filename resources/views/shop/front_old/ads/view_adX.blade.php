@extends('shop.front.layouts.main')

@section('content')

<div class="container">
    <h1>{{$ad->title}}</h1>
    <p>{{$ad->subtitle}}</p>
    <h3>Category - {{$ad->category}} | condition - {{$ad->condition}}</h3>
    <p>
        <h5>Condition Description</h5> {{$ad->condition_description}}
    </p>
    <p>
        <h5>Item Description</h5> {{$ad->item_description}}
    </p>

    <h4>Duration - {{$ad->duration}} | Price - {{$ad->price}} | Quantity - {{$ad->quantity}}</h4>
    <hr>
    <div class="row">
        <div class="col">

            @if($ad->img_1)
            <img src="{{asset(env('IMAGE_LOCATION').$ad->img_1)}}" alt="">
            @endif
        </div>
        <div class="col">
            @if($ad->img_2)
            <img src="{{asset(env('IMAGE_LOCATION').$ad->img_2)}}" alt="">
            @endif
        </div>
        <div class="col">
            @if($ad->img_3)
            <img src="{{asset(env('IMAGE_LOCATION').$ad->img_3)}}" alt="">
            @endif
        </div>
        <div class="col">
            @if($ad->img_4)
            <img src="{{asset(env('IMAGE_LOCATION').$ad->img_4)}}" alt="">
            @endif
        </div>
        <div class="col">
            @if($ad->img_5)
            <img src="{{asset(env('IMAGE_LOCATION').$ad->img_5)}}" alt="">
            @endif
        </div>
        <div class="col">
        <div class="pull-right my-5">
            <div class="form-group">
                <label for="">Choose Quantity</label>
                <input type="number" name="" id="quantity" class="form-control" value="1"
                max="{{$ad->quantity}}" min="1"
                oninput="price.value = this.value*{{$ad->price}}"
                >
            </div>
            <div class="form-group">
                <label for="">Price</label>
                <input type="number" name="" id="price" class="form-control" value="{{$ad->price}}" readonly>
            </div>
            @guest
            @else
            <button class="primary-btn"
            onclick="add_to_cart({{$ad->id}},quantity.value, price.value, this)"
            >
            <i class="fa fa-shopping-cart"></i> <br>
            Add to Cart</button>
            @endguest
            </div>
        </div>

    </div>
    <div class="row ">
        <div class="mr-auto"><a href="/" class="primary-btn"><< Continue Shopping</a></div>
        @guest
            @else
            <div class="ml-auto"><a  href="/my-cart/{{Auth::user()->id}}" class="primary-btn">View Cart >> </a></div>
        @endguest
    </div>



    <hr>
</div>

@endsection
@section('scripts')

{{-- <script>
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function add_to_cart(ad_id, quantity, price, button){
            // console.log(quantity);
            // button.classList.remove('primary-btn');
            // button.classList.add('featured__item__pic__hover');
            $.ajax({
                type: 'GET',
                url: '{{('/add-cart-item')}}',
                data: {
                    ad_id,
                    quantity,
                    price,
                } ,
                success: function(data){
                    // button.classList.remove('btn-featured__item__pic__hover');
                    // button.classList.add('primary-btn');
                    // console.log(data);
                    // if(data == 'success'){
                    //     location.reload()
                    // }
                    // return show_data(data)
                }
            })
        }
</script> --}}
@endsection
