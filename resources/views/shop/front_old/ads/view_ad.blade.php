@extends('shop.front.layouts.main')

@section('content')
<!-- Product Details Section Begin -->
<section class="product-details spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="product__details__pic">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        @for($i =1 ; $i <= $ad->imgs_count; $i++)
                            <ol class="carousel-indicators  bg-dark">
                                <li data-target="#carouselExampleIndicators" data-slide-to="{{$i-1}}" class="active">
                                </li>
                            </ol>
                                @endfor
                        <div class="carousel-inner">
                            @for($i =1 ; $i <= $ad->imgs_count; $i++)
                                @php
                                $img = 'img_'.$i
                                @endphp
                                <div class="carousel-item  @if($i==1) active @endif">
                                    <img class="d-block w-100" src="{{asset(env('IMAGE_LOCATION').$ad->$img)}}
                                    "
                                        alt="First slide"
                                        style="border:2px black solid;"
                                        >
                                </div>
                                @endfor
                        </div>
                        @if($i-1 != 1)
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                            data-slide="prev">
                            <span class="carousel-control-prev-icon  bg-dark" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                            data-slide="next">
                            <span class="carousel-control-next-icon bg-dark" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="product__details__text">
                    <h3>{{$ad->title}}</h3>
                    <div class="product__details__price">Rs.{{$ad->price}}</div>
                    {{-- <div class="product__details__rating" id="star_review">
                        <i class="fa fa-star-o fa-4x"></i>
                        <i class="fa fa-star-o fa-4x"></i>
                        <i class="fa fa-star-o fa-4x"></i>
                        <i class="fa fa-star-o fa-4x"></i>
                        <i class="fa fa-star-o fa-4x"></i>
                        <span>(18 reviews)</span>
                    </div> --}}
                    <div class="star-container">
                        <div class="post">
                            <div class="text">Thanks for your review</div>
                            <div class="edit">Edit Review</div>
                        </div>
                        <div class="star-widget">
                            <input type="radio" name="rate" id="rate-5">
                            <label for="rate-5" class="fa fa-star"></label>

                            <input type="radio" name="rate" id="rate-4">
                            <label for="rate-4" class="fa fa-star"></label>

                            <input type="radio" name="rate" id="rate-3">
                            <label for="rate-3" class="fa fa-star"></label>

                            <input type="radio" name="rate" id="rate-2">
                            <label for="rate-2" class="fa fa-star"></label>

                            <input type="radio" name="rate" id="rate-1">
                            <label for="rate-1" class="fa fa-star"></label>

                            <br>
                            <form action="#">
                                <header></header>
                                <div class="textarea">
                                    <textarea  cols="30" placeholder="Review this product..."></textarea>
                                </div>
                                <div class="btn">
                                    <button id="sub_rev_btn" type="">Review</button>
                                </div>
                            </form>
                        </div>

                    </div>

                    <script>
                        const btn = document.querySelector('#sub_rev_btn')
                        const post = document.querySelector('.post')
                        const widget = document.querySelector('.star-widget')
                        const editBtn = document.querySelector('.edit')

                        btn.onclick = () => {
                            widget.style.display = 'none'
                            post.style.display = 'block'

                            editBtn.onclick = () => {
                                widget.style.display = 'block'
                                post.style.display = 'none'
                            }
                            return false;
                        }
                    </script>

                    <style>
                        .star-container{
                            position: relative;
                            width: 400px;
                            background: #111;
                            padding: 20px 30px;
                            border: 1px solid #444;
                            border-radius: 5px;
                            display: flex;
                            align-items: center;
                            justify-content: center;
                            flex-direction: column;
                        }
                        .star-container .text{
                            font-size: 25px;
                            color: #666;
                            font-weight: 500;
                        }
                        .star-container .edit{
                            position: absolute;
                            right: 10px;
                            font-size: 16px;
                            top: 5px;
                            color: #666;
                            font-weight: 500;
                            cursor: pointer;
                        }

                        .star-container .post{
                            display: none;
                        }


                        .star-widget input{
                            display: none;
                        }
                        .star-widget label{
                            font-size: 40px;
                            color: #444;
                            padding: 5px;
                            float: right;
                            transition: all .2s ease;
                        }

                        input:not(:checked) ~ label:hover,
                        input:not(:checked) ~ label:hover ~ label{
                            color: #fd4;
                        }

                        input:checked ~ label{
                            color: #fd4;
                        }
                        input#rate-5:checked ~ label{
                            color: #fe7;
                            text-shadow: 0 0 20px #952;
                        }


                        input#rate-1:checked ~ form header:before{
                            content: "Hate it";
                        }
                        input#rate-2:checked ~ form header:before{
                            content: "Don't like it";
                        }
                        input#rate-3:checked ~ form header:before{
                            content: "Fine";
                        }
                        input#rate-4:checked ~ form header:before{
                            content: "Like it";
                        }

                        input#rate-5:checked ~ form header:before{
                            content: "Love it";
                        }

                        .star-container form {
                            display: none;
                        }

                        input:checked ~ form {
                            display: block;
                        }
                        form header{
                            width: 100%;
                            font-size: 25px;
                            color: #fe7;
                            font-weight: 500;
                            padding: 10px 10px;
                            margin: 5px 0 20px 0;
                            text-align: center;
                            transition: all .2 ease;
                        }

                        form .textarea {
                            height: 100px;
                            width: 100%;
                            overflow: hidden;
                        }
                        form .textarea textarea{
                            height: 100%;
                            width: 100%;
                            outline: none;
                            border: 1px solid #333;
                            background: #222;
                            padding: 10px;
                            font-size: 17px;
                            resize: none;
                            color: #eee;
                        }

                        form .btn {
                            height: 45px;
                            width: 100%;
                            margin: 15px 0;
                        }

                        form .btn button {
                            height: 100%;
                            width: 100%;
                            border: 1px solid #444;
                            outline : none;
                            background: #222;
                            color: #999;
                            font-size: 17px;
                            font-weight: 500;
                            text-transform: uppercase;
                            cursor: pointer;
                            transition: all .3s ease;
                        }

                        form .btn button:hover{
                            background: #1b1b1b;
                        }


                    </style>

<br>
                    <p>{{$ad->item_description}}</p>
                    {{-- <div class="product__details__quantity"> --}}
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="">Choose Quantity</label>
                                    <input type="number" name="" id="quantity" class="form-control" value="1"
                                        max="{{$ad->quantity}}" min="1" oninput="price.value = this.value*{{$ad->price}}">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="">Total Price</label> Rs.
                                    <input type="number" name="" id="price" class="form-control" value="{{$ad->price}}" readonly>
                                </div>
                            </div>
                        </div>
                    {{-- </div> --}}
                    @guest
                    <a class="primary-btn" href="{{route('login')}}">Login</a>
                    <a class="primary-btn" href="{{route('register')}}">Register</a>
                    @else
                    <button onclick="add_to_cart({{$ad->id}},quantity.value, price.value, quantity.max)" class="primary-btn"><i class="fa fa-shopping-cart"></i>  ADD TO CARD</button>
                    <a href="#" class="heart-icon"><span class="icon_heart_alt"></span></a>
                    @endguest
                    <ul>
                        <li><b>Availability</b> <span> {{$ad->quantity}} in Stock</span></li>
                        <li><b>Shipping</b> <span>01 day shipping. <samp>Free pickup today</samp></span></li>
                        <li><b>Weight</b> <span>0.5 kg</span></li>
                        <li><b>Share on</b>
                            <div class="share">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                                <a href="#"><i class="fa fa-pinterest"></i></a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="product__details__tab">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab"
                                aria-selected="true">Description</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab"
                                aria-selected="false">Information</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab"
                                aria-selected="false">Reviews <span>(1)</span></a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tabs-1" role="tabpanel">
                            <div class="product__details__tab__desc">
                                <h6>Products Infomation</h6>
                                <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.
                                    Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus. Vivamus
                                    suscipit tortor eget felis porttitor volutpat. Vestibulum ac diam sit amet quam
                                    vehicula elementum sed sit amet dui. Donec rutrum congue leo eget malesuada.
                                    Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat,
                                    accumsan id imperdiet et, porttitor at sem. Praesent sapien massa, convallis a
                                    pellentesque nec, egestas non nisi. Vestibulum ac diam sit amet quam vehicula
                                    elementum sed sit amet dui. Vestibulum ante ipsum primis in faucibus orci luctus
                                    et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam
                                    vel, ullamcorper sit amet ligula. Proin eget tortor risus.</p>
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem
                                    ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit aliquet
                                    elit, eget tincidunt nibh pulvinar a. Cras ultricies ligula sed magna dictum
                                    porta. Cras ultricies ligula sed magna dictum porta. Sed porttitor lectus
                                    nibh. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.
                                    Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Sed
                                    porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum
                                    sed sit amet dui. Proin eget tortor risus.</p>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabs-2" role="tabpanel">
                            <div class="product__details__tab__desc">
                                <h6>Products Infomation</h6>
                                <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.
                                    Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus.
                                    Vivamus suscipit tortor eget felis porttitor volutpat. Vestibulum ac diam
                                    sit amet quam vehicula elementum sed sit amet dui. Donec rutrum congue leo
                                    eget malesuada. Vivamus suscipit tortor eget felis porttitor volutpat.
                                    Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Praesent
                                    sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac
                                    diam sit amet quam vehicula elementum sed sit amet dui. Vestibulum ante
                                    ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                                    Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.
                                    Proin eget tortor risus.</p>
                                <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem
                                    ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit aliquet
                                    elit, eget tincidunt nibh pulvinar a. Cras ultricies ligula sed magna dictum
                                    porta. Cras ultricies ligula sed magna dictum porta. Sed porttitor lectus
                                    nibh. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.</p>
                            </div>
                        </div>
                        <div class="tab-pane" id="tabs-3" role="tabpanel">
                            <div class="product__details__tab__desc">
                                <h6>Review Product</h6>
                                <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.
                                    Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus.
                                    Vivamus suscipit tortor eget felis porttitor volutpat. Vestibulum ac diam
                                    sit amet quam vehicula elementum sed sit amet dui. Donec rutrum congue leo
                                    eget malesuada. Vivamus suscipit tortor eget felis porttitor volutpat.
                                    Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Praesent
                                    sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac
                                    diam sit amet quam vehicula elementum sed sit amet dui. Vestibulum ante
                                    ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                                    Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.
                                    Proin eget tortor risus.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Product Details Section End -->

<!-- Related Product Section Begin -->
<section class="related-product">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title related__product__title">
                    <h2>Related Products</h2>
                </div>
            </div>
        </div>
        <div class="row ">
            @foreach ($related as $ad)
            <div class="col-lg-2 col-md-4 col-sm-6 mix cat{{$ad->category}} ">
                <div class="card ">
                 {{-- <div class="featured__item"> --}}
                     <div class="featured__item__pic "
                          >
                          <img src="{{asset(env('IMAGE_LOCATION').$ad->img_1)}}"  alt="">
                         <ul class="featured__item__pic__hover">
                             <li
                             onclick="add_to_wishlist({{$ad->id}})"
                             ><a   title="Add to Wishlist"><i class="fa fa-heart"></i></a></li>
                             <li
                             onclick="add_to_cart({{$ad->id}}, 1, {{$ad->price}}, this)"
                             ><a
                              title="Add to Cart" ><i class="fa fa-shopping-cart"></i></a>
                             </li>
                         </ul>
                     </div>
                     <div class="featured__item__text">
                         <h6><a href="/view-ad/{{$ad->id}}">{{$ad->title}}</a></h6>
                     <h5>Rs.{{$ad->price}}</h5>
                     </div>
                 {{-- </div> --}}
                </div>
                <br>
         </div>
            @endforeach
        </div>
    </div>
</section>
<!-- Related Product Section End -->

@endsection
