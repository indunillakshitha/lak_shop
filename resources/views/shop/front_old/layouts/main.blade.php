@include('shop.front.layouts.header')
<body >
    {{-- <div class="container"> --}}
        @include('layouts.inc.messages')
        @yield('content')
    {{-- </div> --}}
    {{-- @yield('scripts') --}}
    <script>
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //add item to cart (can use globally)
            function add_to_cart(ad_id, quantity, price, max){

                max  = parseInt(max)
                quantity = parseInt(quantity)
                if(max < quantity){
                    return timer_swal('Insuffiecient Quantity', 'error')
                    .then(() => location.reload())
                }
                $.ajax({
                    type: 'GET',
                    url: '{{('/add-cart-item')}}',
                    data: {
                        ad_id,
                        quantity,
                        price,
                    } ,
                    success: function(data){
                        console.log(data);
                        if(data == 'insuf_qty'){
                            return timer_swal('Insuffiecient Quantity', 'error')
                        }
                        return timer_swal()
                    }
                })
            }

            function timer_swal(title = 'Updating Cart', icon = null, timer = 700){
                let timerInterval
                return Swal.fire({
                    allowOutsideClick: false,
                    // position : 'bottom',
                    // title: 'Updating Cart',
                    // timer: 700,
                    // timerProgressBar: true,
                    icon,
                    title,
                    timer,
                    willOpen: () => {
                        Swal.showLoading()
                        timerInterval = setInterval(() => {
                        const content = Swal.getContent()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                            b.textContent = Swal.getTimerLeft()
                            }
                        }
                        }, 100)
                    },
                    onClose: () => {
                        clearInterval(timerInterval)
                    }
                })
            }


            function add_to_wishlist(ad_id){
                $.ajax({
                    type: 'GET',
                    url: '{{('/add-wishlist-item')}}',
                    data: {
                        ad_id
                    } ,
                    success: function(data){
                        console.log(data);
                        if(data == 'item_added_to_wishlist'){
                            return timer_swal('Added to Wishlist', 'success', 1000)
                        }

                        return timer_swal('Item already on Wishlist', 'info', 1000)
                    }
                })
            }

            function alert_remove(id, text, url){
                Swal.fire({
                    // position: 'bottom-end',
                    title: 'Are you sure?',
                    // text: "The item will be removed from the Cart",
                    text,
                    icon: 'warning',
                    showCancelButton: true,
                    // cancelButtonColor: '#7fad39',
                    confirmButtonColor: '#d33',
                    confirmButtonText: 'Remove Item'
                    }).then((result) => {
                    if (result.isConfirmed) {
                        // console.log(12);
                        return remove_ajax(id, url)
                    }
                })
            }

            function remove_ajax(id, url){
                // console.log(url);
                $.ajax({
                    type: 'GET',
                    // url: '{{('/remove-cart-item')}}',
                    url,
                    data: {
                        id
                    } ,
                    success: function(data){
                        console.log(data);
                        Swal.fire(
                        'Deleted!',
                        'Item has been removed.',
                        'success'
                        ).then(() => {
                            return location.reload()
                        })
                    }
                })
            }
    </script>
    </body>
@include('shop.front.layouts.footer')
