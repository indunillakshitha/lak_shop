<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Ogani Template">
    <meta name="keywords" content="Ogani, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lak.lk</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('main/shop/index/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/shop/index/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/shop/index/css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/shop/index/css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/shop/index/css/jquery-ui.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/shop/index/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/shop/index/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('main/shop/index/css/style.css')}}" type="text/css">

    {{-- jquery --}}
    {{-- <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script> --}}
    {{-- ajax --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    {{-- sweet alert --}}
    {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.js"></script>
</head>

<body>

    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Humberger Begin -->
    <div class="humberger__menu__overlay"></div>
    <div class="humberger__menu__wrapper">
        <div class="humberger__menu__logo">
            <a href="#"><img src="{{asset('main/shop/index/img/logo.png')}}" alt=""></a>
        </div>
        <div class="humberger__menu__cart">
            {{-- <ul>
                <li><a href="#"><i class="fa fa-heart"></i> <span>1</span></a></li>
                <li><a href="#"><i class="fa fa-shopping-bag"></i> <span>3</span></a></li>
            </ul> --}}
            {{-- <div class="header__cart__price">item: <span>$150.00</span></div> --}}
            {{-- <div><button class="btn btn-primary">Post Ad</button></div> --}}

        </div>
        <div class="humberger__menu__widget">
            <div class="header__top__right__language">
                <img src="{{asset('main/shop/index/img/language.png')}}" alt="">
                <div>English</div>
                {{-- <span class="arrow_carrot-down"></span>
                <ul>
                    <li><a href="#">Spanis</a></li>
                </ul> --}}
            </div>
            <div class="header__top__right__auth">
                @guest
                            <div class="header__top__right__auth">
                                <a href="{{ route('login') }}"><i class="fa fa-user"></i> Login</a>
                            </div> |
                            <div class="header__top__right__auth">
                                @if (Route::has('register'))
                                <a href="{{ route('register') }}">{{ __('Register') }}</a>
                                @endif
                            </div>
                            @else
                            <div class="header__top__right__auth">
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>
                            </div>
                            @endguest
            </div>
        </div>
        <nav class="humberger__menu__nav mobile-menu">
            <ul>
                <li class="active"><a href="/">Home</a></li>
                @guest
                @else
                <li><a href="/wishlist">Wishlist</a></li>
                @endguest
                </li>
                @guest
                @else
                <li><a href="/create-ad" class="">Post Ad</a></li>
                <li><a href="/my-cart/{{Auth::user()->id}}"
                    class="">View Cart</a></li>
                @endguest
            </ul>
        </nav>
        <div id="mobile-menu-wrap"></div>
        <div class="header__top__right__social">
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-pinterest-p"></i></a>
        </div>
        <div class="humberger__menu__contact">
            <ul>
                {{-- <li><i class="fa fa-envelope"></i> lak@gmail.com</li> --}}
                {{-- <li>Free Shipping for all Order of $99</li> --}}
            </ul>
        </div>
    </div>
    <!-- Humberger End -->

    <!-- Header Section Begin -->
    <header class="header">
        <div class="header__top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="header__top__left">
                            <ul>
                                {{-- <li><i class="fa fa-envelope"></i> hello@colorlib.com</li>
                                <li>Free Shipping for all Order of $99</li> --}}
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="header__top__right">
                            <div class="header__top__right__social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            </div>
                            <div class="header__top__right__language">
                                <img src="{{asset('main/shop/index/img/language.png')}}" alt="">
                                <div>English</div>
                                <span class="arrow_carrot-down"></span>
                                {{-- <ul>
                                    <li><a href="#">Spanis</a></li>
                                    <li><a href="#">English</a></li>
                                </ul> --}}
                            </div>
                            @guest
                            <div class="header__top__right__auth">
                                <a href="{{ route('login') }}"><i class="fa fa-user"></i> Login</a>
                            </div> |
                            <div class="header__top__right__auth">
                                @if (Route::has('register'))
                                <a href="{{ route('register') }}">{{ __('Register') }}</a>
                                @endif
                            </div>
                            @else
                            <div class="header__top__right__auth">
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>
                            </div>
                            @endguest
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="header__logo">
                        <a href="/"><img src="{{asset('main/shop/index/img/logo.png')}}" alt=""></a>
                        {{-- <a href="/"><img src="{{asset('public/images/ads/1602433972104575118.jpeg')}}" alt=""></a>
                        --}}
                    </div>
                </div>
                <div class="col-lg-6">
                    <nav class="header__menu">
                        <ul>
                            <li class="active"><a href="/">Home</a></li>
                            {{-- <li><a href="./shop-grid.html">Shop</a></li>
                            <li><a href="#">Pages</a>
                                <ul class="header__menu__dropdown">
                                    <li><a href="./shop-details.html">Shop Details</a></li>
                                    <li><a href="./shoping-cart.html">Shoping Cart</a></li>
                                    <li><a href="./checkout.html">Check Out</a></li>
                                    <li><a href="./blog-details.html">Blog Details</a></li>
                                </ul>
                            </li> --}}
                            @guest
                            @else
                            <li><a href="/wishlist">Wishlist</a></li>
                            @endguest
                            {{-- <li><a href="./contact.html">Contact</a></li> --}}
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-3">
                    <div class="header__cart">
                        {{-- <ul>
                            <li><a href="#"><i class="fa fa-heart"></i> <span>1</span></a></li>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i> <span>3</span></a></li>
                        </ul> --}}
                        {{-- <div class="header__cart__price">item: <span>$150.00</span></div> --}}
                        @guest
                        @else
                        <div class="row"><a href="/create-ad" class="primary-btn mx-auto">Post Ad</a><a href="/my-cart/{{Auth::user()->id}}"
                                class="primary-btn mx-auto">View Cart</a></div>
                        @endguest
                    </div>
                </div>
            </div>
            <div class="humberger__open">
                <i class="fa fa-bars"></i>
            </div>
        </div>
    </header>
    <!-- Header Section End -->

    @php
        $cats = App\Category::all();
    @endphp
    <!-- Hero Section Begin -->
    <section class="hero hero-normal">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="hero__categories">
                        <div class="hero__categories__all">
                            <i class="fa fa-bars"></i>
                            <span>All Categories</span>
                        </div>
                        <ul>
                            @foreach ($cats as $c)
                                <li >
                                    <a data-toggle="modal" data-target="#exampleModal"
                                    onclick="get_sub_cats({{$c->id}}, this.textContent)"
                                    >{{$c->category_name}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="hero__search pull-right" >
                        <div class="hero__search__form">
                            <form action="/serach-products" method="POST">
                                @csrf
                                {{-- <div class="hero__search__categories">
                                    All Categories
                                    <span class="arrow_carrot-down"></span>
                                </div> --}}
                                <input type="text" name="search" id="gen_search_text" placeholder="What do yo u need?">
                                <button type="submit"
                                {{-- onclick="gen_search(gen_search_text.value)"  --}}
                                class="site-btn">SEARCH</button>
                            </form>
                        </div>
                        {{-- <div class="hero__search__phone">
                            <div class="hero__search__phone__icon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="hero__search__phone__text">
                                <h5>+65 11.188.888</h5>
                                <span>support 24/7 time</span>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Hero Section End -->

    <!-- Button trigger modal -->
{{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    Launch demo modal
  </button> --}}

  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <img class="mx-auto" width="100px" height="25px" src="{{asset('main/shop/index/img/logo.png')}}" alt="">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="modal_body">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
        </div>
      </div>
    </div>
  </div>

  <script>
      function get_sub_cats(id, cat_name){
        exampleModalLabel.textContent = cat_name
        modal_body.innerHTML = ''
        $.ajax({
            type: 'GET',
            url: '{{('/get-sub-cats')}}',
            data: {
                id
            } ,
            success: function(data){
                console.log(data);

                data.forEach(sub_cat => {
                    html = `
                    <a href="/serach-category?search=${sub_cat.category_id}" >${sub_cat.sub_category_name}</a> <br>
                    `
                    modal_body.innerHTML += html
                })
                return
            }
        })
      }

    //   function gen_search(text){
    //     //   console.log(text);
    //     $.ajax({
    //         type: 'GET',
    //         url: '{{('/gen-serach-text')}}',
    //         data: {
    //             text
    //         } ,
    //         success: function(data){
    //             // console.log(data);

    //             // return location = '/'
    //         }
    //     })
    //   }
  </script>






{{--
    1.0.0.1
255.255.255.255

1   2   3   4   5   6   7   8   9   10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31  32
128 64  32  16  8   4   2   1 . 128 64  32  16  8   4   2   1 . 128 64  32  16  8   4   2   1 . 128 64  32  16  8   4   2   1


1   0   1   1   0   0   0   0 . 1   1   1   0   0   1   0   0 . 0   0   1   0   0   0   0   0 . 1   1   1   0   0   1   1   0
1   0   1   1   0   0   0   0 . 1   1   1   0   0   1   0   0 . 0   0   1   0   0   0   0   0 . 1   0   0   0   0   0   0   0  <-network address  176.228.230.128  (a)
1   0   1   1   0   0   0   0 . 1   1   1   0   0   1   0   0 . 0   0   1   0   0   0   0   0 . 1   0   0   0   0   0   0   1  <-first host 176.228.280.129 (d)
1   0   1   1   0   0   0   0 . 1   1   1   0   0   1   0   0 . 0   0   1   0   0   0   0   0 . 1   1   1   1   1   1   1   0  <-last host 176.228.280.254 (e)
1   0   1   1   0   0   0   0 . 1   1   1   0   0   1   0   0 . 0   0   1   0   0   0   0   0 . 1   1   1   1   1   1   1   1  <-broadcast address 176.228.280.255 (f)



2017.6.II

176.228.32.230/25 <- class B  | 255.255.0.0  classful ip | classless ip

class A  255.0.0.0
class B  255.255.0.0
class C  255.255.255.0

------------------------------------------------------------------------------------------------
2016.5

168.173.70.134/29

1   2   3   4   5   6   7   8   9   10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31  32
128 64  32  16  8   4   2   1 . 128 64  32  16  8   4   2   1 . 128 64  32  16  8   4   2   1 . 128 64  32  16  8   4   2   1

1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   0   0   0   <- subnet mask  255.255.255.248 (b)
1   0   1   0   1   0   0   0 . 1   0   1   0   1   1   0   1 . 0   1   0   0   0   1   1   0 . 1   0   0   0   0   1   1   0
1   0   1   0   1   0   0   0 . 1   0   1   0   1   1   0   1 . 0   1   0   0   0   1   1   0 . 1   0   0   0   0   0   0   0   <-network ip 168.173.70.128
1   0   1   0   1   0   0   0 . 1   0   1   0   1   1   0   1 . 0   1   0   0   0   1   1   0 . 1   0   0   0   0   0   0   1   <-first host ip 168.173.70.129
1   0   1   0   1   0   0   0 . 1   0   1   0   1   1   0   1 . 0   1   0   0   0   1   1   0 . 1   0   0   0   0   1   1   0   <-last host ip 168.173.70.134
1   0   1   0   1   0   0   0 . 1   0   1   0   1   1   0   1 . 0   1   0   0   0   1   1   0 . 1   0   0   0   0   1   1   1   <-broadcast ip 168.173.70.135



a.  168 => class B               2^(32-29)-2 = 8-2 = 6
b.
f. available hosts = 6
--}}
