
{{-- ln -S /etc/phpmyadmin/apache.conf /etc/apache2/conf-available/phpmyadmin.conf --}}
@extends('auth.layout')
@section('content')
{{-- <div class="app-content content">
<div class="content-overlay"></div>
<div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body"> --}}
        <section class="row flexbox-container">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-5 py-5 mx-auto">
                {{-- <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0"> --}}
                    <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                        <div class="card-header border-0 pb-0">
                            <div class="card-title text-center">
                                <img src="{{asset('main/shop/admin/app-assets/images/logo/logo.png')}}" alt="branding logo">
                            </div>
                            <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2"><span>Easily Using</span></h6>
                        </div>
                        <div class="card-content">
                            <div class="text-center">
                                <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-facebook"><span class="la la-facebook"></span></a>
                                <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-twitter"><span class="la la-twitter"></span></a>
                                <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-linkedin"><span class="la la-linkedin font-medium-4"></span></a>
                                <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-github"><span class="la la-github font-medium-4"></span></a>
                            </div>
                            <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1"><span>OR Using
                                    Email</span></p>
                            <div class="card-body">
                                <form
                                method="POST"
                                action="{{ route('register') }}"
                                class="form-horizontal" action="index.html" novalidate>
                                @csrf
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <input
                                        placeholder="Enter Your Username"
                                        id="name" type="text" class="form-control
                                        @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus
                                        >

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        <div class="form-control-position">
                                            <i class="la la-user"></i>
                                        </div>
                                    </fieldset>
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <input
                                        placeholder="Enter Your Email"id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        <div class="form-control-position">
                                            <i class="la la-envelope"></i>
                                        </div>
                                    </fieldset>
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <input placeholder="Enter Your Password"id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                        <div class="form-control-position">
                                            <i class="la la-key"></i>
                                        </div>
                                    </fieldset>
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <input placeholder="Confirm Your Password" id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                        <div class="form-control-position">
                                            <i class="la la-key"></i>
                                        </div>
                                    </fieldset>

                                    <fieldset class="form-group position-relative has-icon-left">
                                        <div class="row">
                                            <div class="col-2">
                                                <label for="">Your Procince</label>
                                            </div>
                                            <div class="col-10">
                                                <select name="province_id" id="" class="form-control" required>
                                                    <option value="">Select Province</option>
                                                    @foreach (Illuminate\Support\Facades\DB::table('province_system_data')->get() as $province)
                                                        <option value="{{$province->id}}">{{$province->name_en}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <fieldset class="form-group position-relative has-icon-left">
                                        <div class="row">
                                            <div class="col-2">
                                                <label for="">Your District</label>
                                            </div>
                                            <div class="col-10">
                                                <select name="district_id" id="" class="form-control" required>
                                                    <option value="">Select District</option>
                                                    @foreach (Illuminate\Support\Facades\DB::table('district_system_data')->get() as $district)
                                                        <option value="{{$district->id}}">{{$district->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <fieldset class="form-group position-relative has-icon-left">
                                        <div class="row">
                                            <div class="col-2">
                                                <label for="">Your City</label>
                                            </div>
                                            <div class="col-10">
                                                <select name="city_id" id="" class="form-control" required>
                                                    <option value="">Select City</option>
                                                    @foreach (Illuminate\Support\Facades\DB::table('city_system_data')->get() as $city)
                                                        <option value="{{$city->id}}">{{$city->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <div class="form-group row">
                                        <div class="col-sm-6 col-12 text-center text-sm-left pr-0">
                                            <fieldset>
                                                <input type="checkbox" id="remember-me" class="chk-remember">
                                                <label for="remember-me"> Remember Me</label>
                                            </fieldset>
                                        </div>
                                        <div class="col-sm-6 col-12 float-sm-left text-center text-sm-right"><a href="recover-password.html" class="card-link"></a></div>
                                    </div>
                                    <button type="submit" class="btn btn-outline-info btn-block"><i class="la la-user"></i> Register</button>
                                </form>
                            </div>
                            <div class="card-body">
                                <a href="{{ route('login') }}" class="btn btn-outline-danger btn-block"><i class="ft-unlock"></i>
                                    Login</a>
                            </div>
                        </div>
                    </div>
                {{-- </div> --}}
            </div>
        </section>

    {{-- </div>
</div>
</div> --}}
<!-- END: Content-->
@endsection

{{-- @include('shop.admin.layouts.footer') --}}

