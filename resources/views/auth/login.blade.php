@extends('auth.layout')
{{-- @include('shop.admin.layouts.header') --}}

@section('content')
{{-- <div class="container"> --}}
    {{-- <div class="app-content content mx-auto my-3">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body"> --}}
                <section class="row flexbox-container ">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 my-5 py-5 mx-auto
                    {{-- d-flex align-items-center justify-content-center --}}
                    "
                    >
                        {{-- <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0"> --}}
                            <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                                <div class="card-header border-0">
                                    <div class="card-title text-center">
                                        <img src="{{asset('main/shop/admin/app-assets/images/logo/logo.png')}}"
                                            alt="branding logo">
                                    </div>
                                    <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
                                        <span>Easily Using</span></h6>
                                </div>
                                <div class="card-content">
                                    <div class="text-center">
                                        <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-facebook"><span
                                                class="la la-facebook"></span></a>
                                        <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-twitter"><span
                                                class="la la-twitter"></span></a>
                                        <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-linkedin"><span
                                                class="la la-linkedin font-medium-4"></span></a>
                                        <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-github"><span
                                                class="la la-github font-medium-4"></span></a>
                                    </div>
                                    <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1">
                                        <span>OR Using Account
                                            Details</span></p>
                                    <div class="card-body">
                                        {{-- ###################### --}}
                                        <form method="POST" action="{{ route('login') }}" class="form-horizontal">
                                            @csrf
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="text" class="form-control" name="email" id="user-name"
                                                    placeholder="Your Username" required>
                                                <div class="form-control-position">
                                                    <i class="la la-user"></i>
                                                </div>
                                            </fieldset>
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="password" class="form-control" name="password"
                                                    id="user-password" placeholder="Enter Password" required>
                                                <div class="form-control-position">
                                                    <i class="la la-key"></i>
                                                </div>
                                            </fieldset>
                                            <div class="form-group row">
                                                <div class="col-sm-6 col-12 text-center text-sm-left pr-0">
                                                    <fieldset>
                                                        <input class="chk-remember" type="checkbox" name="remember"
                                                            id="remember" {{ old('remember') ? 'checked' : '' }}>

                                                        <label class="form-check-label" for="remember">
                                                            {{ __('Remember Me') }}
                                                        </label>
                                                    </fieldset>
                                                </div>
                                                <div class="col-sm-6 col-12 float-sm-left text-center text-sm-right">
                                                    @if (Route::has('password.request'))
                                                    <a class="btn-link" href="{{ route('password.request') }}">
                                                        {{ __('Forgot Password?') }}
                                                    </a>
                                                    @endif
                                                </div>
                                            </div>
                                                    <button type="submit" class="btn btn-outline-info btn-block"><i
                                                        class="ft-unlock"></i>
                                                        {{ __('Login') }}
                                                    </button>
                                            </div>
                                            <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1">
                                                <span>New to Lak.lk
                                                    ?</span></p>
                                            <div class="card-body">
                                                <a href="{{ route('register') }}"
                                                    class="btn btn-outline-danger btn-block"><i class="la la-user"></i>
                                                    Register</a>
                                            </div>
                                        </form>
                                        {{-- ###################### --}}
                                    </div>

                                </div>
                            </div>
                        {{-- </div> --}}
                    </div>
                </section>

            {{-- </div>
        </div>
    </div> --}}
{{-- </div> --}}
@endsection

