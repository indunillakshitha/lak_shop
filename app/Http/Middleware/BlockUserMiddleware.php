<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class BlockUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check() && Auth::user()->blocked){
            $blocked = Auth::user()->blocked == '1';
            Auth::logout();

        if ($blocked == '1'){
            $message = 'your Account has been blocked, please contact us!';
        }
        return redirect('/login')->with('status' , $message)->withErrors(['email', 'your Account has been blocked, please contact us!']);
        }
        return $next($request);
    }
}
