<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Brand;
use App\Category;
use App\CategoryHasFields;
use App\CustomField;
use App\Helpers\LogActivity;
use App\ProductReview;
use App\SingleProductReviewData;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {
        $cats = Category::all();
        $brands = Brand::all();
        $cus_fields = CustomField::all();
        $cats_have_fields = CategoryHasFields::all();
        // $cats = Category::where('parent_id', 0)->get();
        // $subs = Category::where('parent_id', 0)->get();
        return view('shop.seller.create_ad.index', compact('cats', 'brands', 'cus_fields', 'cats_have_fields'));
    }


    public function store(Request $request)
    {
        // return $request;

            // $cus_fields = $request->fields;
            //  json_encode($cus_fields);

        $new_ad = Ad::create($request->all());

        // $new_ad->cus_fields = $cus_fields;

        if($request->file('imgs')){

            $images = $request->file('imgs');

            for($i = 0; $i < count($images); $i++){
                $path = 'images/ads';
                // $profile_path = env('ADMIN_ASSET_URL').$path;
                $new_ad['img_'.($i+1)] =  time() . rand() . '.' . $images[$i]->extension();
                $images[$i]->move(public_path($path), $new_ad['img_'.($i+1)]);
                // $images[$i]->move(public_path($profile_path), $new_ad['img_'.($i+1)]);
                // $images[$i]->move('images/ads', $new_ad['img_'.($i+1)]);
                // $path = $images[$i]->storeAs('public/images/ads',$new_ad['img_'.($i+1)]);
            }
        }
        $new_ad->user_id = Auth::user()->id;
        $new_ad->imgs_count = $i;
        $new_ad->SKU = 'LAK'.Auth::user()->id.'C'.$request->category.'S'.$request->sub_category.'P'.$new_ad->id;

        $new_ad->save();

        LogActivity::addToLog('Create new Ad', $new_ad->id);
        return redirect()->back();


        /**
         *  $otherImages = $request->file('imgs');

                $otherImgName['image'] = null;

                if (!empty($otherImages)) {
                    for ($i = 0; $i < count($otherImages); $i++) {
                        $path = 'images/product_imgs';
                        $otherImgName['image'] =  time() . rand() . '.' . $otherImages[$i]->extension();
                        $otherImages[$i]->move(public_path($path), $otherImgName['image']);

                        DB::table('new_free_ads_images')->insert([
                            'name' => $otherImgName['image'],
                            'free_ads_id' => $adId
                        ]);
                    }
                }
         */
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reviews = ProductReview::where('ad_id', $id)->orderBy('id','desc')->skip(0)->take(10)->get();

        if(SingleProductReviewData::where('ad_id', $id)->first()){
            $review_sys_data = SingleProductReviewData::where('ad_id', $id)->first();
        } else {
            $review_sys_data = null;
        }
        // return $review_sys_data;
        $ad = Ad::find($id);
        $related = Ad::where('category', $ad->category)->get();
        Ad::where('id', $id)->update(['view_count'=>$ad->view_count + 1]);
        // return $related;
        return view('shop.front.ads.view_ad', compact('ad', 'related', 'reviews', 'review_sys_data'));
    }

    public function edit(Ad $ad)
    {
        //
    }


    public function update(Request $request, Ad $ad)
    {
        //
    }


    public function destroy(Ad $ad)
    {
        //
    }

    public function genSearch(Request $request){
        // SELECT * FROM `ads` WHERE  title LIKE '%es%' OR subtitle LIKE '%es%'
        // return $request;

       $loaded_ads = DB::select(
           "
           SELECT * FROM `ads`
           WHERE  title LIKE '%$request->search%'
           OR subtitle LIKE '%$request->search%'
           OR category LIKE '%$request->search%'
           OR condition_description LIKE '%$request->search%'
           OR item_description LIKE '%$request->search%'
           "
       );
    //    $cats = Category::all();

    //    return $loaded_ads;

       /**
        * OR category LIKE '%$request->text%'
           OR condition LIKE '%$request->text%'
           OR condition_description LIKE '%$request->text%'
           OR item_description LIKE '%$request->text%'
        */
        return view('general_search', compact('loaded_ads'));
    }

    public function genSearchCategory($cat){
        // return $cat;
        $cat_id = Category::where('category', $cat)->first()->id;

        $loaded_ads = Ad::where('status', 1)->where('category', $cat_id)->get();
        return view('general_search', compact('loaded_ads'));


    }



}
