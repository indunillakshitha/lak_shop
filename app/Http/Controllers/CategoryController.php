<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\LogActivity;

class CategoryController extends Controller
{

    public function index()
    {
        // $cats = Category::where('parent_id',0)->get();
        $cats =Category::all();
        return view('shop.admin.category.viewCategories', compact('cats'));
    }


    public function create()
    {
        $cats = Category::all();
        LogActivity::addToLog('Create Category List','1');

        return view('shop.admin.category.createCategory',compact('cats'));
    }


    public function store(Request $request)
    {
        $cat = Category::create($request->all());
        // $cat->created_by = Auth::user()->id;
        // $cat->save();
        $cat = LogActivity::addToLog('View Categories List','1');
        LogActivity::addToLog('Stored A Category','1');
        return redirect()->route('category.index')->with('success',' Category Added');
    }


    public function show(Category $category)
    {
        //
    }


    public function edit(Category $category)
    {
        //
    }


    public function update(Request $request, Category $category)
    {
        //
    }


    public function destroy(Category $category)
    {
        //
    }

    public function changeStatus(Request $request)

    {

        $user = Category::find($request->user_id);

        $user->status = $request->status;

        $user->save();

        LogActivity::addToLog('Changed Category Status','1');

        return response()->json(['success'=>'Status change successfully.']);

    }
}
