<?php

namespace App\Http\Controllers;
use App\CustomField;
use App\Category;
use App\CategoryHasFields;
use Illuminate\Http\Request;

class CustomFieldController extends Controller
{

    public function index()
    {
        $fields=CustomField::all();
        return view('shop.admin.custom_fields.index',(['fields'=>$fields]));
    }


    public function create()
    {
       return view('shop.admin.custom_fields.create');
    }


    public function store(Request $request)
    {
        $data = $request->validate(['field_name'=>'required',
                                    'type'=>'required']);
         $data['values']=$request->values;
         CustomField::create($data);
         return redirect()->route('fields.index');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
    public function add(Request $request)
    {
        if(CategoryHasFields::where('cat_id',$request->cat_id)->where('field_id',$request->field_id)->first()){

            $filed=CategoryHasFields::where('cat_id',$request->cat_id)->where('field_id',$request->field_id)->first();
            $filed->delete();
            $data=CategoryHasFields::where('cat_id',$request->cat_id)->get();

            return response()->json([$data, 'Deleted']);

        }else{

            CategoryHasFields::create($request->all());
            return response()->json([$request, 'Added']);
        }

    }

    public function addtoCat($id)
    {
        $category=Category::find($id);
        $fields=CustomField::all();
        $this_cat_fields = CategoryHasFields::where('cat_id', $id)->get();
        return view('shop.admin.custom_fields.add_to_category',compact('category','fields', 'this_cat_fields') );
    }
}
