<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function login(Request $request){

        $user =User::where('email',$request->email)->first();
        if($user && Hash::check($request->password, $user->password)){
            $token = $user->createToken('api-user')->accessToken;
            return response()->json(['user'=>$user,'token'=>$token],200);
        }

        return response()->json(['error'=>'authentication failed'],422);

    }

}
