<?php

namespace App\Http\Controllers\Api;

use App\Ad;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdController extends Controller
{
    public function __construct(){
        $this->middleware(['auth:api-user'])->except(['login']);
    }
    public function all(){
        $ads= Ad::all();
        return response()->json($ads);
    }
}
