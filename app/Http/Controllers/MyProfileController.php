<?php

namespace App\Http\Controllers;

use App\User;
use App\UserBankDetails;
use App\UserDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use DB;
use Exception;

class MyProfileController extends Controller
{

    public function index()
    {
        $mydetails = UserDetails::where('user_id',Auth::user()->id)->first();
        $provinces = DB::select('select * from province_system_data ');
        $districts = DB::select('select * from district_system_data ');
        $bank=UserBankDetails::where('user_id',Auth::user()->id)->first();
        return view('shop.seller.profile.update',compact('mydetails','provinces','districts','bank'));

    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
         $userdetails =UserDetails::where('user_id',$id)
                        ->update([
                            'first_name'=>$request->first_name,
                            'last_name'=>$request->last_name,
                            'shop_name'=>$request->shop_name,
                            'mobile'=>$request->mobile,
                            'mobile2'=>$request->mobile2,
                            'address'=>$request->address,
                            'address'=>$request->address,
                            'province_id'=>$request->province_id,
                            'district_id'=>$request->district_id,
                            'city_id'=>$request->city_id,
                            'postal_code'=>$request->postal_code,
                        ]);

        return redirect()->route('myprofile.index')->with('suc', 'Profile Updated Succesfully');

    }


    public function destroy($id)
    {
        //
    }

    public function change(Request $request){


            $request->validate([
                  'old' => 'required',
                  'new_c'=> 'required',
                  'new'=> 'required',

              ]);
              $status = Hash::check($request->old, auth()->user()->password);

              if($request->new_c != $request->new){
                return redirect()->back()->with('er', 'New Passwords are not Matching');
              }else{
                if($status==true){

                    User::find(auth()->user()->id)->update(['password'=>Hash::make($request->new)]);
                    return redirect()->back()->with('suc', 'Password Changed Succesfully');
                }else{
                    return redirect()->back()->with('er', 'Current Password is not Matching');
                }
              }

    }
}
