<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Cart;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{

    public function index()
    {
        //

    }
    public function create()
    {
        //
    }


    public function store(Request $request)
    {

        $ad = Ad::where('id', $request->ad_id)->first();
        try {
            $item = Cart::where('user_id', Auth::user()->id)
                ->where('ad_id', $request->ad_id)->first();

            $checked_qty = $item->quantity += $request->quantity;

            if ($ad->quantity < $checked_qty) {
                return response()->json('insuf_qty');
                // return response()->json($item->quantity += $request->quantity);
            }
            $item->price += $request->price;
            $item->quantity = $checked_qty;
        } catch (Exception $e) {

            $item = Cart::create($request->all());
            $item->user_id = Auth::user()->id;
        }
        $item->save();


        $request->session()->flash('message', 'Item Added');
        $request->session()->flash('message-type', 'success');

        return response()->json('success');
        // return;

    }


    public function show($id)
    {

        // $data = DB::table('repayments')
        // ->select(DB::raw("sum(repayments.paid_amount) as sum"), DB::raw("count(repayments.paid_amount) as count"), 'repayments.payment_date_full', 'repayments.center')
        // ->groupBy('repayments.payment_date_full', 'repayments.center')
        // ->where('repayments.center', $request->center)
        // ->orderBy('repayments.id', 'desc')
        // ->get();

        // $cart = Cart::where('user_id', $id)
        // ->select('ad_id')
        // ->groupBy('ad_id')
        // ->get();

        $cart = DB::table('carts')
            ->where('carts.user_id', $id)
            ->leftJoin('ads', 'ads.id', 'carts.ad_id')
            ->select('carts.id', 'carts.ad_id', 'ads.title', 'ads.price', 'ads.img_1', 'ads.quantity as adQty', DB::raw("sum(carts.quantity) as quantity"), DB::raw("sum(carts.price) as total"))
            ->groupBy('carts.id', 'carts.ad_id', 'ads.title', 'ads.price', 'ads.img_1', 'adQty')
            ->get();
        return view('shop.front.cart.my_cart', compact('cart'));
    }

    public function edit(Cart $cart)
    {
        //
    }

    public function update(Request $request, Cart $cart)
    {
        // $ad = Ad::find($request->ad_id);
        // if($ad->quantity < $request->quantity){
        //     return response()->json(['Insufficient_Quantity', $ad->quantity, $ad->price]);
        // }

        $item = Cart::where('user_id', Auth::user()->id)
            ->where('ad_id', $request->ad_id)->first();
        // return $request;
        $item->price = $request->price;
        $item->quantity = $request->quantity;
        $item->save();

        return response()->json('cart_updated');
    }


    public function destroy(Request $request)
    {
        $item = Cart::find($request->id);
        $item->delete();
        return response()->json($request);
    }

    public function checkout($id)
    {
        $order_id = 'LAK'.Auth::user()->id.'O'.Carbon::now()->timestamp;
        $cart = DB::table('carts')
            ->where('carts.user_id', $id)
            ->leftJoin('ads', 'ads.id', 'carts.ad_id')
            ->select('carts.id', 'carts.ad_id', 'ads.title', 'ads.price', 'ads.img_1', 'ads.quantity as adQty', DB::raw("sum(carts.quantity) as quantity"), DB::raw("sum(carts.price) as total"))
            ->groupBy('carts.id', 'carts.ad_id', 'ads.title', 'ads.price', 'ads.img_1', 'adQty')
            ->get();

        return view('shop.front.cart.checkout', compact('cart', 'order_id'));
    }


}
