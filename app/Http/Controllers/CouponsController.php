<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\Helpers\LogActivity;
use Illuminate\Http\Request;

class CouponsController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set('Asia/Colombo');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = Coupon::all();

        LogActivity::addToLog('view coupon index', '1');

        return view('shop.admin.coupon.indexCoupon', compact('coupons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $coupon = new Coupon;
        $coupon->code = $request->input('code');
        $coupon->type = $request->input('type');
        $type = $request->input('type');
        if ($type == 'fixed') {
            $coupon->fixed = $request->input('value');
        } else if ($type == 'precentage') {
            $coupon->percent_off = $request->input('value');
        }
        $coupon->save();

        LogActivity::addToLog('create coupon', '1');

        return view('shop.admin.coupon.indexCoupon');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coupon = Coupon::find($id);
        // return($coupon);
        LogActivity::addToLog('edit coupon', '1');

        return view('shop.admin.coupon.editCoupon', compact('coupon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $coupon = Coupon::find($id);
        $coupon->code = $request->input('code');
        $coupon->type = $request->input('type');
        $type = $request->input('type');
        if ($type == 'fixed') {
            $coupon->fixed = $request->input('value');
        } else if ($type == 'precentage') {
            $coupon->percent_off = $request->input('value');
        }
        $coupon->update();

        \LogActivity::addToLog('update coupon', '1');

        return redirect('/coupon-index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $coupon = Coupon::find($id);
        $coupon->delete();

        \LogActivity::addToLog('delete coupon', '1');

        return redirect('/coupon-index');
    }
}
