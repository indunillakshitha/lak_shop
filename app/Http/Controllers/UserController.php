<?php

namespace App\Http\Controllers;

use App\Helpers\LogActivity;
// use App\LogActivity;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function myAccount($id)
    {

        $user = User::find($id);
        return view('shop.front.ads.my_account.view', compact('user'));
    }

    public function editAccount($id)
    {

        $user = User::find($id);
        return view('shop.front.ads.my_account.edit', compact('user'));
    }

    public function updateAccount(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        return redirect('/')->with('success', 'Account Updated Successfully');
    }

    public function __construct()
    {
        date_default_timezone_set('Asia/Colombo');
    }

    public function index()
    {
        // $userRoles = DB::table('users')
        //     ->leftjoin('model_has_roles', 'model_has_roles.model_id', 'users.id')
        //     ->leftjoin('roles', 'roles.id', 'model_has_roles.role_id')
        //     ->select('users.id', 'users.name as username', 'roles.name as roleName', 'users.email as email')
        //     ->groupBy('users.id', 'users.name', 'roles.name', 'users.email')
        //     ->groupBy('users.id')
        //     ->get();
        $userRoles = User::all();

        // $user = User::where('id', 1)->first();
        // $userRole = array($user->getRoleNames());

        // $userRole = DB::table('users')
        //     ->where('users.id', 3)
        //     ->leftjoin('model_has_roles', 'model_has_roles.model_id', 'users.id')
        //     ->leftjoin('roles', 'roles.id', 'model_has_roles.role_id')
        //     ->select('users.id', 'roles.name as roleName')
        //     ->groupBy('users.id', 'roles.name')
        //     ->first();
        // $userRole = $userRole->roleName;
        // return ($userRole);


        LogActivity::addToLog('view user details list', '1');

        return view('shop.admin.user.userDetails', compact('userRoles'));

    }

    public function createUser(Request $request)
    {
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->gender_id = $request->gender;
        $user->phone = $request->phone;

        $blockedValue = $request->input('blocked');
        if ($blockedValue == '1') {
            $user->blocked = 1;
        } else {
            $user->blocked = 0;
        }

        $verifiPhn = $request->input('verifiPhn');
        if ($verifiPhn == '1') {
            $user->verified_phone = 1;
        } else {
            $user->verified_phone = 0;
        }

        $verifiEml = $request->input('verifiEml');
        if ($verifiEml == '1') {
            $user->verified_email = 1;
        } else {
            $user->verified_email = 0;
        }

        $user->syncRoles([$request->input('super-admin'),
            $request->input('admin'),
            $request->input('moderator'),
            $request->input('seller'),
            $request->input('user')]);

        $user->syncPermissions([$request->input('list-permission'),
            $request->input('create-permission'),
            $request->input('update-permission'),
            $request->input('delete-permission'),
            $request->input('list-role'),
            $request->input('create-role'),
            $request->input('update-role'),
            $request->input('delete-role'),
            $request->input('access-admin-panel'),
            $request->input('access-dashboard'),
            $request->input('update-settings'),
            $request->input('maintenace'),
            $request->input('list-post'),
            $request->input('list-users'),
            $request->input('update-post'),
            $request->input('verification-handling')]);

        $user->save();

        LogActivity::addToLog('creating user', '1');

        return redirect('/user-details')->with('status', 'user created');
    }

    public function editUser($id)
    {
        $user = User::where('id', $id)->first();
        $permissions = User::where('id', $id)->first()->getAllPermissions();
        // $userRole = DB::table('users')
        //     ->where('users.id', $id)
        //     ->leftjoin('model_has_roles', 'model_has_roles.model_id', 'users.id')
        //     ->leftjoin('roles', 'roles.id', 'model_has_roles.role_id')
        //     ->select('users.id', 'roles.name as roleName')
        //     ->groupBy('users.id', 'roles.name')
        //     ->first();
        // $userRole = $userRole->roleName;
        $userRole = array($user->getRoleNames());

        $perm_arr = array();
        foreach ($permissions as $p) {
            array_push($perm_arr, $p->name);
        }
        LogActivity::addToLog('edit user', '1');

        return view('shop.admin.user.editUser', compact('user', 'permissions', 'userRole', 'perm_arr'));

    }

    public function updateUser(Request $request, $id)
    {

        $user = User::find($id);
        $blockedValue = $request->input('blocked');
        if ($blockedValue == '1') {
            $user->blocked = 1;
        } else {
            $user->blocked = 0;
        }

        $verifiPhn = $request->input('verifiPhn');
        if ($verifiPhn == '1') {
            $user->verified_phone = 1;
        } else {
            $user->verified_phone = 0;
        }

        $verifiEml = $request->input('verifiEml');
        if ($verifiEml == '1') {
            $user->verified_email = 1;
        } else {
            $user->verified_email = 0;
        }

        $user->phone = $request->input('phone');
        $user->gender_id = $request->input('gender');

        $user->syncRoles([$request->input('super-admin'),
            $request->input('admin'),
            $request->input('moderator'),
            $request->input('seller'),
            $request->input('user')]);

        $user->syncPermissions([$request->input('list-permission'),
            $request->input('create-permission'),
            $request->input('update-permission'),
            $request->input('delete-permission'),
            $request->input('list-role'),
            $request->input('create-role'),
            $request->input('update-role'),
            $request->input('delete-role'),
            $request->input('access-admin-panel'),
            $request->input('access-dashboard'),
            $request->input('update-settings'),
            $request->input('maintenace'),
            $request->input('list-post'),
            $request->input('list-users'),
            $request->input('update-post'),
            $request->input('verification-handling')]);

        $user->update();
        LogActivity::addToLog('update user', '1');

        return redirect('/user-details')->with('status', 'user updated');

    }
    public function deleteUser($id)
    {
        $user = User::find($id);
        $user->delete();
        LogActivity::addToLog('delete user', '1');

        return redirect('/user-details')->with('status', 'user deleted');

    }

    public function addRoles(Request $request)
    {
        $roleName = $request->input('roleName');
        $role = Role::create(['name' => $roleName]);
        $role->givePermissionTo([$request->input('list-permission'),
            $request->input('create-permission'),
            $request->input('update-permission'),
            $request->input('delete-permission'),
            $request->input('list-role'),
            $request->input('create-role'),
            $request->input('update-role'),
            $request->input('delete-role'),
            $request->input('access-admin-panel'),
            $request->input('access-dashboard'),
            $request->input('update-settings'),
            $request->input('maintenace'),
            $request->input('list-post'),
            $request->input('list-users'),
            $request->input('update-post'),
            $request->input('verification-handling')]);

        LogActivity::addToLog('adding role', '1');

        return view('shop.admin.index');

        // auth()->user()->assignRole('admin');
        // $user = User::find(2);
        // $user->givePermissionTo('update-role');

        // $role = Role::create(['name' => 'user']);
        // $permission = Permission::create(['name' => 'delete-role']);
        // $permission->assignRole('super-admin');

    }

    public function permissionIndex()
    {
        $permissions = Permission::all();

        LogActivity::addToLog('show permissions', '1');

        return view('shop.admin.permissions.permissionsIndex', compact('permissions'));

    }

    public function addPermissions(Request $request)
    {
        $permissionName = $request->input('permissionName');
        $permission = Permission::create(['name' => $permissionName]);

        LogActivity::addToLog('add permission', '1');

        return redirect('/permissions');
    }

    public function userActivity($id)
    {
        $activityLog = DB::table('log_activity')->where('user_id', $id)->get();
        return view('shop.admin.user.userActivity', compact('activityLog'));
    }

}
