<?php

namespace App\Http\Controllers;

use App\ProductReview;
use App\SingleProductReviewData;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class ProductReviewController extends Controller
{
    public function addReview(Request $request)
    {

        $pr = ProductReview::create($request->all());
        $pr->date = Carbon::now();
        if (Auth::user()) {
            $pr->user_id = Auth::user()->id;
        }
        if (!$pr->star_value) {
            $pr->star_value = 0;
        }
        $pr->save();

        try{

            $prod_data =  SingleProductReviewData::where('ad_id', $request->ad_id)->first();
            $prod_data->total_reviews++;
            $prod_data->total_star_count += $request->star_value;
            $prod_data->total_star_value += ($prod_data->total_star_count / $prod_data->total_reviews);
        } catch (Exception $e){
            $prod_data = new SingleProductReviewData;
            $prod_data->ad_id = $request->ad_id;
            $prod_data->total_reviews = 1;
            if(!$request->star_value){
                $prod_data->total_star_count = 0;
            } else {

                $prod_data->total_star_count = $request->star_value;
            }
        }
        $prod_data->total_star_value = ($prod_data->total_star_count / $prod_data->total_reviews);


        switch ($request->star_value) {
            case 5:
                $prod_data->s5++;
            break;
            case 4:
                $prod_data->s4++;
            break;
            case 3:
                $prod_data->s3++;
            break;
            case 2:
                $prod_data->s2++;
            break;
            case 1:
                $prod_data->s1++;
            break;
            case 0:
                ++$prod_data->s0;
            break;
        }

        $prod_data->save();


        return Redirect::back()->with('success', 'Review Added ');
    }
}
