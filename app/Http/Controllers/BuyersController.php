<?php

namespace App\Http\Controllers;

use App\Category;
use App\User;
use App\UserDetails;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Helpers\LogActivity;

class BuyersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $buy = User::all();
        LogActivity::addToLog('View Buyers List','1');
        return view('shop.admin.users.buyers', compact('buy'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function changeStatusEmail(Request $request)

    {
        $user = User::find($request->user_id);



        LogActivity::addToLog('Changed Email Veryfied','1');
        return response()->json(['success'=>'Status change successfully.']);



    }

    public function changeStatusReviewed(Request $request)

    {
        $user = User::find($request->user_id);



        LogActivity::addToLog('Changed Email Veryfied','1');
        return response()->json(['success'=>'Status change successfully.']);



    }
}
