<?php

namespace App\Http\Controllers;

use App\DeliveryOption;
use Illuminate\Http\Request;
use App\Helpers\LogActivity;


class DeliveryOptionController extends Controller
{
    /**shop.admin.
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dos = DeliveryOption::all();
        LogActivity::addToLog('Dilivery Option View','0');
        return view('shop.admin.delivery.deliveryOption',compact('dos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('shop.admin.delivery.createDeliveryOption');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $do = DeliveryOption::create($request->all());
        $do =  LogActivity::addToLog('Stored Delevery Option','1');
        return redirect()->route('delivery-option.index')->with('success',' Delevery Option Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DeliveryOption  $deliveryOption
     * @return \Illuminate\Http\Response
     */
    public function show(DeliveryOption $deliveryOption)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DeliveryOption  $deliveryOption
     * @return \Illuminate\Http\Response
     */
    public function edit(DeliveryOption $deliveryOption)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DeliveryOption  $deliveryOption
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DeliveryOption $deliveryOption)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DeliveryOption  $deliveryOption
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeliveryOption $deliveryOption)
    {
        //
    }

     public function changeStatus(Request $request)
    {
        $user = DeliveryOption::find($request->user_id);
        LogActivity::addToLog('Changed Delivery Option Status','0');
        return response()->json(['success'=>'Status change successfully.']);
    }
}
