<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Helpers\LogActivity;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;

class AdViewController extends Controller
{

    public function __construct()
    {
        date_default_timezone_set('Asia/Colombo');
    }

    public function index()
    {
        $ads = DB::table('ads')->leftjoin('user_details', 'user_details.user_id', 'ads.user_id')
            ->select('ads.id', 'user_details.user_id', 'user_details.first_name', 'user_details.shop_name', 'ads.title', 'ads.img_1', 'ads.created_at')
            ->get();
        // return $ads;
        return view('shop.admin.ads.adView', compact('ads'));
    }

    public function editAd($id)
    {
        $ad = DB::table('ads')->leftjoin('user_details', 'user_details.user_id', 'ads.user_id')
            ->select('ads.id', 'ads.title', 'ads.category', 'ads.title', 'ads.price', 'ads.item_description',
                'ads.img_1', 'ads.img_2', 'ads.img_3', 'ads.img_4', 'ads.img_5', 'ads.blocked', 'ads.subtitle',
                'user_details.user_id', 'user_details.mobile', 'user_details.shop_name', 'user_details.first_name')
            ->get();
        // return ($ad);
        return view('shop.admin.ads.editAd', compact('ad'));
    }

    public function updateAd(Request $request, $id)
    {
        $ad = Ad::find($id);
        $ad->title = $request->input('title');
        $ad->subtitle = $request->input('subtitle');
        $ad->item_description = $request->input('item_description');
        $ad->price = $request->input('price');
        // $ad->category = $request->input('category');
        // $ad->sub_category = $request->input('sub_category');
        // $ad->img_1 = $request->input('title');
        // $ad->img_1 = $request->input('title');
        // $ad->img_1 = $request->input('title');
        // $ad->img_1 = $request->input('title');
        // $ad->img_1 = $request->input('title');
        $ad->title = $request->input('title');
        $ad->title = $request->input('title');
        $blockedValue = $request->input('blocked');
        if ($blockedValue == '1') {
            $ad->blocked = 1;
        } else {
            $ad->blocked = 0;
        }

        $ad->update();
        LogActivity::addToLog('update ad', '1');
        return redirect('/posted-ads');

    }
}
