<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Ad;
use App\ProductReview;
use Illuminate\Support\Facades\Auth;

class ItemController extends Controller
{

    public function index()
    {
        $ads=Ad::where('user_id',Auth::id())->get();
       return view('shop.seller.items.index',(['ads'=>$ads]));
    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {





    }


    public function show($id)
    {
         $ad=Ad::where('id',$id)->first();
         $producat_reviews=ProductReview::where('ad_id',$id)->get();
        return view('shop.seller.items.itemDetails',compact('ad','producat_reviews'));
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
