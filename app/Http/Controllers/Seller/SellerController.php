<?php

namespace App\Http\Controllers\Seller;

use App\Ad;
use App\Cart;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderAddress;
use App\PayhereTest;
use App\User;
use App\UserBankDetails;
use App\UserDetails;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Helper\Table;

class SellerController extends Controller
{

    public function index()
    {
        if (Auth::user()->filled == 1) {
            $data = DB::table('user_details')->where('user_id', Auth::id())->get();
            $sold = Ad::all()->sum('sold_count');
            $customers = count(User::where('filled', '!=', '1')->get());
            return view('shop.seller.index', (['data' => $data, 'sold' => $sold, 'customers' => $customers]));
        } else {
            return view('shop.seller.register');
        }
    }

    public function sellerreg()
    {
        $provinces = DB::select('select * from province_system_data ');
        $districts = DB::select('select * from district_system_data ');
        return view('shop.seller.sellerDetails', compact('provinces', 'districts'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //    return $request;
        $personal = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'shop_name' => 'required',
            'mobile' => 'required',
            'mobile2' => 'required',
            'address' => 'required',
            'province_id' => 'required',
            'district_id' => 'required',
            'postal_code' => 'required',
            'city_id' => 'required',
            'nic_back_image' => 'required',
            'nic_front_image' => 'required',

        ]);
        // dd($personal);

        $bank = $request->validate([
            'bank_name' => 'required',
            'bank_branch' => 'required',
            'branch_code' => 'required',
            'bank_holder_name' => 'required',
            'bank_acc_no' => 'required',
            'img_passbook' => 'required',
        ]);

        $personal['user_id'] = Auth::id();
        $bank['user_id'] = Auth::id();
        $details = UserDetails::create($personal);
        UserBankDetails::create($bank);
        if ($request->file('nic_back_image')) {

            $image = $request->file('nic_back_image');
            $path = 'images/users';
            $details['nic_back_image'] = time() . rand() . '.' . $image->extension();
            $image->move(public_path($path), $details['nic_back_image']);
        }
        if ($request->file('nic_front_image')) {

            $image = $request->file('nic_front_image');
            $path = 'images/users';
            $details['nic_front_image'] = time() . rand() . '.' . $image->extension();
            $image->move(public_path($path), $details['nic_front_image']);
        }
        $details->save();
        DB::table('users')->update(['filled' => 1]);

        $user = User::where('id', Auth::user()->id)->first();
        $user->syncRoles('seller');

        return redirect()->route('sellerdetails.index');
    }

    function list() {
        $sellers = UserDetails::all();

        return view('shop.admin.seller.sellerList', compact('sellers'));
    }

    public function status($id, $status)
    {
        $data = UserDetails::find($id);
        $data->status = $status;
        $data->update();

    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function getCities(Request $request)
    {

        // $dis_id = DB::select("select id from district_system_data where name = '$request->district'")->get();
        $dis_id = DB::table('district_system_data')
            ->select('district_system_data.id')
            ->where('district_system_data.name', $request->district)
            ->first();

        $cities = DB::table('city_system_data')
            ->select('city_system_data.*')
            ->where('city_system_data.district_id', $dis_id->id)
            ->get();

        // $cities = DB::table("select id from city_system_data where district_id = '$dis_id'");
        // return $cities;
        return response()->json($cities);
    }
    public function getData()
    {
        $data = DB::table('user_details')->where('user_id', Auth::id())->get();

        return response()->json($data);
    }

    public function payhereNotify(Request $request)
    {
        // return 123;
        // return $request;
        try {
            $merchant_id = $request['merchant_id'];
            $order_id = $request['order_id'];
            $payhere_amount = $request['payhere_amount'];
            $payhere_currency = $request['payhere_currency'];
            $status_code = $request['status_code'];
            $md5sig = $request['md5sig'];

            $merchant_secret = '4vUnvhiJOpX4TrJ0BQzl6m4eSzyshhw998X3cgCnG4Ah'; // Replace with your Merchant Secret (Can be found on your PayHere account's Settings page)

            $local_md5sig = strtoupper(md5($merchant_id . $order_id . $payhere_amount . $payhere_currency . $status_code . strtoupper(md5($merchant_secret))));

            if (($local_md5sig === $md5sig) and ($status_code == 2)) {
                //TODO: Update your database as payment success
                // return redirect('/');
                // return DB::select("
                // INSERT INTO
                //     payhere_test
                //     (order_id, amount, currency)
                // VALUES
                //     ('$order_id', '$payhere_amount', '$payhere_currency' )

                // ");
                $order = new PayhereTest;
                $order->order_id = $order_id;
                $order->amount = $payhere_amount;
                $order->currency = $payhere_currency;
                $order->save();
            }
        } catch (Exception $e) {
            $order_id = 'dfddfdg';
            $payhere_amount = 100;
            $payhere_currency = 'USD';

            $order = new PayhereTest;
            $order->order_id = $order_id;
            $order->amount = $payhere_amount;
            $order->currency = $payhere_currency;
            $order->save();
        }

        return 123;
    }

    public function payhereReturn(Request $request)
    {
        // $id;
        // return $request;
        // return $id;

        #reduce quantity in stock
        $checked_out_items = Cart::where('user_id', Auth::user()->id)->get();
        foreach ($checked_out_items as $cot) {
            $ad = Ad::find($cot->ad_id);
            $ad->sold_count += $cot->quantity;
            $ad->quantity -= $cot->quantity;
            $ad->save();
            $cot->delete();
        }

        // $order = PayhereTest::where('order_id', $request->order_id)->first();
        $orders = Order::where('user_id', Auth::user()->id)->where('order_id', $request->order_id)->where('status', 'Processing')->get();
        foreach ($orders as $order) {
            $order->status = 'Transaction Complete';
            $order->checked_out_at = Carbon::now();
            $order->product_name = $ad->title;
            $order->seller_id = $ad->user_id;
            $order->item_id = $ad->id;
            $order->ordered_by = $order->user_id;
            $order->save();

        }
        OrderAddress::where('order_id', $request->order_id)->update(['status' => 'Transaction Complete']);

        return redirect('/')->with('success', 'Order Completed');
    }

    public function orderToDB(Request $request)
    {
        // return $request;
        // PayhereTest::create($request->all());
        // $order = Order::create($request->all());
        OrderAddress::create($request->all());

        $cart_items = Cart::where('user_id', Auth::user()->id)->get();
        foreach ($cart_items as $c) {
            $order = new Order;
            $order->order_id = $request->order_id;
            $order->user_id = Auth::user()->id;
            $order->amount = $c->price;
            $order->quantity = $c->quantity;
            $order->save();
        }

        return response()->json($request);
    }

    public function updateSellerStatus(Request $request)
    {
        $seller = UserDetails::find($request->seller_id);
        $seller->status = $request->value;
        $seller->save();

        return response()->json(['success' => 'Status change successfully.']);

    }

    public function sellerDetails($id)
    {
        $seller = UserDetails::find($id);
        $ads = Ad::where('user_id', $id)->get();
        return view('shop.admin.seller.sellerDetails', compact('seller', 'ads'));
    }

    public function income()
    {
        $sellers = UserDetails::all();
        // $orderCount =
        return view('shop.admin.incomeDetails.sellersIncome', compact('sellers'));
    }

    public function sellerTransactions()
    {
        return view('shop.admin.incomeDetails.sellerTransactions');
    }
}
