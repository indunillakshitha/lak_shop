<?php

namespace App\Http\Controllers\seller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Order;
use Illuminate\Support\Facades\Auth;
use DB;
class OngoingOrderController extends Controller
{

    public function index()
    {
        $pending=DB::table('orders')
            ->leftjoin('ads','ads.id','orders.item_id')
            ->leftjoin('user_details','user_details.user_id','orders.ordered_by')
            ->where('orders.seller_id',Auth::user()->id)
            ->where('orders.status','Dispatched')
            ->select('orders.status as order_status','orders.*','ads.*','user_details.*')
            ->get();

        return view('shop.seller.orders.ongoing.index',compact('pending'));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
