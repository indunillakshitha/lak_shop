<?php

namespace App\Http\Controllers;

use App\Wishlist;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class WishlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return response()->json($request);
        $isAlready = Wishlist::where('user_id', Auth::user()->id)->where('ad_id', $request->ad_id)->first();

        if ($isAlready) {
            return response()->json('item_already_in_wishlist');
        }

        $item = Wishlist::create($request->all());
        $item->user_id = Auth::user()->id;
        $item->save();

        return response()->json('item_added_to_wishlist');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Wishlist  $wishlist
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        // $wishlist = Wishlist::firstWhere('user_id', Auth::user()->id);
        $wishlist = DB::table('wishlists')
            ->where('wishlists.user_id', Auth::user()->id)
            ->leftJoin('ads', 'ads.id', 'wishlists.ad_id')
            ->select('ads.*', 'wishlists.*')
            ->orderBy('wishlists.id')
            ->get();
        // return $wishlist;

        return view('shop.front.wishlist.wishlist', compact('wishlist'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Wishlist  $wishlist
     * @return \Illuminate\Http\Response
     */
    public function edit(Wishlist $wishlist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Wishlist  $wishlist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wishlist $wishlist)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Wishlist  $wishlist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $item = Wishlist::find($request->id);
        $item->delete();
        return response()->json('wishlist_item_removed');
    }
}
