<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function index()
    {
        //
    }


    public function create(Request $request)
    {
        $parent=Category::where('id',$request->cat_id)->get();
        return view('shop.admin.brands.create', compact('parent'));
    }


    public function store(Request $request)
    {
       $brand = Brand::create($request->all());

        return redirect()->back();
    }


    public function show($cat_id)
    {
        $brands=Brand::where('category',$cat_id)->get();
        return view('shop.admin.brands.index', compact('brands','cat_id'));
    }


    public function edit(Brand $brand)
    {
        //
    }


    public function update(Request $request, Brand $brand)
    {
        //
    }


    public function destroy(Brand $brand)
    {
        //
    }
}
