<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $orders = Order::where('user_id', Auth::user()->id)->groupBy('orders.order_id')->get();
        $orders = DB::table('orders')
        ->where('user_id', Auth::user()->id)
        ->select( 'order_id', 'status', DB::raw("sum(amount) as total"), DB::raw("sum(quantity) as quantity"), 'created_at')
        ->groupBy( 'order_id', 'status', 'created_at')
        ->get();

        // return $orders;

        return view('shop.front.orders.my_orders', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show( $id)
    {
        $orders = Order::where('user_id', Auth::user()->id)->where('order_id', $id)->get();
        // return $orders;
        return view('shop.front.orders.view_order', compact('orders'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }

    public function allOrders()
    {
        $orders = DB::table('orders')->leftJoin('users','users.id','orders.ordered_by')
        ->select('users.name','users.phone','orders.order_id','orders.product_name','orders.status','orders.checked_out_at','orders.id')->get();
        // return $orders;
        return view('shop.admin.orders.allOrders', compact('orders'));
    }

    public function orderDetails($id)
    {
        $order = DB::table('orders')->where('id' , $id)->get();
        $customer = DB::table('users')->where('id', $order[0]->ordered_by)->get();
        $seller = DB::table('user_details')->where('user_id', $order[0]->seller_id)->get();

        // return $order;
        return view('shop.admin.orders.orderDetails', compact('order', 'customer', 'seller'));
    }
}
