<?php

namespace App\Http\Controllers;

use App\Category;
use App\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\LogActivity;


class SubCategoryController extends Controller
{

    public function index()
    {
        $sub_cats = SubCategory::all();
        return  $p_id = $sub_cats[0]->parent_id;
        LogActivity::addToLog('View SubCategory','1');
        return view('shop.admin.sub_category.view_sub_cat', compact('sub_cats','p_id'));
    }


    public function create(Request $id)
    {
         $parent=Category::where('id',$id->parent_id)->get();
         LogActivity::addToLog('Create A SubCategory','1');
        return view('shop.admin.sub_category.create_sub_cat', compact('parent'));
    }


    public function store(Request $request)
    {
        $cat = Category::create($request->all());
        LogActivity::addToLog('Stored A SubCategory','1');
        return redirect()->back();
    }


    public function show($parent)
    {
        $category=Category::where('id',$parent)->first();
        $cats=Category::where('parent_id',$parent)->get();
        LogActivity::addToLog('Show SubCategory','1');
        return view('shop.admin.sub_category.view_sub_cat', compact('cats','parent','category'));

    }

    public function edit(SubCategory $subCategory)
    {
        //
    }

    public function update(Request $request, SubCategory $subCategory)
    {
        //
    }


    public function destroy(SubCategory $subCategory)
    {
        //
    }

    public function getSubCats(Request $request){

        $sub_cats = SubCategory::where('category_id', $request->id)->get();
        LogActivity::addToLog('Get SubCategory','1');
        return response()->json($sub_cats);
    }

    public function changeStatusSub(Request $request)

    {

        $user = Category::find($request->user_id);

        $user->status = $request->status;

        $user->save();


        LogActivity::addToLog('Changed SubCategory Status','1');
        return response()->json(['success'=>'Status change successfully.']);

    }
}
