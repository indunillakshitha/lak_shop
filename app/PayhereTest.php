<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayhereTest extends Model
{
    protected $fillable = ['order_id', 'amount', 'currency'];
}
