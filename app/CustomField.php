<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomField extends Model
{
    protected $table='custom_fields';
    protected $fillable=['field_name','type','values','created_at','updated_at'];
}
