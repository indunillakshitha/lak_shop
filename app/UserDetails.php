<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
   protected $table ='user_details';
   protected $fillable=[
    'first_name',
    'last_name',
    'shop_name',
    'mobile',
    'mobile2',
    'address',
    'province_id',
    'district_id',
    'city_id',
    'user_id',
    'nic_back_image',
    'nic_front_image',
    'created_at',
    'updated_at'
   ];
}
