<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryOption extends Model
{
    protected $table='delivery_option';
    protected $fillable = [
        'pricePerKg', 'pricePerKm ', 'status', 'id'
    ];
}
