<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table ='new_categories';
    protected $fillable = ['category', 'parent_id', 'status', 'created_at','updated_at'];
}
