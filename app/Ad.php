<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    protected $fillable = [
        'user_id',
        'status',
        'blocked',
        'title',
        'subtitle',
        'category',
        'condition',
        'condition_description',
        'img_1',
        'img_2',
        'img_3',
        'img_4',
        'img_5',
        'item_description',
        'duration',
        'price',
        'quantity',
        'payment_options',
        'sub_category',
        'brand',
        'cus_fields'
    ];

    protected $casts = [
        'cus_fields' => 'array'
    ];
}
