<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table='new_brands';
    protected $fillable=['brand','category','status','created_at','uodated_at','brand_image'];
}
