<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderAddress extends Model
{
    protected $fillable = [
        'address',
        'order_id',
        'province',
        'district',
        'city',
        'email',
        'phone',
        'order_notes',
        'first_name',
        'last_name',
        'status'
    ];
}
