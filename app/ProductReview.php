<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model
{
    protected $fillable = ['user_id', 'ad_id', 'star_value', 'review_content', 'username', 'email',  'date'];
}
