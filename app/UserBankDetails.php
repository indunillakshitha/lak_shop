<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBankDetails extends Model
{
    protected $table='user_bank_details';
    protected $fillable=['bank_name',
    'bank_branch',
    'branch_code',
    'bank_holder_name',
    'bank_acc_no',
    'img_passbook',
    'created_at','
    updated_at',
    'user_id'
];
}
