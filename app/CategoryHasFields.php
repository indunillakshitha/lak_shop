<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryHasFields extends Model
{
   protected $table='cats_have_fields';
   protected $fillable=['cat_id','field_id','created_at','updated_at'];
}
