<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SingleProductReviewData extends Model
{
    protected $fillable = [
        'ad_id',
        'total_star_value',
        'total_star_count',
        'total_reviews',
        's5',
        's4',
        's3',
        's2',
        's1',
        's0',
    ];
}
