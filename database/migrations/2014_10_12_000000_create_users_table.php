<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            // $table->index('user_id');
            // $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('country_code')->nullable();
            $table->string('language_code')->nullable();
            $table->integer('user_type_id')->nullable();
            $table->integer('gender_id')->nullable();
            $table->string('name');
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->smallInteger('phone_hidden')->nullable();
            $table->string('user_name')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->smallInteger('is_admin')->nullable();
            $table->smallInteger('can_be_impersonated')->nullable();
            $table->smallInteger('dissable_comments')->nullable();
            $table->smallInteger('receive_newslatter')->nullable();
            $table->smallInteger('recive_advice')->nullable();
            $table->string('ip_addr')->nullable();
            $table->string('provider')->nullable();
            $table->integer('provider_id')->nullable();
            $table->string('email_token')->nullable();
            $table->string('phone_token')->nullable();
            $table->text('phone_auth_token')->nullable();
            $table->text('profile_photo')->nullable();
            $table->text('bank')->nullable();
            $table->tinyInteger('verified_email')->default('0');
            $table->tinyInteger('verified_phone')->default('0');
            $table->integer('filled')->default('0');
            $table->tinyInteger('blocked')->default('0');
            $table->smallInteger('closed')->nullable();
            $table->datetime('last_login')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
