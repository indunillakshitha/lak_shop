<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSingleProductReviewDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('single_product_review_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ad_id')->nullable();
            $table->string('total_star_value')->default(0);
            $table->string('total_star_count')->default(0);
            $table->string('total_reviews')->default(0);
            $table->string('s5')->default(0);
            $table->string('s4')->default(0);
            $table->string('s3')->default(0);
            $table->string('s2')->default(0);
            $table->string('s1')->default(0);
            $table->string('s0')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('single_product_review_data');
    }
}
