<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('status')->default(1);
            $table->string('blocked')->default(0);

            $table->string('title')->nullable();
            $table->text('subtitle')->nullable();
            $table->string('category')->nullable();
            $table->string('brand')->nullable();
            $table->string('condition')->nullable();
            $table->text('condition_description')->nullable();
            $table->string('img_1')->nullable();
            $table->string('img_2')->nullable();
            $table->string('img_3')->nullable();
            $table->string('img_4')->nullable();
            $table->string('img_5')->nullable();
            $table->text('item_description')->nullable();

            //selling details
            $table->string('duration')->nullable();
            $table->string('price')->nullable();
            $table->string('quantity')->nullable();
            $table->string('payment_options')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
