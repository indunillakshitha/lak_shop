<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SuperDuperMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('province_system_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_en')->nullable();
            $table->string('name_si')->nullable();
            $table->string('name_ta')->nullable();
            $table->timestamps();
        });

        Schema::create('district_system_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('status')->default(1);
            $table->datetime('deleted_at');
            $table->timestamps();
        });

        Schema::create('city_system_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('district_id');
            $table->string('name')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('status')->default(1);
            $table->datetime('deleted_at');
            $table->timestamps();

            $table->foreign('district_id')->references('id')->on('district_system_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
