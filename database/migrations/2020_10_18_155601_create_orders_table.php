<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('status')->nullable();
            $table->unsignedBigInteger('ordered_by')->nullable();
            $table->unsignedBigInteger('item_id')->nullable();
            $table->string('quantity')->nullable();
            $table->string('checked_out_at')->nullable();
            $table->string('estimated_delivery_date_at')->nullable();
            $table->string('out_for_delivery_at')->nullable();
            $table->string('picked_at_at')->nullable();
            $table->string('in_transit_at')->nullable();
            $table->string('reached_destination_at')->nullable();
            $table->string('order_received_at')->nullable();
            $table->timestamps();

            $table->foreign('ordered_by')->references('id')->on('users');
            $table->foreign('item_id')->references('id')->on('ads');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
