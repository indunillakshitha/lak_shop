<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJsonTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::dropIfExists('custom_fields');
        // Schema::dropIfExists('cats_have_fields');
        // Schema::dropIfExists('user_custom_field_values');

        Schema::create('custom_fields', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('field_name')->nullable();
            $table->string('type')->nullable();
            $table->longtext('values')->nullable();
            $table->timestamps();
        });

        Schema::create('cats_have_fields', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cat_id');
            $table->unsignedBigInteger('field_id');
            $table->foreign('cat_id')->references('id')->on('new_categories');
            $table->foreign('field_id')->references('id')->on('custom_fields');
            $table->timestamps();
        });

        Schema::create('user_custom_field_values', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('field_id');
            $table->unsignedBigInteger('product_id');

            $table->text('value')->nullable();
            $table->foreign('product_id')->references('id')->on('ads');
            $table->foreign('field_id')->references('id')->on('custom_fields');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('custom_fields');
        // Schema::dropIfExists('cats_have_fields');
        // Schema::dropIfExists('user_custom_field_values');
    }
}
