<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('new_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('category');
            $table->bigInteger('parent_id');
            $table->string('status');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('new_categories');
    }
}
