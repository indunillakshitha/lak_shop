<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

use App\Ad;
use App\Category;

// use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    // $loaded_ads = Ad::skip(0)->take(15)->get();

    // $user_city = App\UserDetails::where('user_id', Illuminate\Support\Facades\Auth::user()->id)->first()->city_id;

    // $near_by_sellers = App\UserDetails::where('city_id', $user_city)->get();

    // $near_me = array();
    // foreach($near_by_sellers as $nbs){
    //     $ads = App\Ad::where('user_id', $nbs->user_id)->get();
    //     foreach($ads as $ad){
    //         array_push($near_me, $ad);
    //     }
    // }
    // return count($near_me);

    $loaded_ads = Ad::all();
    $cats = Category::all();

    return view('welcome', compact('loaded_ads', 'cats'));
});

Route::group(['middleware' => ['auth']], function () {

    Route::group(['middleware' => ['blocked']], function () {
        Route::get('/', function () {
            $loaded_ads = Ad::skip(0)->take(15)->get();
            $cats = Category::all();
            return view('welcome', compact('loaded_ads', 'cats'));
        });
    });
    //--------------------------------------------------admin panal-----------------------------------------

    Route::group(['middleware' => ['role:admin|super-admin']], function () {
        Route::get('/admin', function () {
            return view('shop/admin/index');
        });

        Route::get('/add-roles', function () {
            return view('shop.admin.roles.addRoles');
        });
        Route::post('/create-role', 'RolePermissionController@addRoles');
        Route::post('/create-permission', 'RolePermissionController@addPermissions');
        Route::get('/edit-roles-permissions/{id}', 'RolePermissionController@editRolesPermissions');
        Route::get('/update-roles-permissions/{id}', 'RolePermissionController@updateRolesPermissions');

        //roles and permissions
        Route::get('/add-permissions', function () {
            return view('shop.admin.permissions.addPermissions');
        });
        Route::get('/permissions', 'UserController@permissionIndex');
        Route::post('/create-permission', 'UserController@addPermissions');

        Route::post('/create-role', 'UserController@addRoles');

        Route::get('/add-user', function () {
            return view('shop.admin.user.addUser');
        });

        //user
        Route::get('/user-details', 'UserController@index');
        Route::post('/create-user', 'UserController@createUser');
        Route::get('/edit-user/{id}', 'UserController@editUser');
        Route::post('/update-user/{id}', 'UserController@updateUser');
        Route::delete('/delete-user/{id}', 'UserController@deleteUser');
        Route::get('/user-activity/{id}', 'UserController@userActivity');

        Route::post('/create-role', 'RolePermissionController@addRoles');
        Route::post('/create-permission', 'RolePermissionController@addPermissions');
        Route::get('/edit-roles-permissions/{id}', 'RolePermissionController@editRolesPermissions');
        Route::get('/update-roles-permissions/{id}', 'RolePermissionController@updateRolesPermissions');

        //category
        Route::get('/category/add', 'CategoryController@create');
        Route::post('/category/store', 'CategoryController@store');
        Route::get('/category/view', 'CategoryController@index');

        //sub category
        Route::get('/subcategory/add', 'SubCategoryController@create');
        Route::post('/subcategory/store', 'SubCategoryController@store');
        Route::get('/subcategory/view', 'SubCategoryController@editAd');

        //adview
        Route::get('/posted-ads', 'AdViewController@index');
        Route::get('/edit-ad/{id}', 'AdViewController@editAd');
        Route::post('/update-ad/{id}', 'AdViewController@updateAd');

        //oders

        Route::get('/all-orders', 'OrderController@allOrders');
        Route::get('/order-details/{id}', 'OrderController@orderDetails');

        //selles
        Route::get('/sellers-list', 'Seller\SellerController@list');
        Route::post('//updateSellerStatus', 'Seller\SellerController@updateSellerStatus');
        Route::get('/seller-details/{id}', 'Seller\SellerController@sellerDetails');

        //incom and payments
        Route::get('/sellers-income', 'Seller\SellerController@income');
        Route::get('/seller-transactions/{id}', 'Seller\SellerController@sellerTransactions');

    });
    //cart
    Route::get('/add-cart-item', 'CartController@store');
    Route::get('/remove-cart-item', 'CartController@destroy');
    Route::get('/update-cart-item', 'CartController@update');
    Route::get('/my-cart/{id}', 'CartController@show');
    Route::get('/checkout/{id}', 'CartController@checkout');

    //user view orders
    Route::get('/my-orders', 'OrderController@index');
    Route::get('/view-order/{id}', 'OrderController@show');

    //add review
    Route::post('/add-review', 'ProductReviewController@addReview');

    //header
    Route::get('/get-sub-cats', 'SubCategoryController@getSubCats');

    //wishlist
    Route::get('/wishlist', 'WishlistController@show');
    Route::get('/add-wishlist-item', 'WishlistController@store');
    Route::get('/remove-wishlist-item', 'WishlistController@destroy');

    //general search
    Route::post('/serach-products', 'AdController@genSearch');
    Route::get('/search-category/{cat}', 'AdController@genSearchCategory');

    //my account
    Route::get('/my-account/{id}', 'UserController@myAccount');
    Route::get('/edit-account/{id}', 'UserController@editAccount');
    Route::post('/update-account/{id}', 'UserController@updateAccount');

    //-----------------------------------------------------------------------------seller dashboard routes
    Route::get('/seller', 'Seller\SellerController@index');
    Route::get('/get-cities', 'Seller\SellerController@getCities');
    Route::get('/getData', 'Seller\SellerController@getData');
    // Route::post('/seller/notify_url','Seller\SellerController@payhereNotify');
    // Route::get('/seller/payhere/return_url?order_id=ItemNo12345','Seller\SellerController@payhereReturn');
    Route::get('/seller/payhere/return_url', 'Seller\SellerController@payhereReturn');
    Route::post('/order-to-db', 'Seller\SellerController@orderToDB');

    Route::resources([
        'seller/items' => 'Seller\ItemController',
        'seller/orders' => 'Seller\OrderController',
        'sellerdetails' => 'Seller\SellerController',
        'selleritem' => 'AdController',
        'my-ad' => 'MyAdController',
        'category' => 'CategoryController',
        'sub-category' => 'SubCategoryController',
        'brand' => 'BrandController',
        'ongoing' => 'Seller\OngoingOrderController',
        'pending' => 'Seller\PendingOrderController',
        'fields' => 'CustomFieldController',
        'myprofile' => 'MyProfileController',
    ]);

    //---------------------------------------------------------------change seller password----------
    Route::post('/updatepassword', 'MyProfileController@change');
    Route::get('/changepassword', function () {
        return view('shop.seller.profile.changepassword');
    });

    Route::get('/view-ad/{id}', 'AdController@show');
    Route::get('/seller/reg', 'Seller\SellerController@sellerreg');

    Route::get('/view-ad/{id}', 'AdController@show');
    Route::get('/seller/reg', 'Seller\SellerController@sellerreg');

//cart
    Route::get('/add-cart-item', 'CartController@store');
    Route::get('/remove-cart-item', 'CartController@destroy');
    Route::get('/update-cart-item', 'CartController@update');
    Route::get('/my-cart/{id}', 'CartController@show');
    Route::get('/checkout/{id}', 'CartController@checkout');

//user view orders
    Route::get('/my-orders', 'OrderController@index');
    Route::get('/view-order/{id}', 'OrderController@show');

//add review
    Route::post('/add-review', 'ProductReviewController@addReview');

//header
    Route::get('/get-sub-cats', 'SubCategoryController@getSubCats');

//wishlist
    Route::get('/wishlist', 'WishlistController@show');
    Route::get('/add-wishlist-item', 'WishlistController@store');
    Route::get('/remove-wishlist-item', 'WishlistController@destroy');

//general search
    Route::post('/serach-products', 'AdController@genSearch');
    Route::get('/search-category/{cat}', 'AdController@genSearchCategory');

//my account
    Route::get('/my-account/{id}', 'UserController@myAccount');
    Route::get('/edit-account/{id}', 'UserController@editAccount');
    Route::post('/update-account/{id}', 'UserController@updateAccount');

//-----------------------------------------------------------------------------seller dashboard routes
    Route::get('/seller', 'Seller\SellerController@index');
    Route::get('/get-cities', 'Seller\SellerController@getCities');
    Route::get('/getData', 'Seller\SellerController@getData');
// Route::post('/seller/notify_url','Seller\SellerController@payhereNotify');
    // Route::get('/seller/payhere/return_url?order_id=ItemNo12345','Seller\SellerController@payhereReturn');
    Route::get('/seller/payhere/return_url', 'Seller\SellerController@payhereReturn');
    Route::get('/order-to-db', 'Seller\SellerController@orderToDB');

    Route::resources([
        'seller/items' => 'Seller\ItemController',
        'seller/orders' => 'Seller\OrderController',
        'sellerdetails' => 'Seller\SellerController',
        'selleritem' => 'AdController',
        'my-ad' => 'MyAdController',
        'category' => 'CategoryController',
        'sub-category' => 'SubCategoryController',
        'brand' => 'BrandController',
        'ongoing' => 'Seller\OngoingOrderController',
        'pending' => 'Seller\PendingOrderController',
        'fields' => 'CustomFieldController',
    ]);
// });

// ads
    Route::get('/create-ad', 'AdController@create');
    Route::post('/submit-ad', 'AdController@store');

//admin panal
    Route::get('/roles-permissions', 'UserController@index');
    Route::get('/add-roles', function () {
        return view('shop.admin.roles.addRoles');
    });
    Route::get('/add-permissions', function () {
        return view('shop.admin.permissions.addPermissions');
    });

    Route::post('/create-role', 'UserController@addRoles');
    Route::post('/create-permission', 'UserController@addPermissions');
    Route::get('/edit-roles-permissions/{id}', 'UserController@editRolesPermissions');
    Route::get('/update-roles-permissions/{id}', 'UserController@updateRolesPermissions');

//cart
    Route::get('/add-cart-item', 'CartController@store');
    Route::get('/remove-cart-item', 'CartController@destroy');
    Route::get('/update-cart-item', 'CartController@update');
    Route::get('/my-cart/{id}', 'CartController@show');
    Route::get('/checkout/{id}', 'CartController@checkout');

//header
    Route::get('/get-sub-cats', 'SubCategoryController@getSubCats');

//wishlist
    Route::get('/wishlist', 'WishlistController@show');
    Route::get('/add-wishlist-item', 'WishlistController@store');
    Route::get('/remove-wishlist-item', 'WishlistController@destroy');

//general search
    Route::post('/serach-products', 'AdController@genSearch');
    Route::get('/serach-category', 'AdController@genSearch');

//-----------------------------------------------------------------------------seller dashboard routes
    Route::get('/seller', 'Seller\SellerController@index');
    Route::get('/get-cities', 'Seller\SellerController@getCities');
    Route::get('/getData', 'Seller\SellerController@getData');

    Route::resources([
        'seller/items' => 'Seller\ItemController',
        'seller/orders' => 'Seller\OrderController',
        'sellerdetails' => 'Seller\SellerController',
        'selleritem' => 'AdController',
        'my-ad' => 'MyAdController',
        'category' => 'CategoryController',
        'sub-category' => 'SubCategoryController',
        'brand' => 'BrandController',
        'ongoing' => 'Seller\OngoingOrderController',
        'pending' => 'Seller\PendingOrderController',
    ]);

    Route::get('/view-ad/{id}', 'AdController@show');

//-----------------------------------------------------LOG ACTIVITY TEST ROUTES--------------
    Route::get('add-to-log', 'HomeController@myTestAddToLog');
    Route::get('logActivity', 'HomeController@logActivity');

//Coupon
    Route::get('/create-coupon', function () {
        return view('shop.admin.coupon.addCoupon');
    });
    Route::get('/edit-coupon/{id}', 'CouponsController@edit');
    Route::post('/update-coupon/{id}', 'CouponsController@update');
    Route::delete('/delete-coupon/{id}', 'CouponsController@destroy');
    Route::get('/coupon-index', 'CouponsController@index')->name('coupon.index');
    Route::post('/coupon', 'CouponsController@create')->name('coupon.create');
    //-----------------------------------------------------CUSTOM FIELD  ROUTES--------------

    Route::get('add-to-cat/{id}', 'CustomFieldController@addtoCat')->name('fields.cat');
    Route::get('addfields', 'CustomFieldController@add')->name('fields.add');

    // ------------------------------------- Dilivery Option ---------------------------------
    Route::resource('delivery-option', 'DeliveryOptionController');
    Route::get('/delivery-option/changeStatus', 'DeliveryOptionController@changeStatus');
    //-----------------------------------------------------CUSTOM FIELD  ROUTES--------------
    Route::get('add-to-cat/{id}', 'CustomFieldController@addtoCat')->name('fields.cat');
    Route::get('addfields', 'CustomFieldController@add')->name('fields.add');


});

Auth::routes(['verify' => true]);
